import sys
import inspect


def to_fn(op):
  if op == 'ap':
    return lambda fn: lambda x: fn(x)
  elif op == 'add':
    return lambda x: lambda y: int(x) + int(y)
  elif op == 'mul':
    return lambda x: lambda y: int(x) * int(y)
  elif op == 'inc':
    return lambda x: int(x) + 1
  elif op == 'dec':
    return lambda x: int(x) - 1
  elif op == 's':
    return lambda x0: lambda x1: lambda x2: to_fn(x0)(x2)(to_fn(x1)(x2))   #ap ap x0 x2 ap x1 x2
  elif callable(op):
    return op
  else:
    assert False, "Shouldn't happen %s" % op
    return lambda x: x

def consume(ops, n):
  # print ('consume %s %s' % (ops, n))
  out_elems = []
  tail = ops
  for x in range(n):
    head = tail[0]
    tail = tail[1:]
    if head == 'ap':
      elems, tail = consume(tail, 2)
      # print ('ap with %s; tail = %s' % (elems, tail))
      out_elems.append(to_fn(elems[0])(elems[1]))
    elif head == 'add' or head == 'inc' or head == 'mul' or head == 's':
      out_elems.append(head)
    else:
      out_elems.append(int(head))
  # print ('consumed %s from %s -> %s' % (n, ops, out_elems))
  return out_elems, tail

def main(ops):
  print (consume(ops, 1)[0][0])

if __name__ == '__main__':
  main(sys.argv[1:])
