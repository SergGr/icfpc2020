import sys
import inspect
sys.setrecursionlimit(15000)
# def to_fn(op):
#   if op == 'ap':
#     return lambda fn: lambda x: fn(x)
#   elif op == 'add':
#     return lambda x: lambda y: int(x) + int(y)
#   elif op == 'mul':
#     return lambda x: lambda y: int(x) * int(y)
#   elif op == 'inc':
#     return lambda x: int(x) + 1
#   elif op == 'dec':
#     return lambda x: int(x) - 1
#   elif op == 's':
#     return lambda x0: lambda x1: lambda x2: to_fn(x0)(x2)(to_fn(x1)(x2))   #ap ap x0 x2 ap x1 x2
#   elif callable(op):
#     return op
#   else:
#     assert False, "Shouldn't happen %s" % op
#     return lambda x: x

def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
    except TypeError:
        return False

# def is_cons(op):
#   a, b = op
#   a_is_cons = False
#   b_is_cons = False
#   if (a == 'cons' or a == 'nil' or is_int(a)):
#     a_is_cons = True
#   if ((b == 'cons' or b == 'nil' or is_int(b))):
#     b_is_cons = True
#   if a_is_cons and b_is_cons:
#     return True
#   if not a_is_cons and isinstance(a, tuple):
#     a_is_cons = is_cons(a)
#     if not a_is_cons:
#       return False
#   if not b_is_cons and isinstance(b, tuple):
#     b_is_cons = is_cons(b)
#     if not b_is_cons:
#       return False
#   return a_is_cons and b_is_cons

def consume(ops, n):
  # print ('consume %s %s' % (ops, n))
  out_elems = []
  tail = ops
  for x in range(n):
    head = tail[0]
    tail = tail[1:]
    if head == 'ap':
      elems, tail = consume(tail, 2)
      # print ('ap with %s; tail = %s' % (elems, tail))
      out_elems.append((elems[0], elems[1]))
    # elif is_int(head):
    #   out_elems.append(int(head))
    else:
      out_elems.append(head)
  # print ('consumed %s from %s -> %s' % (n, ops, out_elems))
  return out_elems, tail

# def gen_cons(op):
#   return op

def gen_haskell(op):
  if is_int(op) or isinstance(op, str):
    return op
  # elif is_cons(op):
  #   return gen_cons(op)
  else:
    fn, x = op
    return '(' + gen_haskell(fn) + ') (' + gen_haskell(x) + ')'

def convert(ops):
  head, tail = consume(ops, 1)
  # print (head[0])
  return (gen_haskell(head[0]))


if __name__ == '__main__':
  # lines = [x.replace(':', 'op')
  #     .replace('cons', '(:)')
  #     .replace('isnil', 'null')
  #     .replace('nil', '[]')
  #     .strip().split(' ') for x in open(sys.argv[1]).readlines()]
  # # import pdb; pdb.set_trace()
  # for line in lines:
  #   if line[1] == '=':
  #     print ("%s = %s" % (line[0], convert(line[2:])))
  print(convert(sys.argv[1:]))
