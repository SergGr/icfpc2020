import sys
from fractions import Fraction
from tkinter import *

WIDTH = 640
HEIGHT = 480
SIZE = 10

def get_pixel(ptx, pty):
  x = ptx*SIZE + WIDTH // 2
  y = HEIGHT // 2 - pty*SIZE

  return (x - SIZE // 2, y - SIZE // 2, x + SIZE // 2, y + SIZE // 2)


def add_points(points):
  for pt in points:
    w.create_rectangle( get_pixel(*pt) , outline="white", fill="white")

master = Tk()

w = Canvas(master, width=WIDTH, height=HEIGHT)
def callback(event):
  x = (event.x - WIDTH // 2) // SIZE
  y = (HEIGHT // 2 - event.y) // SIZE
  print ("clicked at", x, y)

w.bind("<Button-1>", callback)
w.pack()
w.create_rectangle( (0, 0, WIDTH, HEIGHT) , fill="black")

add_points([(8,10), (8,9), (8,8), (8,7), (8,6), (8,5), (8,4), (8,3), (8,2), (8,1), (8,0), (8,-1), (8,-2), (8,-3), (8,-4), (8,-5), (16,4), (15,4), (14,4), (13,4), (12,4), (11,4), (10,4), (9,4), (8,4), (7,4), (6,4), (5,4), (4,4), (3,4), (2,4), (1,4)])

mainloop()
