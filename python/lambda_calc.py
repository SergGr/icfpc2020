import sys
import inspect
import itertools
sys.setrecursionlimit(150000)

def simple_producer(ops):
  for x in ops:
    yield x

def is_int(s):
  try:
    int(s)
    return True
  except ValueError:
    return False
  except TypeError:
    return False

class Cons(object):
  def __init__(self, head, tail):
    self.head = head
    self.tail = tail

  def __str__(self):
    return 'cons(' + str(self.head) + ', ' + str(self.tail) + ')'

class Evaler(object):
  def __init__(self):
    self.context = {}
    self.ops = None

  def to_int(self, x):
    if is_int(x):
      return int(x)
    if isinstance(x, list):
      assert len(x) == 1, str(x)
      return self.to_int(x[0])
    if x in self.context:
      return self.to_int(self.context[x])
    if isinstance(x, Cons) and x.tail == 'nil':
      return self.to_int(x.head)
    print ("Something really-really bad happened")
    import pdb; pdb.set_trace()
    return 0

  def maybe_eval(self, op):
    if isinstance(op, str) and ':' in op:
      evaler = Evaler()
      evaler.context = self.context
      return evaler.compute(simple_producer(self.context[op]))
    else:
      return op

  def to_fn(self, op):
    if op == 'add':
      return lambda x: lambda y: self.to_int(x) + self.to_int(y)
    elif op == 'mul':
      return lambda x: lambda y: self.to_int(x) * self.to_int(y)
    elif op == 'div':
      def div1(x):
        def div2(y):
          a = self.to_int(x)
          b = self.to_int(y)
          return (a+(-a%b))//b
        return div2
      return div1
    elif op == 'inc':
      return lambda x: self.to_int(x) + 1
    elif op == 'neg':
      return lambda x: -self.to_int(x)
    elif op == 'dec':
      return lambda x: self.to_int(x) - 1
    elif op == 't':
      def t(x):
        return lambda y: x
      return t
    elif op == 'f':
      def f(x):
        return lambda y: y
      return f
    elif op == 's':
      return lambda x0: lambda x1: lambda x2: self.to_fn(x0)(x2)(self.to_fn(x1)(x2))   #ap ap x0 x2 ap x1 x2
    elif op == 'c':
      return lambda x0: lambda x1: lambda x2: self.to_fn(self.to_fn(x0)(x2))(x1)  # ap ap x0 x2 x1
    elif op == 'b':
      return lambda x0: lambda x1: lambda x2: self.to_fn(x0)(self.to_fn(x1)(x2))
    elif op == 'cons':
      def cons1(x0):
        def cons2(x1):
          # xx0 = self.maybe_eval(x0)
          # xx1 = self.maybe_eval(x1)
          return Cons(x0, x1)
        return cons2
      return cons1 # self.to_fn(self.to_fn(x2)(x0))(x1)  #   #  ap ap x2 x0 x1
    elif op == 'car':
      def car(x2):
        # import pdb; pdb.set_trace()
        return x2.head
        # return self.to_fn(x2)(self.to_fn('t'))
      return car
    elif op == 'cdr':
      def cdr(x2):
        return x2.tail
      return cdr
        # return self.to_fn(x2)(self.to_fn('f'))
    elif op == 'nil':
      def nil(x0):
        return self.to_fn('t')
      return nil
    elif op == 'i':
      return lambda x0: x0
    elif op == 'eq':
      return lambda x0: lambda y0: self.to_fn('t') if self.to_int(x0) == self.to_int(y0) else self.to_fn('f')
    elif op == 'lt':
      return lambda x0: lambda y0: self.to_fn('t') if self.to_int(x0) < self.to_int(y0) else self.to_fn('f')
    elif op == 'isnil':
      def isnil(x0):
        return self.to_fn('t') if x0 == 'nil' else self.to_fn('f')
      return isnil
    elif isinstance(op, Cons):
      def cons(x):
        return self.to_fn(x)(op.head)(op.tail)
      return cons
    elif callable(op):
      return op
    elif isinstance(op, str) and ":" in op:
      print("to_fn: Chaining %s" % op)
      self.ops = itertools.chain(simple_producer(self.context[op]), self.ops)
      inp = self.consume1()
      return self.to_fn(inp)
    else:
      import pdb; pdb.set_trace()
      assert False, "Shouldn't happen %s" % op
      return lambda x: x

  def consume_ap(self):
    input1 = self.consume1()
    if isinstance(input1, str) and ':' in input1:
      # print("ap: Chaining %s" % input1)
      self.ops = itertools.chain(simple_producer(self.context[input1]), self.ops)
      return self.consume_ap()
    else:
      return self.to_fn(input1)(self.consume1())

  def consume1(self):
    head = next(self.ops)
    if head == 'ap':
      return self.consume_ap()
    else:
      return head

  def compute(self, ops):
    self.ops = ops
    x = self.consume1()
    return (x)


if __name__ == '__main__':
  evaler = Evaler()
  # lines = [x.strip().split(' ') for x in open(sys.argv[1]).readlines()]
  # for line in lines:
  #   if line[1] == '=':
  #     evaler.context[line[0]] = line[2:]
  # evaler.compute(simple_producer(evaler.context[":1104"]))
  # evaler.context[":galaxy"] = evaler.context["galaxy"]
  # galaxy = 'ap ap :1338 nil ap ap cons 0 ap ap cons 0 nil'.split(' ')
  # print(evaler.compute(simple_producer(galaxy)))
  evaler.context = {":2048": "div 4 2".split(' '),
                    ":2049": "ap ap".split(' '),
                    ":2050": "ap ap ap c".split(' ')}
  galaxy = 'ap ap ap c :2048'.split(' ')
  print(evaler.compute(simple_producer(galaxy)))

  # evaler.context = {":67108929": "ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c cons nil ap c cons".split(' ')}
  # galaxy = "ap ap :67108929 nil ap ap cons 43 ap ap cons 44 nil".split(' ')
  # evaler.compute(simple_producer(galaxy))


  # print (list(context.items())[:3])
  # evaler.context = {":2048": "ap f :2048".split(' ')}
  # galaxy = "ap :2048 42".split(' ')
  # evaler.compute(simple_producer(galaxy))
  # evaler.context = {":1029": "ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil".split(' ')}
  # galaxy = "ap ap :1029 nil ap ap cons 43 nil".split(' ')
  # print(evaler.compute(simple_producer(galaxy)))
  # print(evaler.compute(simple_producer(sys.argv[1:])))
  # compute(simple_producer(galaxy), context)
