package icfpc.tools

import icfpc.expr.ExprElem
import java.awt.image.BufferedImage
import java.io.File
import java.io.FileWriter
import java.io.PrintStream
import java.io.PrintWriter
import java.util.*
import javax.imageio.ImageIO
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.HashSet
import kotlin.math.abs
import kotlin.math.sqrt


enum class GlyphColor(val rgb: Int) {
    FilledPixel(0xFFFFFF), // white
    EmptyPixel(0x333333), // dark grey

    Number(0x00FF00), // green
    BinaryNumber(0x88FF00), // greenish-yellow different from Boolean
    Boolean(0xBBFF00), // greenish-yellow different from Apply
    Var(0x0000FF), // blue
    Apply(0x8800FF), // purple
    Equals(0xFFAA00), // orangish-yellow
    Operator(0xFFFF00), // yellow
    UnknownOperator(0xFF0000), // red
    Ellipsis(0x777777),  // grey
    ImageLike(0xc0c080),  // light-yellow
    ;

    fun toHexColor(): String {
        return "#" + String.format("%06x", rgb)
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

interface GlyphWriter {
    fun start()
    fun finish()
    fun writePixel(x: Int, y: Int, filled: Boolean)
    fun writeAnnotation(x: Int, y: Int, size: GlyphSize, color: GlyphColor, text: String)
}

/**
 * Save Glyphs as annotated SVG image
 */
class SvgGlyphWriter(
    private val textWriter: PrintWriter,
    private val width: Int,
    private val height: Int,
    private val zoom: Int = 16
) : GlyphWriter {

    // TODO: switch to an actual XML-library instead of this hack
    private fun escapeXml(str: String): String {
        val sb = StringBuilder(str.length + 10)
        for (c in str) {
            when (c) {
                '<' -> sb.append("&lt;")
                '>' -> sb.append("&gt;")
                '\"' -> sb.append("&quot;")
                '&' -> sb.append("&amp;")
                '\'' -> sb.append("&apos;")
                else -> sb.append(c)
            }
        }
        return sb.toString()
    }

    override fun start() {

        textWriter.println(
            """<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="${width * zoom}" height="${height * zoom}">"""
        )
        textWriter.println(
            """<rect width="100%" height="100%" style="fill:black"/>"""
        )
    }

    override fun finish() {
        textWriter.println("</svg>")
        textWriter.flush()
    }

    override fun writePixel(x: Int, y: Int, filled: Boolean) {
        val color = if (filled) GlyphColor.FilledPixel else GlyphColor.EmptyPixel
        textWriter.println("""<rect x="${x * zoom}" y="${y * zoom}" width="${zoom - 1}" height="${zoom - 1}" style="fill:${color.toHexColor()}"/>""")
    }

    override fun writeAnnotation(
        x: Int,
        y: Int,
        size: GlyphSize,
        color: GlyphColor,
        text: String
    ) {
        textWriter.println("""<rect x="${x * zoom}" y="${y * zoom}" width="${size.externalWidth * zoom}" height="${size.externalHeight * zoom}" style="fill:${color.toHexColor()};opacity:0.6"/>""")
        val style = "paint-order: stroke; fill: lightgrey; stroke: black; stroke-width: 3px; font:20px bold sans;"
        val escapedText = escapeXml(text)
        textWriter.println(
            """<text x="${x * zoom + size.externalWidth * zoom / 2}" y="${y * zoom + size.externalHeight * zoom / 2}" dominant-baseline="middle" text-anchor="middle" style="$style">${escapedText}</text>"""
        )
    }
}

/**
 * Write Glyph at shifted position
 * @see PositionedGlyph
 */
class ShiftedGlyphWriter(private val writer: GlyphWriter, private val dx: Int, private val dy: Int) :
    GlyphWriter {
    override fun start() {
        writer.start()
    }

    override fun finish() {
        writer.finish()
    }

    override fun writePixel(x: Int, y: Int, filled: Boolean) {
        writer.writePixel(x + dx, y + dy, filled)
    }

    override fun writeAnnotation(
        x: Int,
        y: Int,
        size: GlyphSize,
        color: GlyphColor,
        text: String
    ) {
        writer.writeAnnotation(x + dx, y + dy, size, color, text)
    }
}

/**
 * Save glyphs as plain text.
 * This writer expects that glyphs are written in the natural order top to bottom, left to right
 */
class TextGlyphWriter(private val textStream: PrintStream) : GlyphWriter {

    companion object {
        fun systemOut(): TextGlyphWriter {
            return TextGlyphWriter(System.out)
        }
    }

    private var lastY = -1
    private var firstInLine = true
    override fun start() {
    }

    override fun finish() {
        if (lastY != -1)
            textStream.println()
        textStream.flush()
    }

    override fun writePixel(x: Int, y: Int, filled: Boolean) {
        // do nothing
    }

    override fun writeAnnotation(x: Int, y: Int, size: GlyphSize, color: GlyphColor, text: String) {
        if (lastY != y) {
            textStream.println()
            lastY = y
            firstInLine = true
        }
        if (firstInLine) {
            firstInLine = false

        } else {
            textStream.print("  ")
        }
        textStream.print(text)
    }
}

class CompositeGlyphWriter(private val w1: GlyphWriter, private val w2: GlyphWriter) : GlyphWriter {
    override fun start() {
        w1.start()
        w2.start()
    }

    override fun finish() {
        w1.finish()
        w2.finish()
    }

    override fun writePixel(x: Int, y: Int, filled: Boolean) {
        w1.writePixel(x, y, filled)
        w2.finish()
    }

    override fun writeAnnotation(x: Int, y: Int, size: GlyphSize, color: GlyphColor, text: String) {
        w1.writeAnnotation(x, y, size, color, text)
        w2.writeAnnotation(x, y, size, color, text)
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////

data class GlyphSize constructor(
    val internalWidth: Int,
    val internalHeight: Int,
    val externalWidth: Int,
    val externalHeight: Int
) {

    companion object {
        private const val numberBorderSize = 1 // top and left is 1 pixel
        private const val opBorderSize = 1 // top and left is 1 pixel
        const val additionalVarBorder = 2 // vars have additional 1 pixel wide border around at all 4 sides
        private const val varBorderSize = numberBorderSize + additionalVarBorder

        // most of the Ops so far are 4x4
        val defaultOpSize4: GlyphSize = opSize(4)

        fun extSize(width: Int, height: Int): GlyphSize {
            return GlyphSize(width, height, width, height)
        }

        fun opSize(extSize: Int): GlyphSize {
            return GlyphSize(extSize - opBorderSize, extSize - opBorderSize, extSize, extSize)
        }

        private fun numberSizeFromIntImpl(value: Int): Int {
            if (value == 0)
                return 1
            val pow = Glyph.powFromInt(value)
            val iSqrt = sqrt((pow + 1).toDouble()).toInt()
            val size = if (iSqrt * iSqrt >= pow + 1) iSqrt else iSqrt + 1
            return size
        }

        fun numberSizeFromInt(value: Int): GlyphSize {
            val intSize = numberSizeFromIntImpl(value)
            return numberSize(intSize, value < 0)
        }

        /**
         * @see GlyphSize.isNegativeNumber
         */
        fun numberSize(intSize: Int, isNegative: Boolean): GlyphSize {
            return GlyphSize(
                intSize, intSize,
                intSize + numberBorderSize,
                intSize + numberBorderSize + (if (isNegative) 1 else 0)
            )
        }


        fun varSize(intSize: Int): GlyphSize {
            return GlyphSize(
                intSize, intSize,
                intSize + varBorderSize, intSize + varBorderSize
            )
        }

        fun varSizeFromInt(value: Int): GlyphSize {
            if (value < 0)
                throw  RuntimeException("Unexpected variable index '$value'")
            val intSize = numberSizeFromIntImpl(value)
            return varSize(intSize)
        }
    }

    val isNegativeNumber: Boolean
        get() {
            return externalHeight > externalWidth
        }
}


data class EvalResult(val res: Glyph.ExprGlyph, val followingExpression: List<Glyph>)


sealed class Glyph {
    abstract fun write(glyphWriter: GlyphWriter)
    abstract val size: GlyphSize get

    abstract class ExprGlyph : Glyph() {
        abstract fun toExprElem(): ExprElem
    }


    companion object {
        fun powFromInt(value: Int): Int {
            if (value == 0)
                return 1
            val abs = abs(value)
            var pow = 0
            var tmp = abs
            while (tmp > 1) {
                pow += 1
                tmp /= 2
            }
            return pow
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    data class NumberGlyph(val value: Int, override val size: GlyphSize) : ExprGlyph() {
        companion object {
            fun fromInt(value: Int): NumberGlyph {
                return NumberGlyph(value, GlyphSize.numberSizeFromInt(value))
            }
        }

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, this.size, GlyphColor.Number, value.toString())
        }

        override fun toExprElem(): ExprElem {
            return ExprElem.NumberElem(value.toBigInteger())
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    data class BinaryNumberGlyph(val value: Int, override val size: GlyphSize) : Glyph() {
        override fun write(glyphWriter: GlyphWriter) {
            val binaryString = (if (value < 0) "-" else "+") + Integer.toBinaryString(abs(value)) + "b"
            glyphWriter.writeAnnotation(0, 0, this.size, GlyphColor.BinaryNumber, "$value as $binaryString")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BooleanGlyph protected constructor(val value: Boolean) : ExprGlyph() {
        companion object {
            val booleanSize: GlyphSize = GlyphSize.opSize(3)
        }

        override val size: GlyphSize
            get() = booleanSize

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, this.size, GlyphColor.Boolean, if (value) "True" else "False")
        }
    }

    object TrueGlyph : BooleanGlyph(true) {
        override fun toExprElem(): ExprElem {
            return ExprElem.TrueElem
        }
    }

    object FalseGlyph : BooleanGlyph(false) {
        override fun toExprElem(): ExprElem {
            return ExprElem.FalseElem
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    data class VarGlyph(val value: Int, override val size: GlyphSize) : Glyph() {
        companion object {
            fun fromInt(value: Int): VarGlyph {
                return VarGlyph(value, GlyphSize.varSizeFromInt(value))
            }
        }

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, this.size, GlyphColor.Var, "Var$value")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Ellipsis : Glyph() {
        override val size: GlyphSize = GlyphSize.extSize(7, 1)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Ellipsis, "...")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BaseOp : ExprGlyph() {
    }

    abstract class BaseOp1 : BaseOp() {
    }

    abstract class BaseOp2 : BaseOp() {
    }

    abstract class BaseOp3 : BaseOp() {
    }


    ////////////////////////////////////////////////////////////////////////////////////
    data class UnknownOp(val opCode: Int, override val size: GlyphSize) : BaseOp() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.UnknownOperator, "?$opCode?")
        }

        override fun toExprElem(): ExprElem {
            TODO("Not yet implemented")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    //
    //
    /**
     * The difference between [EqOp] and [MetaEquals] is that [EqOp] is a usual operator with 2 parameters that can
     * return true ([TrueGlyph] or false ([FalseGlyph]) while [MetaEquals] is a meta-glyph that is not an operator
     * and which is used to describe axioms
     * @see EqOp
     * @see MetaEquals
     * @see TrueGlyph
     * @see FalseGlyph
     */
    object MetaEquals : Glyph() {
        private val equalsSize: GlyphSize = GlyphSize.opSize(3)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Equals, "===")
        }

        override val size: GlyphSize
            get() = equalsSize
    }

    object EqOp : BaseOp2() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "_ == _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            return ExprElem.EqOp
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // According to the specification in the message12 it is strictly less
    // 0 < 0 === False
    // 0 < 1 === True
    object Less : BaseOp2() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "_ < _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            return ExprElem.Less
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    /**
     * This glyph means that after that follows another [Apply] or some Op-glyph (a subclass of [BaseOp])
     * and then the same number of parameters as there were [Apply] glyphs.
     * For example:
     * ap Dec 1 => 0
     * ap ap Plus 1 2 => 3
     */
    object Apply : ExprGlyph() {
        private val applySize: GlyphSize = GlyphSize.opSize(2)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Apply, "ap")
        }

        override val size: GlyphSize
            get() = applySize

        override fun toExprElem(): ExprElem {
            return ExprElem.Apply
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Inc : BaseOp1() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, this.size, GlyphColor.Operator, "Inc _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4


        override fun toExprElem(): ExprElem {
            return ExprElem.Inc
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Dec : BaseOp1() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, this.size, GlyphColor.Operator, "Dec _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            return ExprElem.Dec
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Plus : BaseOp2() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "_ + _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            return ExprElem.Plus
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Mul : BaseOp2() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "_ * _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            return ExprElem.Mul
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // 5 / -3 === -1
    // -5 / 3 === -1
    // -5 / -3 === 1
    // so the rounding seems always to be towards 0
    object Div : BaseOp2() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "_ / _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4


        override fun toExprElem(): ExprElem {
            return ExprElem.Div
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // neg 0 === 0
    // neg 1 === -1
    // neg -1 === 1
    object Neg : BaseOp1() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "Neg _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            return ExprElem.Neg
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object ConvertNumberToBits : BaseOp() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "toBits _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            TODO("Not yet implemented")
        }
    }

    object ConvertBitsToNumber : BaseOp() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "fromB _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            TODO("Not yet implemented")
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////

    object SCombinatorOp : BaseOp3() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "S _ _ _")
        }

        override val size: GlyphSize
            get() = GlyphSize.opSize(3)


        override fun toExprElem(): ExprElem {
            return ExprElem.SCombinatorOp
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////

    object CCombinatorOp : BaseOp3() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "S _ _ _")
        }

        override val size: GlyphSize
            get() = GlyphSize.opSize(3)

        override fun toExprElem(): ExprElem {
            return ExprElem.SCombinatorOp
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////

    object AlienFunction : BaseOp() {
        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.Operator, "Alien _")
        }

        override val size: GlyphSize
            get() = GlyphSize.defaultOpSize4

        override fun toExprElem(): ExprElem {
            TODO("Not yet implemented")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////

    object ListStartOp : PatternGlyph() {
        private val pattern = """
            ..*
            .**
            ***
            .**
            ..*
        """.trimIndent()

        val bitsMatrix = PatternGlyph.bitsFromStringPattern(pattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "<<")
        }
    }

    object ListEndOp : PatternGlyph() {
        private val pattern = """
            *..
            **.
            ***
            **.
            *..
        """.trimIndent()

        val bitsMatrix = PatternGlyph.bitsFromStringPattern(pattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, ">>")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BaseImageGlyph : Glyph() {
    }

    ////////////////////////////////////////////////////////////////////////////////////

    class UnknownImageGlyph(override val size: GlyphSize) : Glyph() {
        override fun write(glyphWriter: GlyphWriter) {
//            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "Unknown Image")
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "??")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////

    abstract class PatternGlyph : BaseImageGlyph() {
        companion object {
            val knownPatternGlyphsImages: Array<Pair<PatternGlyph, Array<BooleanArray>>> = arrayOf(
                Pair(AlienShipGlyph, AlienShipGlyph.bitsMatrix),
                Pair(EarthRadioGlyph, EarthRadioGlyph.bitsMatrix),
                Pair(SendWaveGlyph, SendWaveGlyph.bitsMatrix),
                Pair(SendWaveVerticalGlyph, SendWaveVerticalGlyph.bitsMatrix),
                Pair(SmallPlanetGlyph, SmallPlanetGlyph.bitsMatrix),
                Pair(MediumPlanetGlyph, MediumPlanetGlyph.bitsMatrix),
                Pair(SaturnPlanetGlyph, SaturnPlanetGlyph.bitsMatrix),
                Pair(JuniperPlanetGlyph, JuniperPlanetGlyph.bitsMatrix),
                Pair(SunGlyph, SunGlyph.bitsMatrix)
            )

            val knownPatternGlyphs: Array<Pair<PatternGlyph, Array<BooleanArray>>> = arrayOf(
                Pair(ListStartOp, ListStartOp.bitsMatrix),
                Pair(ListEndOp, ListEndOp.bitsMatrix)
            )

            fun bitsFromStringPattern(stringPattern: String): Array<BooleanArray> {
                val lines = stringPattern.split("\n")
                val w = lines[0].length
                val h = lines.size
                val bitMatrix = Array<BooleanArray>(h) { BooleanArray(w) }
                for ((y, line) in lines.withIndex()) {
                    val bitLine = bitMatrix[y]
                    for ((x, ch) in line.withIndex()) {
                        bitLine[x] = when (ch) {
                            '*' -> true
                            '.' -> false
                            else -> throw RuntimeException("Unexpected symbol in pattern '$ch'")
                        }
                    }
                }
                return bitMatrix
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////

    object AlienShipGlyph : PatternGlyph() {
        private val alienShipPattern = """
            ...*...
            .*****.
            .*...*.
            **...**
            **.*.**
            .*****.
            *.*.*.*
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(alienShipPattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "AlienShip")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object EarthRadioGlyph : PatternGlyph() {
        private val earthRadioPattern = """
            ..*.*..
            ..*.*..
            ..***..
            ..***..
            ..***..
            *******
            ...*...
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(earthRadioPattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "EarthRadio")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object SendWaveGlyph : PatternGlyph() {
        private val wavePattern = """
            ..**........**.......
            .*..*......*..*......
            *....*....*....*....*
            ......*..*......*..*.
            .......**........**..
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(wavePattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "send wave")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object SendWaveVerticalGlyph : PatternGlyph() {
        private val verticalWavePattern = """
            ..*..
            ...*.
            ....*
            ....*
            ...*.
            ..*..
            .*...
            *....
            *....
            .*...
            ..*..
            ...*.
            ....*
            ....*
            ...*.
            ..*..
            .*...
            *....
            *....
            .*...
            ..*..
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(verticalWavePattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "send wave")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object SmallPlanetGlyph : PatternGlyph() {
        private val planetPattern = """
            ***
            *.*
            ***
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(planetPattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "Planet")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object MediumPlanetGlyph : PatternGlyph() {
        private val planetPattern = """
            *****
            *...*
            *...*
            *...*
            *****
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(planetPattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "Planet")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object SaturnPlanetGlyph : PatternGlyph() {
        private val planetPattern = """
            ..********..
            ..*......*..
            ..*......*..
            ..*......*..
            ************
            ..*......*..
            ..*......*..
            ..*......*..
            ..********..
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(planetPattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "Saturn")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object JuniperPlanetGlyph : PatternGlyph() {
        private val planetPattern = """
            *************
            *...........*
            *...........*
            *...........*
            *...........*
            *...........*
            *...........*
            *...........*
            *...........*
            *...........*
            *...........*
            *...........*
            *************
        """.trimIndent()

        val bitsMatrix = bitsFromStringPattern(planetPattern)

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "Juniper")
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object SunGlyph : PatternGlyph() {

        val bitsMatrix = Array(55) { BooleanArray(21) { true } }

        override val size: GlyphSize = GlyphSize.extSize(bitsMatrix[0].size, bitsMatrix.size)

        override fun write(glyphWriter: GlyphWriter) {
            glyphWriter.writeAnnotation(0, 0, size, GlyphColor.ImageLike, "Sun")
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////
}


data class PositionedGlyph(val xLeft: Int, val yTop: Int, val glyph: Glyph) {
    fun writeAsSvg(writer: GlyphWriter) {
        val shiftedWriter = ShiftedGlyphWriter(writer, xLeft, yTop)
        glyph.write(shiftedWriter)
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////


class ImageDecoder constructor(private val inputImageFile: String, private val failOnUnknown: Boolean = true, private val zoom: Int = 4) {
    private val image: BufferedImage = ImageIO.read(File(inputImageFile))

    val width: Int
        get() {
            return image.width / zoom
        }

    val height: Int
        get() {
            return image.height / zoom
        }


    private fun getPixel(x: Int, y: Int): Boolean {
        val rgb = image.getRGB(x * zoom, y * zoom)
        val r = (rgb shr 16) and 0xff
        val g = (rgb shr 8) and 0xff
        val b = rgb and 0xff
        return r + g + b >= 127 * 3
    }

    fun proccessPixels(pixelProcessor: (x: Int, y: Int, filled: Boolean) -> Unit) {
        val imageWidth = width
        val imageHeight = height
        for (y in 0 until imageHeight) {
            for (x in 0 until imageWidth) {
                pixelProcessor(x, y, getPixel(x, y))
            }
        }
    }

    fun decodeGlyphs(): List<PositionedGlyph> {
        return decodeGlyphLines().fold<List<PositionedGlyph>, MutableList<PositionedGlyph>>(ArrayList<PositionedGlyph>(),
            { acc, lineList -> acc.addAll(lineList); acc })
    }

    fun decodeGlyphLines(): List<List<PositionedGlyph>> {
        val lines: MutableList<List<PositionedGlyph>> = ArrayList()
        val takenPixels: MutableMap<Int, MutableSet<Int>> = HashMap()

        val imageWidth = width
        val imageHeight = height

        // 1 to h-1 and w-1 to skip 1 pixel wide borders of the whole image
        for (yTop in 1 until imageHeight - 1) {
            val glyphs: MutableList<PositionedGlyph> = ArrayList()
            takenPixels.putIfAbsent(yTop, HashSet())
            val takenInTopLine = takenPixels[yTop]!!
            for (xLeft in 1 until imageWidth - 1) {
                if (takenInTopLine.contains(xLeft))
                    continue

                val glyph = decodeGlyphAt(xLeft, yTop)
                var positionedGlyph: PositionedGlyph? = null
                if (glyph != null) {
                    positionedGlyph = PositionedGlyph(xLeft, yTop, glyph)
                } else if (getPixel(xLeft, yTop)) {
                    // if we have a pixel and it is not detected, process it anyway as UnknownImageGlyph
                    positionedGlyph = findUnknownConnectedAreaAt(xLeft, yTop)
                }
                // mark pixels as taken
                if (positionedGlyph != null) {
                    glyphs.add(positionedGlyph)
                    val glyphSize = positionedGlyph.glyph.size
                    for (y in 0 until glyphSize.externalHeight) {
                        takenPixels.putIfAbsent(positionedGlyph.yTop + y, HashSet())
                        val takenInLine = takenPixels[positionedGlyph.yTop + y]!!
                        for (x in 0 until glyphSize.externalWidth) {
                            takenInLine.add(positionedGlyph.xLeft + x)
                        }
                    }
                }
            }
            if (glyphs.isNotEmpty())
                lines.add(glyphs)
            // micro-optimization of memory usage
            takenPixels.remove(yTop)
        }

        return lines
    }

    private fun findUnknownConnectedAreaAt(xLeft: Int, yTop: Int): PositionedGlyph? {
        val q: Queue<Pair<Int, Int>> = LinkedList<Pair<Int, Int>>()
        val visited: MutableSet<Pair<Int, Int>> = HashSet<Pair<Int, Int>>()
        q.add(Pair(xLeft, yTop))
        var xMin = xLeft
        var xMax = xLeft
        var yMin = yTop
        var yMax = yTop

        val imageWidth = width - 1
        val imageHeight = height - 1

        while (!q.isEmpty()) {
            val p = q.poll()
            if (p in visited)
                continue
            visited.add(p)
            val (x, y) = p
            if ((x <= 0) || (x > imageWidth) || (y <= 0) || (y >= imageHeight))
                continue
            if (!getPixel(x, y)) continue

            if (x < xMin) xMin = x
            if (x > xMax) xMax = x

            if (y < yMin) yMin = y
            if (y > yMax) yMax = y

            // add all 8 neighbors
            q.add(Pair(x - 1, y - 1))
            q.add(Pair(x - 1, y))
            q.add(Pair(x - 1, y + 1))
            q.add(Pair(x, y - 1))
            q.add(Pair(x, y + 1))
            q.add(Pair(x + 1, y - 1))
            q.add(Pair(x + 1, y))
            q.add(Pair(x + 1, y + 1))
        }

        // don't treat single standalone pixels as glyphs
        if ((xMax > xMin) || (yMax > yMin))
            return PositionedGlyph(xMin, yMin, Glyph.UnknownImageGlyph(GlyphSize.extSize(xMax - xMin + 1, yMax - yMin + 1)))
        else
            return null
    }


    private fun detectEllipsisAt(xLeft: Int, yTop: Int): Glyph.Ellipsis? {
        // -1 to not process right and bottom borders
        val imageWidth = width - 1
        val imageHeight = height - 1
        if (yTop + 1 >= imageHeight)
            return null
        // check for "..."
        if ((xLeft + 6 < imageWidth)
            && !getPixel(xLeft, yTop + 1)
            && getPixel(xLeft, yTop)
            && !getPixel(xLeft + 1, yTop)
            && getPixel(xLeft + 2, yTop)
            && !getPixel(xLeft + 3, yTop)
            && getPixel(xLeft + 4, yTop)
            && !getPixel(xLeft + 5, yTop)
            && getPixel(xLeft + 6, yTop)
        ) {
            return Glyph.Ellipsis
        }
        return null
    }

    private fun detectBinaryNumberAt(xLeft: Int, yTop: Int): Glyph.BinaryNumberGlyph? {
        // -1 to not process right and bottom borders
        val imageWidth = width - 1
        val imageHeight = height - 1

        // if it is 3 pixels-high it is not binary
        if ((yTop + 2 < imageHeight) && getPixel(xLeft, yTop + 2))
            return null
        if (xLeft + 3 >= imageWidth)
            return null
        // positive pattern is
        // .*
        // *.
        val isPositive = !getPixel(xLeft, yTop) && getPixel(xLeft, yTop + 1)
                && getPixel(xLeft + 1, yTop) && !getPixel(xLeft + 1, yTop + 1)
        // negative pattern is
        // *.
        // .*
        val isNegative = getPixel(xLeft, yTop) && !getPixel(xLeft, yTop + 1)
                && !getPixel(xLeft + 1, yTop) && getPixel(xLeft + 1, yTop + 1)

        if (!isPositive && !isNegative)
            return null

        // just positive pattern alone is regular 0
        // .*
        // *.
        val isRegular0 = !getPixel(xLeft + 2, yTop) && !getPixel(xLeft + 2, yTop + 1)
        if (isRegular0)
            return null

        val lenStart = 2
        var lenMul4 = 0
        while (getPixel(xLeft + lenStart + lenMul4, yTop)) {
            lenMul4 += 1
        }
        val valStart = lenStart + lenMul4
        val len = lenMul4 * 4 + 1
        var pow = 1
        var value = 0
        for (x in (len - 1) downTo 0) {
            if (getPixel(xLeft + valStart + x, yTop))
                value += pow
            pow *= 2
        }
        if (isNegative)
            value = -value
        return Glyph.BinaryNumberGlyph(value, GlyphSize.extSize(lenStart + lenMul4 + len, 2))
    }

    private fun decodeNumberAt(xLeft: Int, yTop: Int, intGlyphSize: Int, isNegative: Boolean, inverted: Boolean): Int {
        var value = 0
        var pow = 1
        for (y in 1..intGlyphSize) {
            for (x in 1..intGlyphSize) {
                if (getPixel(xLeft + x, yTop + y) != inverted)
                    value += pow
                pow *= 2
            }
        }
        if (isNegative)
            value = -value
        return value
    }

    enum class RegularGlyphKind {
        // Ellipsis and BinaryNumber are non-regular i.e. non-square
        //    Ellipsis,
        //    BinaryNumber,
        Number,
        Boolean,
        Op,
        Var
    }

    // detects regular square-like glyphs
    private fun detectRegularGlyphAt(xLeft: Int, yTop: Int): Pair<RegularGlyphKind, GlyphSize>? {
        // -1 to not process right and bottom borders
        val imageWidth = width - 1
        val imageHeight = height - 1

        val isOp = getPixel(xLeft, yTop)

        // by default this is the size except for top and left borders
        // for vars we need to reduce it by 2 more for the additional border around
        var intGlyphSize = 1
        while ((yTop + intGlyphSize < imageHeight) && getPixel(xLeft, yTop + intGlyphSize)
            && (xLeft + intGlyphSize < imageWidth) && getPixel(xLeft + intGlyphSize, yTop)
        ) {
            intGlyphSize += 1
        }
        intGlyphSize -= 1
        if (intGlyphSize == 0)
            return null

        // check for Var first
        if (isOp && intGlyphSize >= 3) {
            // to speed up start with a simple check of the bottom right
            val yBottom = yTop + intGlyphSize
            val xRight = xLeft + intGlyphSize
            var wholeBorder = getPixel(xRight, yBottom)
            if (wholeBorder) {
                for (y in yTop..yBottom) {
                    if (!getPixel(xRight, y)) {
                        wholeBorder = false
                        break
                    }
                }
            }
            if (wholeBorder) {
                for (x in xLeft..xRight) {
                    if (!getPixel(x, yBottom)) {
                        wholeBorder = false
                        break
                    }
                }
            }

            if (wholeBorder && getPixel(xLeft + 1, yTop + 1))
                return Pair(RegularGlyphKind.Var, GlyphSize.varSize(intGlyphSize - GlyphSize.additionalVarBorder))
        }

        if (isOp)
            return Pair(RegularGlyphKind.Op, GlyphSize.opSize(intGlyphSize + 1))

        val isNegative = (yTop + intGlyphSize + 1 < imageHeight) && getPixel(xLeft, yTop + intGlyphSize + 1)
        return Pair(RegularGlyphKind.Number, GlyphSize.numberSize(intGlyphSize, isNegative))
    }

    private fun opFromCode(code: Int, glyphSize: GlyphSize): Glyph {
        return when (code) {
            // not really Ops but look similar so parse them here
            0 -> Glyph.Apply
            2 -> Glyph.TrueGlyph
            8 -> Glyph.FalseGlyph
            12 -> Glyph.MetaEquals

            // regular Ops
            7 -> Glyph.SCombinatorOp
            6 -> Glyph.CCombinatorOp
            10 -> Glyph.Neg
            40 -> Glyph.Div
            146 -> Glyph.Mul
            365 -> Glyph.Plus
            170 -> Glyph.ConvertNumberToBits
            341 -> Glyph.ConvertBitsToNumber
            // TODO: are there others DecBy and IncBy or is it always by 1?
            401 -> Glyph.Dec
            417 -> Glyph.Inc
            416 -> Glyph.Less
            448 -> Glyph.EqOp
            174 -> Glyph.AlienFunction
            else -> if (failOnUnknown) throw RuntimeException("Unknown op-code $code")
            else {
                System.err.println("Found an unknown op-code $code")
                Glyph.UnknownOp(code, glyphSize)
            }
        }
    }

    private fun checkPatternAtImpl(xLeft: Int, yTop: Int, bitsMatrix: Array<BooleanArray>): Boolean {
        // -1 to not process right and bottom borders
        val imageWidth = width - 1
        val imageHeight = height - 1

        val h = bitsMatrix.size
        val w = bitsMatrix[0].size
        if (((xLeft + w) > imageWidth) || ((yTop + h) > imageHeight))
            return false

        for (y in 0 until h) {
            for (x in 0 until w) {
                if (getPixel(xLeft + x, yTop + y) != bitsMatrix[y][x])
                    return false
            }
        }

        return true
    }

    private fun detectPatternGlyphAt(xLeft: Int, yTop: Int): Glyph? {
        for ((glyph, bitsMatrix) in Glyph.PatternGlyph.knownPatternGlyphs) {
            if (checkPatternAtImpl(xLeft, yTop, bitsMatrix))
                return glyph
        }
        return null
    }

    private fun decodeGlyphAt(xLeft: Int, yTop: Int): Glyph? {
        // detect strange image-like patterns first
        val strangeGlyph = detectPatternGlyphAt(xLeft, yTop)
        if (strangeGlyph != null)
            return strangeGlyph

        // ellipsis and binary numbers have an unusual pattern so detect them first
        // The order is potentially important as a binary representation's top line
        // might look like Ellipsis
        val binary = detectBinaryNumberAt(xLeft, yTop)
        if (binary != null)
            return binary

        val ellipsis = detectEllipsisAt(xLeft, yTop)
        if (ellipsis != null)
            return ellipsis


        val regularDetectResult = detectRegularGlyphAt(xLeft, yTop) ?: return null

        var (glyphKind, glyphSize) = regularDetectResult

        if (glyphKind == RegularGlyphKind.Var) {
            // vars have additional border and inverted colors
            val value =
                decodeNumberAt(xLeft + 1, yTop + 1, glyphSize.internalWidth, glyphSize.isNegativeNumber, glyphKind == RegularGlyphKind.Var)
            // ugly hack
            // according to message29 Var10 is an operation
            if (value < 10)
                return Glyph.VarGlyph(value, glyphSize)
            else
                glyphKind = RegularGlyphKind.Op
        }

        val value = decodeNumberAt(xLeft, yTop, glyphSize.internalWidth, glyphSize.isNegativeNumber, glyphKind == RegularGlyphKind.Var)

        return when (glyphKind) {
            RegularGlyphKind.Op -> opFromCode(value, glyphSize)
            RegularGlyphKind.Number -> Glyph.NumberGlyph(value, glyphSize)
            else -> throw RuntimeException("Unexpected glyph kind '$glyphKind'")
        }
    }

}

fun annotateImage(inMsgFile: String) {
    val dotPos = inMsgFile.indexOf(".")
    val outMsgFile = inMsgFile.substring(0, dotPos) + ".svg"
    val inFilePath = "./data/messages/$inMsgFile"
    println("Annotating '$inFilePath'")
    val imageDecoder = ImageDecoder(inFilePath, false)
    val glyphs = imageDecoder.decodeGlyphs()
    println("Found ${glyphs.size} glyphs in '$inMsgFile'")

    File("./data/messages/annotated").mkdirs()
    PrintWriter(FileWriter("./data/messages/annotated/$outMsgFile")).use { textWriter ->
        val svgWriter = SvgGlyphWriter(textWriter, imageDecoder.width, imageDecoder.height, if (imageDecoder.width > 100) 12 else 16)
        val glyphWriter = CompositeGlyphWriter(svgWriter, TextGlyphWriter.systemOut())
        glyphWriter.start()
        imageDecoder.proccessPixels { x: Int, y: Int, filled: Boolean ->
            glyphWriter.writePixel(x, y, filled)
        }

        for (gl in glyphs) {
            gl.writeAsSvg(glyphWriter)
        }
        glyphWriter.finish()
        println("Finished annotation of '$inMsgFile'")
    }
}

//@JvmStatic
fun main(args: Array<String>) {
//    val inMsgFile = "message26.png"
//    annotateImage(inMsgFile)
    for (i in 1..42) {
        annotateImage("message${i}.png")
    }
}
