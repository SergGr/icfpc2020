package icfpc.tools

import icfpc.expr.ExprElem
import icfpc.expr.parseLine
import icfpc.expr.parseText
import java.io.File
import kotlin.reflect.KCallable

//@JvmStatic
fun main(args: Array<String>) {
//    var context = HashMap<Int, List<ExprElem>>()
//    context.put(2048, parseLine("ap f :2048"))
//    val expr = parseLine("ap :2048 43")
//    var evaler = Evaler(expr.asIterable().iterator(), context)
//    val res = evaler.consume1()
//    println(res)
//    var context = HashMap<Int, List<ExprElem>>()
//    context.put(2048, parseLine("div 2 4"))
//    context.put(2049, parseLine("ap f :2049"))

//    val expr = parseLine("ap ap ap c :2048")
//    val expr = parseLine("ap :2049 43")
//    var evaler = Evaler(expr.asIterable().iterator(), context)
//    evaler.initTrivialCache()
//    println(evaler.isTrivialCache[2048])
//    println(evaler.isTrivialCache[2049])
//    println(evaler.isTrivialOp(2048))
//    println(evaler.isTrivialOp(2049))

//    val res = evaler.consume1()
//    println(res)

    val lines: List<String> = File("./data/messages/galaxy.v2.txt").readLines()
    val galaxy = parseText(lines)
    evaluateWithLambdaCal(galaxy)
}

fun evaluateWithLambdaCal(lines: List<List<ExprElem>>) {
    var context = HashMap<Int, List<ExprElem>>()
    var galaxy: List<ExprElem>? = null

    for (line in lines) {
        if (line.size < 3 || !(line[1] is ExprElem.UnknownOp) || (line[1] as ExprElem.UnknownOp).opCode != -1) {
            throw RuntimeException("Unexpected op in $line")
        }
        val opCode = (line[0] as ExprElem.UnknownOp).opCode
        if (opCode == -2) {  // it's a special galaxy op
            galaxy = line.subList(2, line.size)
        } else {
            context.put(opCode, line.subList(2, line.size))
        }
    }
    val galaxy_plus = parseLine("ap ap") + galaxy!! + parseLine("nil ap ap cons 0 ap ap cons 0 nil")
//    val galaxy_plus = parseLine("ap ap ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c cons nil ap c cons nil ap ap cons 0 ap ap cons 0 nil")
    var evaler = Evaler(galaxy_plus.asIterable().iterator(), context)
    evaler.initTrivialCache()
    val res = evaler.consume1()

    println("Galaxy(Nil) = $res")
}


data class Evaler(var seq: Iterator<ExprElem>, var context: Map<Int, List<ExprElem>>) {
    var aps = 0
    var opsCache: HashMap<Int, CurryFunc> = HashMap<Int, CurryFunc>()
    var isTrivialCache: HashMap<Int, Boolean> = HashMap()

    fun consume1(): Any {

        val head = this.seq.next()
        if (head is ExprElem.Apply)
            return this.consume_ap()
        else if (head is ExprElem.UnknownOp && isTrivialCache[head.opCode]!!) {
            prependSeq(head)
            return consume1()
        } else
            return head
    }

    private fun prependSeq(inp: ExprElem.UnknownOp) {
        val opCode = (inp as ExprElem.UnknownOp).opCode
        val new_tokens = this.context[opCode]!!
        val old_seq = this.seq
        this.seq = iterator {
            yieldAll(new_tokens)
            yieldAll(old_seq)
        }

    }

    private fun consume_ap(): Any {
        aps += 1
        if (aps % 1000 == 0) {
            println("aps=$aps")
        }

        val input1 = this.consume1()
        if (input1 is ExprElem.UnknownOp) {
            prependSeq(input1)
            return consume_ap()
        }
        return to_fn(input1)(consume1())
    }

    open class CurryFunc (private val x : (Any) -> Any) {
        operator fun invoke(y : Any): Any {
            return x(y)
        }
    }

    class FalseCurryFunc(x: (Any) -> Any) : CurryFunc(x) {}

    private fun to_fn(op: Any): CurryFunc {
        if (op is ExprElem.Plus) {
            return CurryFunc({ x: Any -> { y: Any -> to_int(x) + to_int(y) } })
        } else if (op is ExprElem.Mul) {
            return CurryFunc({ x: Any -> { y: Any -> to_int(x) * to_int(y) } })
        } else if (op is ExprElem.Div) {
            return CurryFunc({ x: Any -> { y: Any -> to_int(x).div(to_int(y)) } })
        } else if (op is ExprElem.Inc) {
            return CurryFunc({ x: Any -> to_int(x) + 1 })
        } else if (op is ExprElem.SCombinatorOp) {
            return CurryFunc({ x0: Any -> { x1: Any -> { x2: Any ->
                val pred = (to_fn(to_fn(x0)(x2)))
                if (pred is FalseCurryFunc)
                    pred(0)
                else
                    pred(to_fn(x1)(x2)) } } })
        } else if (op is ExprElem.CCombinatorOp) {
            return CurryFunc({ x0: Any -> { x1: Any -> { x2: Any -> (to_fn(to_fn(x0)(x2)))(x1) } } })
        } else if (op is ExprElem.BCombinatorOp) {
//            return { x0: Any -> { x1: Any -> { x2: Any -> (to_fn(x0)(to_fn(x1)(x2))) } } }
            return CurryFunc({ x0: Any ->
                val pred = to_fn(x0)
                if (pred is FalseCurryFunc)
                    {x1: Any -> {x2: Any -> pred(0) }}
                else
                    { x1: Any -> { x2: Any -> (pred(to_fn(x1)(x2))) } } })
        } else if (op is ExprElem.ConsOp) {
            return CurryFunc({ x0: Any -> { x1: Any -> Pair(x0, x1) } })
        } else if (op is ExprElem.CarOp) {
            return CurryFunc({ x2: Any -> (x2 as Pair<*, *>).first!! })
        } else if (op is ExprElem.CdrOp) {
            return CurryFunc({ x2: Any -> (x2 as Pair<*, *>).second!! })
        } else if (op is ExprElem.EqOp) {
            return CurryFunc({ x0: Any -> { x1: Any -> if (to_int(x1) == to_int(x0)) to_fn(ExprElem.TrueElem) else to_fn(ExprElem.FalseElem) } })
        } else if (op is ExprElem.Less) {
            return CurryFunc({ x0: Any -> { x1: Any -> if (to_int(x0) < to_int(x1)) to_fn(ExprElem.TrueElem) else to_fn(ExprElem.FalseElem) } })
        } else if (op is ExprElem.TrueElem) {
            return CurryFunc({ x: Any -> { y: Any -> x } })
        } else if (op is ExprElem.Neg) {
            return CurryFunc({ x: Any -> -to_int(x) })
        } else if (op is ExprElem.Id) {
            return CurryFunc({ x: Any -> x })
        } else if (op is ExprElem.IsNil) {
            return CurryFunc({ x: Any -> if (x is ExprElem.Nil) to_fn(ExprElem.TrueElem) else to_fn(ExprElem.FalseElem) })
        } else if (op is ExprElem.Nil) {
            return CurryFunc({ x: Any -> to_fn(ExprElem.TrueElem) })
        } else if (op is ExprElem.FalseElem) {
            return FalseCurryFunc({ x: Any -> { y: Any -> y } })
        } else if (op is ExprElem.UnknownOp) {
            val opCode = (op as ExprElem.UnknownOp).opCode
            if (!opsCache.containsKey(opCode)) {
                prependSeq(op)
                val fn = to_fn(consume1())
                this.opsCache.set(opCode, fn)
                if (opCode == 1208) {
                    println("Cached break :$opCode")
                } else {
                                    println("Cached :$opCode")

                }
            }

//            return fn
            return opsCache[opCode]!!
//            return to_fn(consume1())
        } else if (op is Pair<*, *>) {
            val pair = op as Pair<*, *>
            return CurryFunc({ x: Any -> (to_fn(x)(pair.first!!) as (Any) -> Any)(pair.second!!) })
        } else if (op is CurryFunc) {
            return op
//        } else {
//            return CurryFunc (op as (Any) -> Any)
        } else {
            return CurryFunc (op as (Any) -> Any)
        }
    }

    fun to_int(x: Any): Long {
        if (x is ExprElem.NumberElem) {
            return x.value.toLong() //hack-hack
        } else if (x is Long) {
            return x
        } else if (x is List<*>) {
            assert(x.size == 1)
            return to_int(x[0]!!)
        } else if (x is Pair<*, *> && (x as Pair<*, *>).second is ExprElem.Nil) {
            return to_int((x as Pair<*, *>).first!!)
        } else if (x is ExprElem.UnknownOp) {
            val opCode = (x as ExprElem.UnknownOp).opCode
            return to_int(context[opCode]!!)
        } else {
            throw  RuntimeException("Unexpected to_int arg '$x'")
        }
    }

    fun isTrivialOp(startOpCode: Int): Boolean {
        var marked: HashSet<Int> = HashSet<Int>()
        var isTrivial = true
        marked.add(startOpCode)
        fun dfs(opCode: Int) {
            for (expr in context[opCode]!!) {
                if (expr is ExprElem.UnknownOp) {
                    if (marked.contains(expr.opCode)) {
                        isTrivial = false
                        return
                    }
                    marked.add(expr.opCode)
                    dfs(expr.opCode)
                }
            }
        }
        dfs(startOpCode)
        return isTrivial
    }

    fun initTrivialCache() {
        for (opCode in context.keys) {
            isTrivialCache.set(opCode, isTrivialOp(opCode))
        }
    }

}


