package icfpc.expr

import java.math.BigDecimal
import java.math.RoundingMode


data class TreeExprNode(val expr: TreeExpr, val children: MutableList<TreeExprNode>) {
    constructor(expr: TreeExpr) : this(expr, arrayListOf()) {
    }

    private var cachedRes: TreeExpr? = null
    fun evalExpr(): TreeExpr {
        if (children.isEmpty()) {
            return expr
        }
        if (cachedRes == null) {
            cachedRes = evalExprImpl()
        }
        return cachedRes!!
    }

    private fun evalExprImpl(): TreeExpr {
//        val args: List<TreeExprNode> = children.map { it -> { -> it.evalExpr() } }
//        return expr.evalExprWithArgs(args)
        return expr.evalExprWithArgs(children)
    }

    val isFull: Boolean
        get() {
            return expr.expectedArgsCount == children.size
        }

    fun newByAddingArg(arg: TreeExprNode): TreeExprNode {
        val copy = TreeExprNode(expr)
        copy.children.addAll(children)
        copy.children.add(arg)
        return copy
    }

    fun newByAddingArgs(args: List<TreeExprNode>): TreeExprNode {
        val copy = TreeExprNode(expr)
        copy.children.addAll(children)
        copy.children.addAll(args)
        return copy
    }
}

sealed class TreeExpr {

    abstract fun evalExprWithArgs(args: List<TreeExprNode>): TreeExpr


    abstract val expectedArgsCount: Int

    ////////////////////////////////////////////////////////////////////////////////////
    data class NumberElem(val value: Long) : TreeExpr() {

        override val expectedArgsCount: Int
            get() = 0

        override fun evalExprWithArgs(args: List<TreeExprNode>): TreeExpr {
            if (args.size != 0) {
                throw EvalException("Unexpected number of arguments $args")
            }
            return this
        }

        override fun toString(): String {
            return value.toString()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BooleanElem protected constructor(val value: Boolean) : BaseOp() {

        override val expectedArgsCount: Int
            get() = 2

        override fun evalExprWithArgs(args: List<TreeExprNode>): TreeExpr {
            if (args.size == 0) {
                return this
            } else if (args.size == 2) {
                return if (value) args[0].evalExpr() else args[1].evalExpr()
            } else {
                throw EvalException("Unexpected number of arguments $args")
            }
        }

        override fun toString(): String {
            return if (value) "t" else "f"
        }
    }

    object TrueElem : BooleanElem(true)

    object FalseElem : BooleanElem(false)


    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BaseOp : TreeExpr() {
    }


    abstract class BaseOp1() : BaseOp() {

        override val expectedArgsCount: Int
            get() = 1

        override fun evalExprWithArgs(args: List<TreeExprNode>): TreeExpr {
            if (args.size == 1) {
                return eval1(args[0])
            } else {
                throw EvalException("Unexpected number of arguments $args")
            }
        }

        abstract fun eval1(arg: TreeExprNode): TreeExpr
    }

    abstract class EagerOp1() : BaseOp1() {
        final override fun eval1(arg1: TreeExprNode): TreeExpr {
            return eagerEval1(arg1.evalExpr())
        }

        abstract fun eagerEval1(arg1: TreeExpr): TreeExpr
    }

    abstract class BaseOp2() : BaseOp() {
        override val expectedArgsCount: Int
            get() = 2

        override fun evalExprWithArgs(args: List<TreeExprNode>): TreeExpr {
            if (args.size == 2) {
                return eval2(args[0], args[1])
            } else {
                throw EvalException("Unexpected number of arguments $args")
            }
        }

        abstract fun eval2(arg1: TreeExprNode, arg2: TreeExprNode): TreeExpr
    }

    abstract class EagerOp2() : BaseOp2() {
        final override fun eval2(arg1: TreeExprNode, arg2: TreeExprNode): TreeExpr {
            return eagerEval2(arg1.evalExpr(), arg2.evalExpr())
        }

        abstract fun eagerEval2(arg1: TreeExpr, arg2: TreeExpr): TreeExpr
    }

    abstract class BaseOp3() : BaseOp() {
        override val expectedArgsCount: Int
            get() = 3

        override fun evalExprWithArgs(args: List<TreeExprNode>): TreeExpr {
            if (args.size == 3) {
                return eval3(args[0], args[1], args[2])
            } else {
                val size = args.size
                throw EvalException("Unexpected number of arguments $size $args")
            }
        }

        abstract fun eval3(arg1: TreeExprNode, arg2: TreeExprNode, arg3: TreeExprNode): TreeExpr
    }


    ////////////////////////////////////////////////////////////////////////////////////

    object EqOp : EagerOp2() {
        override fun eagerEval2(arg1: TreeExpr, arg2: TreeExpr): TreeExpr {
            return if (arg1 == arg2) TrueElem else FalseElem
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////
    // According to the specification in the message12 it is strictly less
    // 0 < 0 === False
    // 0 < 1 === True
    object Less : EagerOp2() {
        override fun eagerEval2(arg1: TreeExpr, arg2: TreeExpr): TreeExpr {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                return if (arg1.value < arg2.value) TrueElem else FalseElem
            } else {
                throw EvalException("Unexpected TreeExprs $arg1 or $arg2")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    /**
     * This TreeExpr means that after that follows another [Apply] or some Op-glyph (a subclass of [BaseOp])
     * and then the same number of parameters as there were [Apply] TreeExprs.
     * For example:
     * ap Dec 1 => 0
     * ap ap Plus 1 2 => 3
     */
//    object Apply : TreeExpr() {
//        override fun evalExprImpl(): TreeExpr {
////            println("Evaluating Apply ${this} rest = ${followingExpression}")
//            val first = followingExpression[0]
//            val rest = followingExpression.subList(1, followingExpression.size)
//            val firstEv = if (first is Apply) first.evalExprImpl() else EvalResult(first, rest)
//            val evalResult = firstEv.res.evalExprImpl(firstEv.followingExpression, context)
//            return evalResult
//        }
//    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Inc : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            if (arg1 is NumberElem) {
                return NumberElem(arg1.value + 1)
            } else {
                throw EvalException("Unexpected TreeExpr $arg1")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Dec : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            if (arg1 is NumberElem) {
                return NumberElem(arg1.value - 1)
            } else {
                throw EvalException("Unexpected TreeExpr $arg1")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Pwr2 : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            if (arg1 is NumberElem) {
                return NumberElem(1L shl arg1.value.toInt())
            } else {
                throw EvalException("Unexpected TreeExpr $arg1")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Nil : BaseOp1() {
        override fun eval1(arg: TreeExprNode): TreeExpr {
            return TrueElem
        }

        override fun toString(): String {
            return "Nil"
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object IsNil : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            return if (Nil == arg1) TrueElem else FalseElem
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Id : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            return arg1
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////
    object Plus : EagerOp2() {
        override fun eagerEval2(arg1: TreeExpr, arg2: TreeExpr): TreeExpr {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                return NumberElem(arg1.value + arg2.value)
            } else {
                throw EvalException("Unexpected TreeExprs $arg1 or $arg2")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Mul : EagerOp2() {
        override fun eagerEval2(arg1: TreeExpr, arg2: TreeExpr): TreeExpr {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                return NumberElem(arg1.value * arg2.value)
            } else {
                throw EvalException("Unexpected TreeExprs $arg1 or $arg2")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // 5 / -3 === -1
    // -5 / 3 === -1
    // -5 / -3 === 1
    // so the rounding seems always to be towards 0
    object Div : EagerOp2() {
        override fun eagerEval2(arg1: TreeExpr, arg2: TreeExpr): TreeExpr {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                val div = BigDecimal(arg1.value).divide(BigDecimal(arg2.value), RoundingMode.DOWN).toLong()
                return NumberElem(arg1.value / arg2.value)
            } else {
                throw EvalException("Unexpected TreeExprs $arg1 or $arg2")
            }
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////
    object ConsOp : EagerOp2() {
        override fun eagerEval2(arg1: TreeExpr, arg2: TreeExpr): TreeExpr {
            return ConsElem(arg1, arg2)
        }

        override fun toString(): String {
            return "cons"
        }
    }

    data class ConsElem(val head: TreeExpr, val tail: TreeExpr) : TreeExpr() {
        override val expectedArgsCount: Int
            get() = 2

        override fun evalExprWithArgs(args: List<TreeExprNode>): TreeExpr {
            if (args.size != 0) {
                throw EvalException("Unexpected number of arguments $args")
            }
            return this
        }
    }

    object CarOp : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            if (arg1 is ConsElem) {
                return arg1.head
            } else {
                throw EvalException("Unexpected TreeExprs $arg1")
            }
        }
    }

    object CdrOp : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            if (arg1 is ConsElem) {
                return arg1.tail
            } else {
                throw EvalException("Unexpected TreeExpr $arg1")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // neg 0 === 0
    // neg 1 === -1
    // neg -1 === 1
    object Neg : EagerOp1() {
        override fun eagerEval1(arg1: TreeExpr): TreeExpr {
            if (arg1 is NumberElem) {
                return NumberElem(-arg1.value)
            } else {
                throw EvalException("Unexpected TreeExpr $arg1")
            }
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////

    object SCombinatorOp : BaseOp3() {

        override fun eval3(larg1: TreeExprNode, larg2: TreeExprNode, larg3: TreeExprNode): TreeExpr {
            if (larg1.expr is BaseOp && larg2.expr is BaseOp) {
                val innerNode = larg2.newByAddingArg(larg3)
                val outerNode = larg1.newByAddingArgs(listOf(larg3, innerNode))
                return outerNode.evalExpr()
            } else {
                throw EvalException("Unexpected TreeExprs ${larg1.expr} or ${larg2.expr}")
            }

        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object BCombinatorOp : BaseOp3() {
        override fun eval3(larg1: TreeExprNode, larg2: TreeExprNode, larg3: TreeExprNode): TreeExpr {
            if (larg1.expr is BaseOp && larg2.expr is BaseOp) {
                val innerNode: TreeExprNode = larg2.newByAddingArg(larg3)
                val outerNode = larg1.newByAddingArg(innerNode)
                return outerNode.evalExpr()
            } else {
                throw EvalException("Unexpected TreeExprs ${larg1.expr} or ${larg2.expr}")
            }
        }

        override fun toString(): String {
            return "b"
        }

    }
    ////////////////////////////////////////////////////////////////////////////////////

    object CCombinatorOp : BaseOp3() {
        override fun eval3(larg1: TreeExprNode, larg2: TreeExprNode, larg3: TreeExprNode): TreeExpr {
            if (larg1.expr is BaseOp) {
                // maybe we should join two Base1Op?
                val outerNode = larg1.newByAddingArgs(listOf(larg3, larg2))
                return outerNode.evalExpr()
            } else {
                throw EvalException("Unexpected TreeExprs ${larg1.expr}")
            }
        }

        override fun toString(): String {
            return "c"
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////
}
