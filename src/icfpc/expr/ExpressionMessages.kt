package icfpc.expr

import java.math.BigInteger


object ExpressionMessages {

    private fun testEval(expected: ExprElem, expression: List<ExprElem>) {
        val res = ExprElem.evalExpr(expression, null)
        if (res != expected) {
            println("Expected = $expected")
            println("Actual = $res")
            println(expression)
            throw RuntimeException("bad expression test")
        }

    }

    private fun testEval(expected: BigInteger, expression: List<ExprElem>) {
        testEval(ExprElem.NumberElem(expected), expression)
    }

    ////////////////////////////////////////////////////////////

    fun message18() {
        testEval(
            BigInteger("42"), listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.SCombinatorOp,
                ExprElem.Mul,
                ExprElem.Apply,
                ExprElem.Plus,
                ExprElem.NumberElem(BigInteger.ONE),
                ExprElem.NumberElem(BigInteger("6"))
            )
        )
        testEval(
            BigInteger("3"),
            listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.SCombinatorOp,
                ExprElem.Plus,
                ExprElem.Inc,
                ExprElem.NumberElem(BigInteger.ONE)
            )
        )

    }

    ////////////////////////////////////////////////////////////

    fun message19() {
        testEval(
            BigInteger("3"), listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.CCombinatorOp,
                ExprElem.Plus,
                ExprElem.NumberElem(BigInteger.ONE),
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )
    }

    ////////////////////////////////////////////////////////////

    fun message20() {
        testEval(
            BigInteger.valueOf(2), listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.BCombinatorOp,
                ExprElem.Inc,
                ExprElem.Dec,
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )

        testEval(
            BigInteger("-1"), listOf(
                ExprElem.Apply,
                ExprElem.Inc,
                ExprElem.Apply,
                ExprElem.Neg,
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )
        testEval(
            BigInteger("-3"), listOf(
                ExprElem.Apply,
                ExprElem.Neg,
                ExprElem.Apply,
                ExprElem.Inc,
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )
        testEval(
            BigInteger("-1"), listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.BCombinatorOp,
                ExprElem.Inc,
                ExprElem.Neg,
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )
    }

    ////////////////////////////////////////////////////////////

    fun message21() {
        testEval(
            BigInteger("42"), listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.TrueElem,
                ExprElem.NumberElem(BigInteger("42")),
                ExprElem.NumberElem(BigInteger("24"))
            )
        )
        testEval(
            ExprElem.TrueElem, listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.TrueElem,
                ExprElem.TrueElem,
                ExprElem.NumberElem(BigInteger("24"))
            )
        )
    }

    ////////////////////////////////////////////////////////////
    fun message22() {
        testEval(
            BigInteger("24"), listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.FalseElem,
                ExprElem.NumberElem(BigInteger("42")),
                ExprElem.NumberElem(BigInteger("24"))
            )
        )
        testEval(
            ExprElem.FalseElem, listOf(
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.FalseElem,
                ExprElem.NumberElem(BigInteger("42")),
                ExprElem.FalseElem
            )
        )
    }

    ////////////////////////////////////////////////////////////
    fun message23() {
        testEval(
            BigInteger("4"), listOf(
                ExprElem.Apply,
                ExprElem.Pwr2,
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )
        testEval(
            BigInteger("8"), listOf(
                ExprElem.Apply,
                ExprElem.Pwr2,
                ExprElem.NumberElem(BigInteger.valueOf(3))
            )
        )
        testEval(
            BigInteger("256"), listOf(
                ExprElem.Apply,
                ExprElem.Pwr2,
                ExprElem.NumberElem(BigInteger.valueOf(8))
            )
        )
    }

    ////////////////////////////////////////////////////////////
    fun message24() {
        testEval(
            BigInteger.valueOf(2), listOf(
                ExprElem.Apply,
                ExprElem.Id,
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )
        testEval(
            ExprElem.Plus, listOf(
                ExprElem.Apply,
                ExprElem.Id,
                ExprElem.Plus
            )
        )
    }
    ////////////////////////////////////////////////////////////

    fun message26() {
        testEval(
            ExprElem.Plus, listOf(
                ExprElem.Apply,
                ExprElem.CarOp,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.ConsOp,
                ExprElem.Plus,
                ExprElem.Nil
            )
        )
        testEval(
            ExprElem.NumberElem(BigInteger("3")), listOf(
                ExprElem.Apply,
                ExprElem.CarOp,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.ConsOp,
                ExprElem.NumberElem(BigInteger("3")),
                ExprElem.Nil
            )
        )
    }
    ////////////////////////////////////////////////////////////

    fun message27() {
        testEval(
            ExprElem.Nil, listOf(
                ExprElem.Apply,
                ExprElem.CdrOp,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.ConsOp,
                ExprElem.Plus,
                ExprElem.Nil
            )
        )
        testEval(
            ExprElem.Nil, listOf(
                ExprElem.Apply,
                ExprElem.CdrOp,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.ConsOp,
                ExprElem.NumberElem(BigInteger("3")),
                ExprElem.Nil
            )
        )
        testEval(
            ExprElem.NumberElem(BigInteger("3")), listOf(
                ExprElem.Apply,
                ExprElem.CdrOp,
                ExprElem.Apply,
                ExprElem.Apply,
                ExprElem.ConsOp,
                ExprElem.Nil,
                ExprElem.NumberElem((BigInteger("3")))
            )
        )
    }

    ////////////////////////////////////////////////////////////
    fun message28() {
        testEval(
            ExprElem.TrueElem, listOf(
                ExprElem.Apply,
                ExprElem.Nil,
                ExprElem.NumberElem(BigInteger.valueOf(2))
            )
        )
    }

    ////////////////////////////////////////////////////////////
    fun message29() {
        testEval(
            ExprElem.TrueElem, listOf(
                ExprElem.Apply,
                ExprElem.IsNil,
                ExprElem.Nil
            )
        )
        testEval(
            ExprElem.FalseElem, listOf(
                ExprElem.Apply,
                ExprElem.IsNil,
                ExprElem.Plus
            )
        )
    }
}


fun main(args: Array<String>) {
//*
    ExpressionMessages.message18()
    ExpressionMessages.message19()
    ExpressionMessages.message20()
    ExpressionMessages.message21()
    ExpressionMessages.message22()
    ExpressionMessages.message23()
    ExpressionMessages.message24()
    //TODO: add 25-27 cons/car/cdr
    ExpressionMessages.message26()
    ExpressionMessages.message27()
    ExpressionMessages.message28()
    ExpressionMessages.message29()
//*/

    println("Everything is fine")
}