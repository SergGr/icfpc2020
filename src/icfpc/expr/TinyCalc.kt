package icfpc.expr

import icfpc.network.AliensApi
import icfpc.player.visualizer.Visualizer
import java.io.File
import java.math.BigInteger
import kotlin.math.absoluteValue


sealed class Expr(var evaluated: Expr?) {
    override fun toString(): String {
        return "Expr(evaluated=$evaluated)"
    }
    fun modulate(): String {
        return TinyModulator.encode(this)
    }
}

abstract class BaseAtom() : Expr(null) {
    abstract val name: String

}

data class Atom(override val name: String) : BaseAtom() {
    override fun toString(): String {
        return "Atom(name='$name')"
    }
}

data class IntAtom(var value: BigInteger) : BaseAtom() {
    constructor(value: Int) : this(BigInteger.valueOf(value.toLong())) {

    }

    override val name: String
        get() = "value"

    override fun toString(): String {
        return "IntAtom(value=$value)"
    }
}

data class Ap(var func: Expr, var arg: Expr) : Expr(null) {
    override fun toString(): String {
        return "Ap(func=$func, arg=$arg)"
    }
}

//class Vect(var x: BigInteger, var y: BigInteger) {}

val cons = Atom("cons")
val t = Atom("t")
val f = Atom("f")
val nil = Atom("nil")

fun tinyParseText(lines: List<String>): List<List<String>> {
    var out = ArrayList<List<String>>()
    for (line in lines) {
        out.add(line.split(" "))
    }
    return out
}

fun consHead(expr: Ap): Expr {
    return (expr.func as Ap).arg
}

fun consTail(expr: Ap): Expr {
    return expr.arg
}

fun printCons(expr: Expr): String {
    val sb = StringBuilder()
    printCons(sb, expr)
    return sb.toString()
}

fun printCons(sb: StringBuilder, expr: Expr) {
    if (expr is IntAtom) {
        sb.append(expr.value.toString())
    } else if (expr is Atom) {
        sb.append(expr.name)
    } else if (expr is Ap) {
        sb.append('(')
        printCons(sb, consHead(expr))
        sb.append(',')
        printCons(sb, consTail(expr))
        sb.append(')')
    } else {
        TODO("Invalid cons expr= $expr")
    }
}


class ConsParser(val s: String) {
    companion object {
        fun parse(s: String): Expr {
//            println("Parsing string '$s'")
//            val res = ConsParser(s).parse()
//            println(res)
//            println(printCons(res))
//            return res
            return ConsParser(s).parse()
        }
    }

    var pos = 0

    fun parse(): Expr {
        while (Character.isWhitespace(s[pos])) {
            pos++
        }
        val ch = s[pos]
        val expr = when (ch) {
            '(' -> parseCons()
            '-' -> parseNumber()
            'n' -> {
                pos += 3
                nil
            }
            in '0'..'9' -> parseNumber()
            else -> throw RuntimeException("Unexpected char '$ch'")
        }
        return expr

    }

    val endChars: CharArray = charArrayOf(',', ')')
    private fun parseNumber(): Expr {
        val end = s.indexOfAny(endChars, pos)
        val valS = s.substring(pos, end)
        val intValue = BigInteger(valS)
        pos = end
        return IntAtom(intValue)
    }

    private fun parseCons(): Expr {
        pos++ // (
        val head = parse()
        pos++ // ,
        val tail = parse()
        pos++ // )
//        val innerAp = Ap(cons, head)
//        val outerAp = Ap(innerAp, tail)
        //return outerAp
        return Ap(Ap(cons, head), tail)
    }
}


fun tinyEvaluateAll(lines: List<List<String>>, pass_params: Boolean = false) {
    val functions = prepareFunctions(lines)
    if (pass_params) {
        val event = parseFunction("ap ap cons 42 ap ap cons 43 nil".split(" ").asIterable().iterator())
        val inp = Ap(Ap(Atom("galaxy"), Atom("nil")), event)
        val expr = TinyEvaler(functions).eval(inp)

//        println("Galaxy (params) = $expr")
        val flag = consHead(expr as Ap)
        println("flag = $flag")
        val newState = consHead(consTail(expr) as Ap)
//        println("newState = $newState")
        val data = consHead(consTail(consTail(expr) as Ap) as Ap)
//        println("data = $data")
        val sb = StringBuilder()
        printCons(sb, data)
        println("data = $sb")

        //        var [flag, newState, data] = GET_LIST_ITEMS_FROM_EXPR(res)
        //    if (asNum(flag) == 0)
        //        return (newState, data)
        //    return interact(newState, SEND_TO_ALIEN_PROXY(data))
        val mod = TinyModulator.encode(data)
        println("Modulated = $mod")
    } else {
        val expr = TinyEvaler(functions).eval(functions["galaxy"]!!)
        println("Galaxy = $expr")

    }
}

fun prepareFunctions(lines: List<List<String>>): HashMap<String, Expr> {
    var context = HashMap<String, List<String>>()
    for (line in lines) {
        if (line.size < 3 || line[1] != "=") {
            throw RuntimeException("Unexpected op in $line")
        }
        context.put(line[0], line.subList(2, line.size))
    }
    return parseFunctions(context)
}

fun asNum(eval: Expr): BigInteger {
    if (eval is IntAtom) {
        return eval.value
    } else if (eval is Atom) {
        return BigInteger(eval.name)
//        return if (eval.name == "nil") BigInteger.ZERO else BigInteger(eval.name)
//    } else if (eval is Ap) {
//        assert ((eval.func as Ap).func == cons)
//        return asNum(consHead(eval))
    } else {
        TODO("Not a number $eval")
    }
}

fun asNumPair(expr: Ap): Pair<BigInteger, BigInteger> {
    return Pair(asNum(consHead(expr)), asNum(consTail(expr)))
}

data class TinyEvaler(var functions: HashMap<String, Expr>) {
    fun eval(arg: Expr): Expr {
        var expr = arg
        if (expr.evaluated != null)
            return expr.evaluated!!
        var initialExpr = expr
        while (true) {
            var result = tryEval(expr)
            if (result == expr) {
                initialExpr.evaluated = result
                return result
            }
            expr = result
        }
    }

    fun tryEval(expr: Expr): Expr {
        if (expr.evaluated != null)
            return expr.evaluated!!
        if (expr is Atom && functions[expr.name] != null)
            return functions[expr.name]!!
        if (expr is Ap) {
            val func = eval(expr.func)
            val x = expr.arg
            if (func is BaseAtom) {
                if (func.name == "neg") return IntAtom(-asNum(eval(x)))
                if (func.name == "inc") return IntAtom(asNum(eval(x)) + BigInteger.ONE)
                if (func.name == "i") return x
                if (func.name == "nil") return t
                if (func.name == "isnil") return Ap(x, Ap(t, Ap(t, f)))
                if (func.name == "car") return Ap(x, t)
                if (func.name == "cdr") return Ap(x, f)
            }
            if (func is Ap) {
                val func2 = eval(func.func)
                val y = func.arg
                if (func2 is BaseAtom) {
                    if (func2.name == "t") return y
                    if (func2.name == "f") return x
                    if (func2.name == "add") return IntAtom(asNum(eval(x)) + asNum(eval(y)))
                    if (func2.name == "mul") return IntAtom(asNum(eval(x)) * asNum(eval(y)))
                    if (func2.name == "div") return IntAtom(asNum(eval(y)).div(asNum(eval(x))))
                    if (func2.name == "lt") return if (asNum(eval(y)) < asNum(eval(x))) t else f
                    if (func2.name == "eq") return if (asNum(eval(x)) == asNum(eval(y))) t else f
                    if (func2.name == "cons") return evalCons(y, x)
                }
                if (func2 is Ap) {
                    val func3 = eval(func2.func)
                    val z = func2.arg
                    if (func3 is Atom) {
                        if (func3.name == "s") return Ap(Ap(z, x), Ap(y, x))
                        if (func3.name == "c") return Ap(Ap(z, x), y)
                        if (func3.name == "b") return Ap(z, Ap(y, x))
                        if (func3.name == "cons") return Ap(Ap(x, z), y)
                    }
                }
            }
        }
        return expr

    }

    private fun evalCons(a: Expr, b: Expr): Expr {
        var res = Ap(Ap(cons, eval(a)), eval(b))
        res.evaluated = res
        return res
    }

    fun interact(state: Expr, event: Expr, sendToProxy: Boolean = true): Pair<Expr, Expr> {
        val expr = Ap(Ap(Atom("galaxy"), state), event)
        val res = eval(expr)
        val flag = consHead(res as Ap)
        val newState = consHead(consTail(res) as Ap)
        val data = consHead(consTail(consTail(res) as Ap) as Ap)
        if (asNum(flag) == BigInteger.ZERO) {
            return Pair(newState, data)
        } else {
            if (sendToProxy)
                return interact(newState, sendToProxy(state, data))
            else {
                println("Fake send data at ${state.hashCode()}: ${printCons(state)}")
                println("Fake send data: ${printCons(data)}")
                return Pair(newState, data)
            }
        }
    }

    open fun sendToProxy(state: Expr, data: Expr): Ap {
        println("Trying to send data at ${state.hashCode()}: ${printCons(state)}")
        println("Trying to send data: ${printCons(data)}")
        val ret = AliensApi().sendToAliens(TinyModulator.encode(data))
        val resp = TinyModulator.decode(ret!!) as Ap
        val sb = StringBuilder()
        printCons(sb, resp)
        println("Aliens Response=$sb")
        return resp
    }
}


fun parseFunctions(context: HashMap<String, List<String>>): HashMap<String, Expr> {
    var functions = HashMap<String, Expr>()
    for (name in context.keys) {
        functions.set(name, parseFunction(context[name]!!.asIterable().iterator()))
    }
    return functions
}


fun parseFunction(iter: Iterator<String>): Expr {
    val head = iter.next()
    if (head == "ap") {
        return parseFunctionAp(iter)
    } else {
        return Atom(head)
    }
}

fun parseFunctionAp(iter: Iterator<String>): Expr {
    val input1 = parseFunction(iter)
    val input2 = parseFunction(iter)
    return Ap(input1, input2)
}


fun main(args: Array<String>) {
//    // click repeatedly.
//    var finalResult = ArrayList<String>()
//    playTheGame({ images ->
//        val l = consToList(images)
//        var out = ArrayList<String>()
//        for (image in l) {
//            val sb = StringBuilder()
//            sb.append("[")
//            val points = consToList(image)
//            for ((i, point) in points.withIndex()) {
//                printCons(sb, point)
//                if (i != points.size - 1) sb.append(", ")
//            }
//            sb.append("]")
//            out.add(sb.toString())
//        }
//        finalResult.add("[" + out.joinToString(", ") + "]")
//        Pair(0, 0)
//    }, 20)
//    println("["+finalResult.joinToString(", ")+"]")
    playTheGame({ x -> printAndReadClicks(x) })
//    tinyEvaluateAll(
//            tinyParseText(
//                    listOf(
//                            ":1029 = ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil",
////                    "galaxy = ap ap :1029 nil ap ap cons 42 ap ap cons 43 nil"
//                            "galaxy = ap ap :1029 42 ap ap cons 43 nil"
//                    )
//            )
//    )
//    tinyEvaluateAll(
//            tinyParseText(
//                    listOf(
//                            ":1029 = ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil",
//                            "galaxy = :1029"
//                    )
//            ), true
//    )

    val lines: List<String> = File("./data/messages/galaxy.v2.txt").readLines()
    tinyEvaluateAll(tinyParseText(lines), true)

//    tinyEvaluateAll(
//            tinyParseText(
//                    listOf(
//                            ":2048 = div 2 4",
//                            ":2049 = ap f :2049",
////                            "galaxy = ap ap ap c :2048"
//                            "galaxy = ap :2049 43"
//                    )
//            )
//    )

//    tinyEvaluateAll(
//            tinyParseText(
//                    listOf(
//                            "galaxy = ap ap ap s mul ap add 1 6"
//                    )
//            )
//    )

}

fun printAndReadClicks(images: Expr): Pair<Int, Int> {
    printImages(images)
    println("new click")
    val inp = readLine()!!.split(' ')
    return Pair(inp[0].toInt(), inp[1].toInt())
}

fun playTheGame(userCallback: (Expr) -> Pair<Int, Int>, n: Int = 1000000) {
    val lines: List<String> = File("./data/messages/galaxy.v2.txt").readLines()
    val functions = prepareFunctions(tinyParseText(lines))
    var evaler = TinyEvaler(functions)
    var x = 0
    var y = 0
    var state: Expr = nil
    for (i in 0..n) {
//        var click = parseFunction("ap ap cons $x ap ap cons $y nil".split(" ").asIterable().iterator())
        val click = Ap(Ap(cons, Atom(x.toString())), Atom(y.toString()))
        var (newState, images) = evaler.interact(state, click)
        val pair = userCallback(images)
        x = pair.first
        y = pair.second
//        PRINT_IMAGES(images)
//        vector = REQUEST_CLICK_FROM_USER()
        state = newState
    }
}

fun consToList(inp: Expr): List<Expr> {
    var expr = inp
    var ret = ArrayList<Expr>()
    while (true) {
        if (expr is Atom && expr.name == "nil") break
        ret.add(consHead(expr as Ap))
        expr = consTail(expr as Ap)
    }
    return ret
}

fun printImages(images: Expr) {
    val l = consToList(images)
    for (image in l) {
        val sb = StringBuilder()
        sb.append("[")
        val points = consToList(image)
        for ((i, point) in points.withIndex()) {
            printCons(sb, point)
            if (i != points.size - 1) sb.append(", ")
        }
        sb.append("]")
        println("add_points($sb)")
    }
//    println("images=$l")
}


data class AlienImage(val points: List<IntPoint>) {

    companion object {
        val emptyImage = AlienImage(listOf(IntPoint(-1, -1), IntPoint(0, 0), IntPoint(1, 1)))
    }

    val minX: Int
    val minY: Int
    val maxX: Int
    val maxY: Int
    val numLayers: Int

    init {
        var localMinX = Int.MAX_VALUE
        var localMinY = Int.MAX_VALUE
        var localMaxX = Int.MIN_VALUE
        var localMaxY = Int.MIN_VALUE
        var localNumLayers = 0
        for (p in points) {
            if (p.x < localMinX) localMinX = p.x
            if (p.y < localMinY) localMinY = p.y
            if (p.x > localMaxX) localMaxX = p.x
            if (p.y > localMaxY) localMaxY = p.y
            if (p.layer > localNumLayers) localNumLayers = p.layer
        }
        numLayers = localNumLayers + 1
        minX = localMinX
        minY = localMinY
        maxX = localMaxX
        maxY = localMaxY
    }
}

data class HotPoints(val hotPoints: MutableList<IntPoint>, val state: Expr, val inProgress: Boolean) {

}


class GameEngine() {
    data class FullState(val point: Pair<Int, Int>, val state: Expr, val images: Expr)

    val statesHistory: MutableList<FullState> = ArrayList()

    val initState: Expr = nil
    // galaxy
//    val initState: Expr = ConsParser.parse("(1,((11,nil),(0,(nil,nil))))")
    // first send
//    val initState: Expr = ConsParser.parse("(2,((1,(-1,nil)),(0,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29976,nil)))))))),(0,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29966,nil)))))))),(1,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29957,nil)))))))),(3,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29951,nil)))))))),(6,(nil,nil))))")

//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29951,nil)))))))),(7,(nil,nil))))")

//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29933,nil)))))))),(8,(nil,nil))))")


    // multiplayer
//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29933,nil)))))))),(9,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((3,(0,(nil,(nil,(nil,(nil,((36,0),(29902,nil)))))))),(9,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((4,(3760358461917263994,(nil,(nil,(nil,(nil,((36,0),(29902,nil)))))))),(9,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((2,(0,(nil,(nil,(nil,(nil,(nil,(29933,nil)))))))),(9,(nil,nil))))")
//    val initState: Expr = ConsParser.parse("(5,((4,(5250670092951256026,(nil,(nil,(nil,(nil,((36,0),(29902,nil)))))))),(9,(nil,nil))))")


    //(5,((4,(126,(nil,(nil,(nil,(nil,((36,0),(29670,nil)))))))),(9,(nil,nil))))

//    val initState: Expr = ConsParser.parse("(6,((0,(12,(238193943001947139,(2,(0,(2,(nil,(nil,(4,((14,((16,(128,nil)),((((1,(0,((18,46),((2,-3),((170,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((2,((18,-11),(24,(16,(4,nil))))),((0,((0,1),nil)),nil)),nil)),(((0,(1,((18,-11),((4,7),((101,(40,(20,(1,nil)))),(109,(128,(2,nil)))))))),(((2,((18,47),(40,(63,(4,nil))))),nil),nil)),nil)),nil))),(nil,(((16,(128,nil)),(((0,((((1,(0,((48,24),((0,0),((206,(30,(10,(1,nil)))),(0,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-48,-24),((0,0),((110,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(nil,nil)),nil)),nil)),((1,((((1,(0,((46,25),((-2,1),((205,(30,(10,(1,nil)))),(0,(64,(1,nil)))))))),(((0,((1,-1),nil)),nil),nil)),(((0,(1,((-46,-24),((2,0),((109,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(((0,((-1,0),nil)),nil),nil)),nil)),nil)),((2,((((1,(0,((42,27),((-4,2),((204,(30,(10,(1,nil)))),(0,(64,(1,nil)))))))),(((0,((1,-1),nil)),nil),nil)),(((0,(1,((-42,-24),((4,0),((108,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(((0,((-1,0),nil)),nil),nil)),nil)),nil)),((3,((((1,(0,((37,29),((-5,2),((204,(30,(10,(1,nil)))),(0,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-37,-25),((5,-1),((107,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(((0,((0,1),nil)),nil),nil)),nil)),nil)),((4,((((1,(0,((32,32),((-5,3),((203,(30,(10,(1,nil)))),(7,(64,(1,nil)))))))),(((0,((-1,-1),nil)),nil),nil)),(((0,(1,((-32,-27),((5,-2),((106,(40,(20,(1,nil)))),(28,(128,(2,nil)))))))),(((2,((31,31),(40,(39,(4,nil))))),((0,((1,1),nil)),nil)),nil)),nil)),nil)),((5,((((1,(0,((27,35),((-5,3),((202,(30,(10,(1,nil)))),(5,(64,(1,nil)))))))),(((0,((-1,-1),nil)),nil),nil)),(((0,(1,((-27,-30),((5,-3),((105,(40,(20,(1,nil)))),(16,(128,(2,nil)))))))),(((0,((1,1),nil)),nil),nil)),nil)),nil)),((6,((((1,(0,((23,38),((-4,3),((201,(30,(10,(1,nil)))),(3,(64,(1,nil)))))))),(((0,((-1,-1),nil)),nil),nil)),(((0,(1,((-22,-32),((5,-2),((105,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(nil,nil)),nil)),nil)),((7,((((1,(0,((19,41),((-4,3),((200,(30,(10,(1,nil)))),(1,(64,(1,nil)))))))),(((0,((0,-1),nil)),nil),nil)),(((0,(1,((-16,-33),((6,-1),((104,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(((0,((-1,0),nil)),nil),nil)),nil)),nil)),((8,((((1,(0,((16,44),((-3,3),((199,(30,(10,(1,nil)))),(0,(64,(1,nil)))))))),(((0,((-1,-1),nil)),nil),nil)),(((0,(1,((-10,-33),((6,0),((104,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(nil,nil)),nil)),nil)),((9,((((1,(0,((14,47),((-2,3),((198,(30,(10,(1,nil)))),(0,(64,(1,nil)))))))),(((0,((-1,-1),nil)),nil),nil)),(((0,(1,((-5,-33),((5,0),((103,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(((0,((1,1),nil)),nil),nil)),nil)),nil)),((10,((((1,(0,((13,49),((-1,2),((197,(30,(10,(1,nil)))),(0,(64,(1,nil)))))))),(((0,((-1,0),nil)),nil),nil)),(((0,(1,((0,-31),((5,2),((102,(40,(20,(1,nil)))),(0,(128,(2,nil)))))))),(((0,((0,-1),nil)),nil),nil)),nil)),nil)),((11,((((1,(0,((13,50),((0,1),((196,(30,(10,(1,nil)))),(3,(64,(1,nil)))))))),(((0,((-1,0),nil)),nil),nil)),(((0,(1,((5,-28),((5,3),((102,(40,(20,(1,nil)))),(20,(128,(2,nil)))))))),(((2,((12,50),(40,(22,(4,nil))))),nil),nil)),nil)),nil)),((12,((((1,(0,((14,50),((1,0),((195,(30,(10,(1,nil)))),(10,(64,(1,nil)))))))),(((0,((-1,0),nil)),nil),nil)),(((0,(1,((10,-24),((5,4),((102,(40,(20,(1,nil)))),(40,(128,(2,nil)))))))),(((2,((13,50),(40,(38,(4,nil))))),nil),nil)),nil)),nil)),((13,((((1,(0,((16,49),((2,-1),((194,(30,(10,(1,nil)))),(50,(64,(1,nil)))))))),(((2,((15,-19),(30,(21,(4,nil))))),((0,((-1,0),nil)),nil)),nil)),(((0,(1,((14,-18),((4,6),((101,(40,(20,(1,nil)))),(73,(128,(2,nil)))))))),(((2,((15,49),(40,(51,(4,nil))))),((0,((1,-1),nil)),nil)),nil)),nil)),nil)),((14,((((1,(0,((18,46),((2,-3),((170,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((2,((18,-11),(24,(16,(4,nil))))),((0,((0,1),nil)),nil)),nil)),(((0,(1,((18,-11),((4,7),((101,(40,(20,(1,nil)))),(109,(128,(2,nil)))))))),(((2,((18,47),(40,(63,(4,nil))))),nil),nil)),nil)),nil)),((15,((((1,(0,((20,42),((2,-4),((112,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((21,-3),((3,8),((92,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((20,42),(39,(68,(4,nil))))),((0,((0,-1),nil)),nil)),nil)),nil)),nil)),((16,((((1,(0,((22,37),((2,-5),((94,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((22,4),((1,7),((83,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((22,37),(20,(28,(4,nil))))),((0,((1,1),nil)),nil)),nil)),nil)),nil)),((17,((((1,(0,((25,32),((3,-5),((76,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((2,((22,11),(10,(2,(4,nil))))),((0,((-1,-1),nil)),nil)),nil)),(((0,(1,((23,12),((1,8),((74,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((24,31),(20,(36,(4,nil))))),((0,((-1,-1),nil)),nil)),nil)),nil)),nil)),((18,((((1,(0,((28,25),((3,-7),((59,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((2,((23,20),(10,(26,(4,nil))))),((0,((0,1),nil)),nil)),nil)),(((0,(1,((22,21),((-1,9),((59,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((28,26),(20,(35,(4,nil))))),((0,((1,-1),nil)),nil)),nil)),nil)),nil)),((19,((((1,(0,((31,19),((3,-6),((38,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((2,((20,30),(10,(20,(4,nil))))),((0,((-1,-1),nil)),nil)),nil)),(((0,(1,((19,29),((-3,8),((45,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((30,18),(20,(50,(4,nil))))),((0,((1,1),nil)),nil)),nil)),nil)),nil)),((20,((((1,(0,((33,14),((2,-5),((35,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((0,((0,-1),nil)),nil),nil)),(((0,(1,((15,35),((-4,6),((36,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((33,13),(20,(18,(4,nil))))),((0,((1,1),nil)),nil)),nil)),nil)),nil)),((21,((((1,(0,((35,10),((2,-4),((34,(30,(10,(1,nil)))),(62,(64,(1,nil)))))))),(((0,((-1,-1),nil)),nil),nil)),(((0,(1,((10,39),((-5,4),((35,(40,(20,(1,nil)))),(116,(128,(2,nil)))))))),(((0,((1,1),nil)),nil),nil)),nil)),nil)),((22,((((1,(0,((37,7),((2,-3),((26,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((0,((-1,-1),nil)),nil),nil)),(((0,(1,((4,41),((-6,2),((26,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((36,6),(32,(46,(4,nil))))),((0,((1,1),nil)),nil)),nil)),nil)),nil)),((23,((((1,(0,((37,3),((0,-4),((22,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((0,((1,1),nil)),nil),nil)),(((0,(1,((-1,43),((-5,2),((17,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((38,4),(20,(22,(4,nil))))),((0,((-1,-1),nil)),nil)),nil)),nil)),nil)),((24,((((1,(0,((37,-1),((0,-4),((21,(30,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((0,((-1,0),nil)),nil),nil)),(((0,(1,((-6,44),((-5,1),((17,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((36,-1),(20,(8,(4,nil))))),nil),nil)),nil)),nil)),((25,((((1,(0,((36,-5),((-1,-4),((21,(30,(10,(1,nil)))),(61,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-12,45),((-6,1),((8,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((36,-5),(20,(7,(4,nil))))),((0,((1,-1),nil)),nil)),nil)),nil)),nil)),((26,((((1,(0,((35,-10),((-1,-5),((20,(30,(10,(1,nil)))),(59,(64,(1,nil)))))))),(((0,((-1,1),nil)),nil),nil)),(((0,(1,((-18,45),((-6,0),((8,(40,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((34,-9),(20,(3,(4,nil))))),nil),nil)),nil)),nil)),((27,((((1,(0,((33,-15),((-2,-5),((20,(30,(10,(1,nil)))),(49,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-24,44),((-6,-1),((8,(40,(20,(1,nil)))),(108,(128,(2,nil)))))))),(nil,nil)),nil)),nil)),((28,((((1,(0,((30,-20),((-3,-5),((0,(22,(10,(1,nil)))),(64,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-29,41),((-5,-3),((0,(39,(20,(1,nil)))),(128,(128,(2,nil)))))))),(((2,((30,-20),(40,(53,(4,nil))))),((0,((-1,1),nil)),nil)),nil)),nil)),nil)),((29,((((1,(0,((26,-25),((-4,-5),((0,(22,(10,(1,nil)))),(54,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-34,37),((-5,-4),((0,(39,(20,(1,nil)))),(108,(128,(2,nil)))))))),(nil,nil)),nil)),nil)),((30,((((1,(0,((21,-30),((-5,-5),((0,(0,(3,(1,nil)))),(64,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-39,32),((-5,-5),((0,(39,(20,(1,nil)))),(127,(128,(2,nil)))))))),(((2,((21,-30),(39,(49,(4,nil))))),nil),nil)),nil)),nil)),((31,((((1,(0,((16,-34),((-5,-4),((0,(0,(3,(1,nil)))),(61,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-43,27),((-4,-5),((0,(39,(20,(1,nil)))),(107,(128,(2,nil)))))))),(nil,nil)),nil)),nil)),((32,((((1,(0,((11,-37),((-5,-3),((0,(0,(0,(0,nil)))),(64,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((-46,22),((-3,-5),((0,(39,(20,(1,nil)))),(126,(128,(2,nil)))))))),(((2,((11,-37),(39,(52,(4,nil))))),nil),nil)),nil)),nil)),nil))))))))))))))))))))))))))))))))),nil)),(nil,nil))))))))))))),(8,(nil,nil))))")

    var state: Expr = initState

    private val lines: List<String> = File("./data/messages/galaxy.v2.txt").readLines()
    private val functions = prepareFunctions(tinyParseText(lines))
    private val evaler = TinyEvaler(functions)
    var lastImages: Expr = nil

    init {
        val emptyState = nil
        // force initial eval to avoid stack overflow later on a real state
        if (true) {
            val click = Ap(Ap(cons, IntAtom(0)), IntAtom(0))
            val (newState, images) = evaler.interact(emptyState, click)
            lastImages = images
            println("Initial eval succeed")
        }
//        if (state != nil) {
//            val click = Ap(Ap(cons, IntAtom(-100)), IntAtom(-100))
//            val (newState, images) = evaler.interact(state, click)
//            lastImages = images
//            println("Initial real succeed")
//        }
    }

    fun step(point: Pair<Int, Int>): Boolean {
        return stepEx(point, true) != null
    }

    private fun stepEx(point: Pair<Int, Int>, updateHistory: Boolean): Expr? {
        if (updateHistory) {
            println("Processing point $point at state ${state.hashCode()}: ${printCons(state)}")
        }

        val (x, y) = point
        val click = Ap(Ap(cons, Atom(x.toString())), Atom(y.toString()))
        val (newState, images) = evaler.interact(state, click, updateHistory)
        // lastImages check is  a hack for the initial state
        val firstClick: Boolean = updateHistory && (lastImages != nil)
        if (state == newState && !firstClick) {
            return null
        } else {
            if (updateHistory || firstClick) {
                statesHistory.add(FullState(point, state, lastImages))
                state = newState
                lastImages = images
//                println("New state ${lastImages.hashCode()}, state ${state.hashCode()}: ${printCons(state)}")
            }
            return newState
        }
    }


    fun restart(withCheats: Boolean) {
        state = if (withCheats) ConsParser.parse("(0,((0,nil),(0,((192496425430,(103652820,nil)),nil))))") else initState
        lastImages = nil
        statesHistory.clear()
    }

    val canUndo: Boolean
        get() {
            return statesHistory.isNotEmpty()
        }

    fun undoStep(): Boolean {
        if (statesHistory.size == 0)
            return false

        val lastState = statesHistory.removeAt(statesHistory.lastIndex)
        lastImages = lastState.images
        state = lastState.state
        println("Undo to images ${lastState.images.hashCode()}, state ${state.hashCode()}: ${printCons(state)}")
        return true
    }

    fun findHotPoints(progressCallback: ((HotPoints) -> Unit)? = null): HotPoints {
        return findHotPointsSmart(progressCallback)
    }

    fun findHotPointsSmart(progressCallback: ((HotPoints) -> Unit)? = null): HotPoints {
        val origState = state;
        val hotPoints: MutableList<IntPoint> = ArrayList()
        val excludePoints: MutableSet<IntPoint> = HashSet()
        val alienImage = lastAlienImage
        // build points around the image
        val pointsToCheck: MutableSet<IntPoint> = HashSet()
        // always check 0,0
        pointsToCheck.add(IntPoint(0, 0))
        val isBigImage = (alienImage.maxX - alienImage.minX) > 200
        for (p in alienImage.points) {
            val includeDs = 3
            for (dx in -includeDs..includeDs) {
                for (dy in -includeDs..includeDs) {
                    val newP = IntPoint(p.x + dx, p.y + dy)
                    if (!isBigImage || ((newP.x % 2 == 0) && (newP.y % 2 == 0))) {
                        pointsToCheck.add(newP)
                    }
                }
            }
        }

        for (p in pointsToCheck) {
            // early break
            if (origState != state)
                return HotPoints(hotPoints, origState, true)
            if (p in excludePoints)
                continue
            val newState = stepEx(Pair(p.x, p.y), false)
            if (newState != null) {
                hotPoints.add(p)
                if (progressCallback != null) {
                    progressCallback(HotPoints(hotPoints, origState, true))
                }
                val exclDs = 3
                for (dx in -exclDs..exclDs) {
                    for (dy in -exclDs..exclDs) {
                        excludePoints.add(IntPoint(p.x + dx, p.y + dy))
                    }
                }
            }
        }
        return HotPoints(hotPoints, origState, false)
    }

    fun findHotPointsAll(): HotPoints {
        val origState = state;
        val hotPoints: MutableList<IntPoint> = ArrayList()
        val excludePoints: MutableSet<IntPoint> = HashSet()
        val alienImage = lastAlienImage
        for (x in alienImage.minX..alienImage.maxX) {
            for (y in alienImage.minY..alienImage.maxY) {
                // early break
                if (origState != state)
                    return HotPoints(hotPoints, origState, true)
                val p = IntPoint(x, y)
                if (p in excludePoints)
                    continue
                val newState = stepEx(Pair(x, y), false)
                if (newState != null) {
                    hotPoints.add(p)
                    excludePoints.add(p)
                    val ds = 4
                    for (dx in -ds..ds) {
                        for (dy in -ds..ds) {
                            excludePoints.add(IntPoint(x + dx, y + dy))
                        }
                    }

                }
            }
        }
        return HotPoints(hotPoints, origState, false)
    }

    val lastAlienImage: AlienImage
        get() {
            return decodeImage(lastImages)
        }

    private fun decodeImage(images: Expr): AlienImage {
        val l = consToList(images)
        val coordsList: MutableList<IntPoint> = ArrayList()
        if (l.isEmpty()) {
            return AlienImage.emptyImage
        } else {
            var layer = 0
            for (image in l) {
                val points = consToList(image)
                for (point in points) {
                    coordsList.add(consToPoint(point as Ap, layer))
                }
                layer++
            }
        }
        return AlienImage(coordsList)
    }

    private fun consToPoint(point: Ap, layer: Int): IntPoint {
        return IntPoint(
                (asNum(consHead(point))).intValueExact(),
                (asNum(consTail(point))).intValueExact(),
                layer
        )
    }
}


object TinyModulator {
    fun encode(listExpr: Expr): String {
        val sb = StringBuilder()
        append(sb, listExpr)
        return sb.toString()
    }

    private fun append(sb: StringBuilder, elem: Expr) {
        when (elem) {
            is IntAtom -> appendNumber(sb, elem.value)
            is Atom -> if (elem.name == "nil") appendNil(sb) else appendNumber(sb, asNum(elem))
            is Ap -> appendCons(sb, elem)
            else -> throw RuntimeException("Unexpected element $elem")
        }
    }

    private fun appendNil(sb: StringBuilder) {
        sb.append("00")
    }

    private fun appendNumber(sb: StringBuilder, input: BigInteger) {
        assert(input.abs() < BigInteger.valueOf(2).pow(63))
        val value = input.toLong()
        if (value < 0) {
            sb.append("10")
        } else {
            sb.append("01")
        }
        if (value == 0L) {
            sb.append("0")
            return
        }
        val pow = powFromLong(value)
        val pow4 = pow / 4 + 1
        for (i in 0 until pow4)
            sb.append("1")
        sb.append("0")
        val abs = value.absoluteValue
        val binStr = java.lang.Long.toBinaryString(abs)
        for (i in binStr.length until (pow4 * 4)) {
            sb.append(0)
        }
        sb.append(binStr)
    }

    private fun appendCons(sb: StringBuilder, cons: Ap) {
        sb.append("11")
        append(sb, (cons.func as Ap).arg)
        append(sb, cons.arg)
    }

    private fun powFromLong(value: Long): Int {
        if (value == 0L)
            return -1
        val abs = value.absoluteValue
        var pow = 0
        var tmp = abs
        while (tmp > 1) {
            pow += 1
            tmp /= 2
        }
        return pow
    }

    fun decode(msg: String): Expr {
        return TinyDemodulator(msg).expr
    }

    private class TinyDemodulator(val msg: String) {
        private var pos: Int = 0
        val expr: Expr = decode()

        fun decode(): Expr {
            val ch0 = msg[pos]
            val ch1 = msg[pos + 1]
            val prefix = Pair(ch0, ch1)
            return when (prefix) {
                Pair('0', '0') -> {
                    pos += 2
                    nil
                }
                Pair('1', '0') -> Atom(decodeNumber().toString())
                Pair('0', '1') -> Atom(decodeNumber().toString())
                Pair('1', '1') -> decodeCons()
                else -> throw RuntimeException("Unexpected message prefix $prefix")
            }
        }

        private fun decodeNumber(): BigInteger {
            val ch0 = msg[pos]
            val ch1 = msg[pos + 1]
            val isNegative = ch0 == '1'
            val lenStart = pos + 2
            var lenMul4 = 0
            while (msg[lenStart + lenMul4] == '1') {
                lenMul4 += 1
            }
            if (lenMul4 == 0) {
                pos += 2 + 1
                return BigInteger.ZERO
            }

            val valStart = lenStart + lenMul4 + 1
            val len = lenMul4 * 4
            var pow = BigInteger.ONE
            var value = BigInteger.ZERO
            for (x in (len - 1) downTo 0) {
                if (msg[valStart + x] == '1')
                    value += pow
                pow *= BigInteger.valueOf(2)
            }
            if (isNegative)
                value = -value

            pos += 2 + 1 + lenMul4 * 5

            return value
        }

        private fun decodeCons(): Ap {
            pos += 2
            val head = decode()
            val tail = decode()
            var res = Ap(Ap(cons, head), tail)
            return res
        }
    }
}

