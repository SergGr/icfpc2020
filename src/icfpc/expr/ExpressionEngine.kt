package icfpc.expr


import java.math.BigDecimal
import java.math.RoundingMode
import java.io.File
import java.math.BigInteger


class EvalException(msg: String) : RuntimeException(msg)

data class EvalResult(val res: ExprElem, val followingExpression: List<ExprElem>)


sealed class ExprElem {
    companion object {
        fun evalExpr(expression: List<ExprElem>, context: Map<Int, List<ExprElem>>?): ExprElem {
            val first = expression[0]
            val rest = expression.subList(1, expression.size)
            val evalResult = first.evalExprImpl(rest, context ?: HashMap<Int, List<ExprElem>>())
            if (evalResult.followingExpression.isNotEmpty()) {
                println("Expression=$expression")
                println("evalResult.res=$evalResult.res")
                println("evalResult.followingExpression=$evalResult.followingExpression")
                throw EvalException("not full evaluation")
            }
            return evalResult.res
        }

        fun opFromCode(code: Int, failOnUnknown: Boolean = false): ExprElem {
            return when (code) {
                // not really Ops but look similar so parse them here
                0 -> Apply
                2 -> TrueElem
                8 -> FalseElem
//                12 -> MetaEquals

                // regular Ops
                5 -> BCombinatorOp
                7 -> SCombinatorOp
                6 -> CCombinatorOp
                10 -> Neg
                40 -> Div
                146 -> Mul
                365 -> Plus
//                170 -> ConvertNumberToBits
//                341 -> ConvertBitsToNumber
                // TODO: are there others DecBy and IncBy or is it always by 1?
                401 -> Dec
                417 -> Inc
                416 -> Less
                448 -> EqOp
//                174 -> AlienFunction
                else -> if (failOnUnknown) throw RuntimeException("Unknown op-code $code")
                else {
                    System.err.println("Found an unknown op-code $code")
                    UnknownOp(code)
                }
            }
        }

    }

    abstract fun evalExprImpl(followingExpression: List<ExprElem>, context: Map<Int, List<ExprElem>>): EvalResult


    ////////////////////////////////////////////////////////////////////////////////////
    data class NumberElem(val value: BigInteger) : ExprElem() {

        override fun evalExprImpl(followingExpression: List<ExprElem>, context: Map<Int, List<ExprElem>>): EvalResult {
            return EvalResult(this, followingExpression)
        }

        override fun toString(): String {
            return value.toString()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BooleanElem protected constructor(val value: Boolean) : BaseOp2() {
        override fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem {
            return if (value) arg1 else arg2
        }
    }

    object TrueElem : BooleanElem(true)

    object FalseElem : BooleanElem(false)


    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BaseOp : ExprElem() {
        override fun evalExprImpl(followingExpression: List<ExprElem>, context: Map<Int, List<ExprElem>>): EvalResult {
//            println("Evaluating ${this} rest = ${followingExpression}")
            if (followingExpression.size < 1) {
                println("this=$this")
                throw EvalException("Out of bounds")
            }
            val first = followingExpression[0]
            val rest = followingExpression.subList(1, followingExpression.size)
            // another ugly hack to handle Apply precedence
            val firstEv = if (first is Apply) first.evalExprImpl(rest, context) else EvalResult(first, rest)
            val evalRes = eval(firstEv.res, context)
//            return evalRes.evalExprImpl(firstEv.followingExpression)
            return EvalResult(evalRes, firstEv.followingExpression)
        }

        abstract fun eval(arg: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem
    }

    data class UnknownOp(val opCode: Int) : BaseOp() {
        override fun eval(arg: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
//            return ExprElem.evalExpr(context[opCode]!!, context)
            TODO("Not implemented yet")
        }

        override fun evalExprImpl(followingExpression: List<ExprElem>, context: Map<Int, List<ExprElem>>): EvalResult {
            val mergedList: MutableList<ExprElem> = ArrayList()
            mergedList.addAll(context[opCode]!!)
            mergedList.addAll(followingExpression)

            val first = mergedList[0]
            val rest = mergedList.subList(1, mergedList.size)

            val innerRes = first.evalExprImpl(rest, context)
            return innerRes.res.evalExprImpl(innerRes.followingExpression, context)
            //return EvalResult(ExprElem.evalExpr(context[opCode]!!, context), followingExpression)
        }

    }


    abstract class BaseOp1 : BaseOp() {
    }

    abstract class BaseOp2 : BaseOp() {
        override fun eval(arg: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            return CurriedOp2(this, arg)
        }

        abstract fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem
    }

    data class CurriedOp2(val op2: BaseOp2, val arg1: ExprElem) : BaseOp1() {
        override fun eval(arg: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            return op2.eval2(arg1, arg)
        }
    }

    abstract class BaseOp3 : BaseOp() {
        override fun eval(arg: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            return CurriedOp31(this, arg)
        }

        abstract fun eval3(arg1: ExprElem, arg2: ExprElem, arg3: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem
    }

    data class CurriedOp31(val op3: BaseOp3, val arg1: ExprElem) : BaseOp1() {
        override fun eval(arg: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            return CurriedOp32(op3, arg1, arg)
        }
    }

    data class CurriedOp32(val op3: BaseOp3, val arg1: ExprElem, val arg2: ExprElem) : BaseOp1() {
        override fun eval(arg: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            return op3.eval3(arg1, arg2, arg, context)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////

    object EqOp : BaseOp2() {
        override fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem {
            return if (arg1 == arg2) TrueElem else FalseElem
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // According to the specification in the message12 it is strictly less
    // 0 < 0 === False
    // 0 < 1 === True
    object Less : BaseOp2() {

        override fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                return if (arg1.value < arg2.value) TrueElem else FalseElem
            } else {
                throw EvalException("Unexpected ExprElems $arg1 or $arg2")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    /**
     * This ExprElem means that after that follows another [Apply] or some Op-glyph (a subclass of [BaseOp])
     * and then the same number of parameters as there were [Apply] ExprElems.
     * For example:
     * ap Dec 1 => 0
     * ap ap Plus 1 2 => 3
     */
    object Apply : ExprElem() {
        override fun evalExprImpl(followingExpression: List<ExprElem>, context: Map<Int, List<ExprElem>>): EvalResult {
//            println("Evaluating Apply ${this} rest = ${followingExpression}")
            val first = followingExpression[0]
            val rest = followingExpression.subList(1, followingExpression.size)
            val firstEv = if (first is Apply) first.evalExprImpl(rest, context) else EvalResult(first, rest)
            val evalResult = firstEv.res.evalExprImpl(firstEv.followingExpression, context)
            return evalResult
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Inc : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            if (arg is NumberElem) {
                return NumberElem(arg.value + BigInteger.ONE)
            } else {
                throw EvalException("Unexpected ExprElem $arg")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Dec : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            if (arg is NumberElem) {
                return NumberElem(arg.value - BigInteger.ONE)
            } else {
                throw EvalException("Unexpected ExprElem $arg")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Pwr2 : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            if (arg is NumberElem) {
                return NumberElem(BigInteger.valueOf(2).pow(arg.value.toInt()))
            } else {
                throw EvalException("Unexpected ExprElem $arg")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Nil : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            return TrueElem
        }

        override fun toString(): String {
            return "Nil"
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object IsNil : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            return if (Nil == arg) TrueElem else FalseElem
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Id : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            return arg
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////
    object Plus : BaseOp2() {
        override fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                return NumberElem(arg1.value + arg2.value)
            } else {
                throw EvalException("Unexpected ExprElems $arg1 or $arg2")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Mul : BaseOp2() {
        override fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                return NumberElem(arg1.value * arg2.value)
            } else {
                throw EvalException("Unexpected ExprElems $arg1 or $arg2")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // 5 / -3 === -1
    // -5 / 3 === -1
    // -5 / -3 === 1
    // so the rounding seems always to be towards 0
    object Div : BaseOp2() {
        override fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem {
            if (arg1 is NumberElem && arg2 is NumberElem) {
                val div = BigDecimal(arg1.value).divide(BigDecimal(arg2.value), RoundingMode.DOWN).toLong()
                return NumberElem(arg1.value / arg2.value)
            } else {
                throw EvalException("Unexpected ExprElems $arg1 or $arg2")
            }
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////
    object ConsOp : BaseOp2() {
        override fun eval2(arg1: ExprElem, arg2: ExprElem): ExprElem {
            return ConsElem(arg1, arg2)
        }

        override fun toString(): String {
            return "cons"
        }
    }

    data class ConsElem(val head: ExprElem, val tail: ExprElem) : ExprElem() {

        override fun evalExprImpl(followingExpression: List<ExprElem>, context: Map<Int, List<ExprElem>>): EvalResult {
            return EvalResult(this, followingExpression)
        }
    }

    object CarOp : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            if (arg is ConsElem) {
                return arg.head
            } else {
                throw EvalException("Unexpected ExprElems $arg")
            }
        }
    }

    object CdrOp : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            if (arg is ConsElem) {
                return arg.tail
            } else {
                throw EvalException("Unexpected ExprElems $arg")
            }
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // neg 0 === 0
    // neg 1 === -1
    // neg -1 === 1
    object Neg : BaseOp1() {
        override fun eval(
            arg: ExprElem,
            context: Map<Int, List<ExprElem>>
        ): ExprElem {
            if (arg is NumberElem) {
                return NumberElem(-arg.value)
            } else {
                throw EvalException("Unexpected ExprElem $arg")
            }
        }

    }


    ////////////////////////////////////////////////////////////////////////////////////

    object SCombinatorOp : BaseOp3() {
        override fun eval3(arg1: ExprElem, arg2: ExprElem, arg3: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            if (arg1 is BaseOp2 && arg2 is BaseOp) {
                val innerRes = arg2.eval(arg3, context)
                return arg1.eval2(arg3, innerRes)
            } else if (arg1 is BaseOp && arg2 is BaseOp) {
                // maybe we should join two Base1Op?
                val eval1 = arg1.eval(arg3, context)
                if (eval1 is BaseOp) {
                    val innerRes = arg2.eval(arg3, context)
                    return eval1.eval(innerRes, context)
                } else {
                    throw EvalException("Unexpected ExprElems $arg1 / $eval1")
                }
            } else {
                val b1 = arg1 is BaseOp2
                val b2 = arg2 is BaseOp
                println("arg1_fl=$b1; arg2_fl=$b2")
                throw EvalException("Unexpected ExprElems arg1=$arg1 or arg2=$arg2")
            }

        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object BCombinatorOp : BaseOp3() {
        override fun eval3(arg1: ExprElem, arg2: ExprElem, arg3: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            if (arg1 is BaseOp && arg2 is BaseOp) {
                val innerRes = arg2.eval(arg3, context)
                return arg1.eval(innerRes, context)
            } else {
                throw EvalException("Unexpected ExprElems $arg1 or $arg2")
            }
        }

        override fun toString(): String {
            return "b"
        }

    }
    ////////////////////////////////////////////////////////////////////////////////////

    object CCombinatorOp : BaseOp3() {
        override fun eval3(arg1: ExprElem, arg2: ExprElem, arg3: ExprElem, context: Map<Int, List<ExprElem>>): ExprElem {
            if (arg1 is BaseOp2) {
                return arg1.eval2(arg3, arg2)
            } else if (arg1 is BaseOp) {
                // maybe we should join two Base1Op?
                val eval1 = arg1.eval(arg3, context)
                if (eval1 is BaseOp) {
                    return eval1.eval(arg2, context)
                } else {
                    throw EvalException("Unexpected ExprElems $arg1 / $eval1")
                }
            } else {
                throw EvalException("Unexpected ExprElems $arg1")
            }
        }

        override fun toString(): String {
            return "c"
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////


}

fun evaluateAll(lines: List<List<ExprElem>>) {
    var context = HashMap<Int, List<ExprElem>>()
    var galaxy: List<ExprElem>? = null
    for (line in lines) {
        if (line.size < 3 || !(line[1] is ExprElem.UnknownOp) || (line[1] as ExprElem.UnknownOp).opCode != -1) {
            throw RuntimeException("Unexpected op in $line")
        }
        val opCode = (line[0] as ExprElem.UnknownOp).opCode
        if (opCode == -2) {  // it's a special galaxy op
            galaxy = line.subList(2, line.size)
        } else {
            context.put(opCode, line.subList(2, line.size))
        }
    }
//    val res = ExprElem.evalExpr(parseLine("ap") + galaxy!! + parseLine("nil"), context)
//    val res = ExprElem.evalExpr(galaxy!!, context)
//    println("Galaxy = $res")
    val galaxy_plus = parseLine("ap ap") + galaxy!! + parseLine("nil ap ap cons 0 ap ap cons 0 nil")
    val res2 = ExprElem.evalExpr(galaxy_plus, context)
    println("Galaxy(Nil) = $res2")
}

fun parseText(lines: List<String>): List<List<ExprElem>> {
    var out = ArrayList<List<ExprElem>>()
    for (line in lines) {
        out.add(parseLine(line))
    }
    return out
}

fun parseLine(line: String): List<ExprElem> {
    var out = ArrayList<ExprElem>()
    for (op in line.split(" ")) {
        val elem = when (op) {
            "ap" -> ExprElem.Apply
            "cons" -> ExprElem.ConsOp
            "nil" -> ExprElem.Nil
            "=" -> ExprElem.UnknownOp(-1)
            "neg" -> ExprElem.Neg
            "c" -> ExprElem.CCombinatorOp
            "b" -> ExprElem.BCombinatorOp
            "s" -> ExprElem.SCombinatorOp
            "isnil" -> ExprElem.IsNil
            "car" -> ExprElem.CarOp
            "cdr" -> ExprElem.CdrOp
            "vec" -> ExprElem.ConsOp
            "eq" -> ExprElem.EqOp
            "mul" -> ExprElem.Mul
            "add" -> ExprElem.Plus
            "div" -> ExprElem.Div
            "lt" -> ExprElem.Less
            "i" -> ExprElem.Id
            "t" -> ExprElem.TrueElem
            "f" -> ExprElem.FalseElem
            "galaxy" -> ExprElem.UnknownOp(-2)

            else -> if (op.startsWith(":")) {
                ExprElem.opFromCode(op.substring(1).toInt())
            } else if (op.toLongOrNull() != null) {
                ExprElem.NumberElem(op.toBigInteger())
            } else throw RuntimeException("Unknown op $op")
        }
        out.add(elem)
    }
    return out
}

fun parseLineAndEval(line: String): ExprElem {
    val expr = parseLine(line)
    val res = ExprElem.evalExpr(expr, null)
    return res
}


fun main(args: Array<String>) {

//    val lines: List<String> = File("./data/messages/galaxy.v2.txt").readLines()
//    val galaxy = parseText(lines)
//    evaluateAll(galaxy)
//      evaluateAll(parseText(listOf(":2048 = ap f :2048", "galaxy = ap :2048 42")))
//      evaluateAll(parseText(listOf(":67108929 = ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c cons nil ap c cons",
//"galaxy = ap ap :67108929 nil ap ap cons 1 ap cons 1 nil")))


//    val expr = listOf(
//        ExprElem.Apply,
//        ExprElem.Apply,
//        ExprElem.Apply,
//        ExprElem.CCombinatorOp,
//        ExprElem.Plus,
//        ExprElem.NumberElem(1),
//        ExprElem.NumberElem(2)
//    )


    val expr = parseLine("ap ap cons 8398848 ap ap cons 8407040 ap ap cons 8398849 ap ap cons 8407041 ap ap cons 8398850 ap ap cons 8402946 ap ap cons 8407042 ap ap cons 8398851 ap ap cons 8402947 ap ap cons 8407043 ap ap cons 8398852 ap ap cons 8402948 ap ap cons 8407044 ap ap cons 8390661 ap ap cons 8394757 ap ap cons 8398853 ap ap cons 8402949 ap ap cons 8407045 ap ap cons 8411141 ap ap cons 8415237 ap ap cons 8402950 nil")
    val res = ExprElem.evalExpr(expr, null)
    println(res)

//    val expr = parseLine("ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil")
//    evaluateAll(parseText(listOf("galaxy = ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil")))

//    evaluateAll(parseText(listOf("ap ap ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil 42 24")))
//    val res = ExprElem.evalExpr(expr, null)
//    println(res)

//    evaluateAll(parseText(listOf(":1029 = ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil",
//        "galaxy = ap ap :1029 42 24")))
//
//    val p3 =
//        parseLine("ap ap ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil 42 24")
//    val p4 =
//        parseLine("ap ap ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil nil ap ap cons 42 ap ap cons 43 nil")
//    val res3 = ExprElem.evalExpr(p3, null)
//    val res4 = ExprElem.evalExpr(p4, null)
//
//    println(res3)
//    println(res4)

//    val p3 =
//            parseLine("ap ap ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil 42 24")
//    val p4 =
//        parseLine("ap ap ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil nil ap ap cons 42 ap ap cons 43 nil")
//    val res3 = ExprElem.evalExpr(p3, null)
//    val res4 = ExprElem.evalExpr(p4, null)

//    println(res3)
//    println(res4)

//    evaluateAll(
//            parseText(
//                    listOf(
//                            ":1029 = ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil",
////                "galaxy = ap ap :1029 42 24"
//                            "galaxy = ap ap :1029 42 ap ap cons 43 nil"
//                    )
//            )
//    )
}
