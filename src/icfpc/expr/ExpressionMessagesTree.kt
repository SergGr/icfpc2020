package icfpc.expr


object ExpressionMessagesTree {

    private fun testEval(expected: TreeExpr, expressionString: String) {
        val treeExpr = ListExpr.parseLineAndBuildTree(expressionString)
        val res = treeExpr.evalExpr()
        if (res != expected) {
            println("Expected = $expected")
            println("Actual = $res")
            println(expressionString)
            throw RuntimeException("bad expression test")
        }

    }

    private fun testEval(expected: Long, expressionString: String) {
        testEval(TreeExpr.NumberElem(expected), expressionString)
    }

    ////////////////////////////////////////////////////////////

    fun message18() {
        testEval(42, "ap ap ap s mul ap add 1 6")
        testEval(3, "ap ap ap s add inc 1")

    }

    ////////////////////////////////////////////////////////////
    fun message19() {
        testEval(3, "ap ap ap c add 1 2")
    }

    ////////////////////////////////////////////////////////////

    fun message20() {
        testEval(2, "ap ap ap b inc dec 2")

        testEval(-1, "ap inc ap neg 2")
        testEval(-3, "ap neg ap inc 2")
        testEval(-1, "ap ap ap b inc neg 2")
    }

    ////////////////////////////////////////////////////////////

    fun message21() {
        testEval(42, "ap ap t 42 24")
        testEval(TreeExpr.TrueElem, "ap ap t t ap inc 5")
    }

    ////////////////////////////////////////////////////////////
    fun message22() {
        testEval(24, "ap ap f 42 24")
        testEval(6, "ap ap f t ap inc 5")
    }


    ////////////////////////////////////////////////////////////
    fun message23() {
        testEval(4, "ap pwr2 2")
        testEval(8, "ap pwr2 3")
        testEval(256, "ap pwr2 8")
    }

    ////////////////////////////////////////////////////////////
    fun message24() {
        testEval(2, "ap id 2")
        testEval(TreeExpr.Plus, "ap id add")
    }
    ////////////////////////////////////////////////////////////

    fun message26() {
        testEval(TreeExpr.Plus, "ap car ap ap cons add nil")
        testEval(3, "ap car ap ap cons 3 nil")
    }
    ////////////////////////////////////////////////////////////

    fun message27() {
        testEval(TreeExpr.Nil, "ap cdr ap ap cons add nil")
        testEval(TreeExpr.Nil, "ap cdr ap ap cons 3 nil")
        testEval(3, "ap cdr ap ap cons nil 3")
    }


    ////////////////////////////////////////////////////////////
    fun message28() {
        testEval(TreeExpr.TrueElem, "ap nil 2")
        testEval(TreeExpr.TrueElem, "ap nil ap inc 2")
    }

    ////////////////////////////////////////////////////////////
    fun message29() {
        testEval(TreeExpr.TrueElem, "ap isnil nil")
        testEval(TreeExpr.FalseElem, "ap isnil add")
    }

}


fun main(args: Array<String>) {
//*
    ExpressionMessagesTree.message18()
    ExpressionMessagesTree.message19()
    ExpressionMessagesTree.message20()
    ExpressionMessagesTree.message21()
    ExpressionMessagesTree.message22()
    ExpressionMessagesTree.message23()
    ExpressionMessagesTree.message24()
    ExpressionMessagesTree.message26()
    ExpressionMessagesTree.message27()
    //TODO: add 25-27 cons/car/cdr
    ExpressionMessagesTree.message28()
    ExpressionMessagesTree.message29()
//*/

    println("Everything is fine")
}