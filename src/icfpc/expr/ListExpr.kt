package icfpc.expr

import icfpc.expr.ListExpr.Companion.evaluateAllEx
import icfpc.expr.ListExpr.Companion.parseTextEx


data class TreeBuildResult(val res: TreeExprNode, val followingExpression: List<ListExpr>)

sealed class ListExpr {
    companion object {
        fun buildTree(expression: List<ListExpr>, context: Map<Int, List<ListExpr>>? = null): TreeExprNode {
            val first = expression[0]
            val rest = expression.subList(1, expression.size)
            val buildResult = first.buildTreeEx(rest, context ?: HashMap())
            if (buildResult.followingExpression.isNotEmpty()) {
                println("Expression=$expression")
                println("buildResult.res=$buildResult.res")
                println("buildResult.followingExpression=$buildResult.followingExpression")
                throw EvalException("not full evaluation")
            }
            return buildResult.res
        }


        fun opFromCode(code: Int, failOnUnknown: Boolean = false): ListExpr {
            return when (code) {
                // not really Ops but look similar so parse them here
                0 -> ListExpr.Apply
                2 -> ListExpr.TrueElem
                8 -> ListExpr.FalseElem
//                12 -> MetaEquals

                // regular Ops
                5 -> ListExpr.BCombinatorOp
                7 -> ListExpr.SCombinatorOp
                6 -> ListExpr.CCombinatorOp
                10 -> ListExpr.Neg
                40 -> ListExpr.Div
                146 -> ListExpr.Mul
                365 -> ListExpr.Plus
//                170 -> ConvertNumberToBits
//                341 -> ConvertBitsToNumber
                // TODO: are there others DecBy and IncBy or is it always by 1?
                401 -> ListExpr.Dec
                417 -> ListExpr.Inc
                416 -> ListExpr.Less
                448 -> ListExpr.EqOp
//                174 -> AlienFunction
                else -> if (failOnUnknown) throw RuntimeException("Unknown op-code $code")
                else {
                    System.err.println("Found an unknown op-code $code")
                    ListExpr.UnknownOp(code)
                }
            }
        }

        fun parseLineAndBuildTree(line: String): TreeExprNode {
            val parsed = parseLine(line)
            val tree = ListExpr.buildTree(parsed)
            return tree
        }

        fun parseLine(line: String): List<ListExpr> {
            val out = ArrayList<ListExpr>()
            for (op in line.split(" ")) {
                val elem = when (op) {
                    "ap" -> ListExpr.Apply
                    "cons" -> ListExpr.ConsOp
                    "nil" -> ListExpr.Nil
                    "=" -> ListExpr.UnknownOp(-1)
                    "neg" -> ListExpr.Neg
                    "c" -> ListExpr.CCombinatorOp
                    "b" -> ListExpr.BCombinatorOp
                    "s" -> ListExpr.SCombinatorOp
                    "isnil" -> ListExpr.IsNil
                    "car" -> ListExpr.CarOp
                    "cdr" -> ListExpr.CdrOp
                    "vec" -> ListExpr.ConsOp
                    "eq" -> ListExpr.EqOp
                    "inc" -> ListExpr.Inc
                    "dec" -> ListExpr.Dec
                    "mul" -> ListExpr.Mul
                    "add" -> ListExpr.Plus
                    "div" -> ListExpr.Div
                    "pwr2" -> ListExpr.Pwr2
                    "lt" -> ListExpr.Less
                    "i" -> ListExpr.Id
                    "t" -> ListExpr.TrueElem
                    "f" -> ListExpr.FalseElem
                    "id" -> ListExpr.Id
                    "galaxy" -> ListExpr.UnknownOp(-2)

                    else -> if (op.startsWith(":")) {
                        ListExpr.opFromCode(op.substring(1).toInt())
                    } else if (op.toLongOrNull() != null) {
                        ListExpr.NumberElem(op.toLong())
                    } else throw RuntimeException("Unknown op $op")
                }
                out.add(elem)
            }
            return out
        }

        fun parseTextEx(lines: List<String>): List<List<ListExpr>> {
            var out = ArrayList<List<ListExpr>>()
            for (line in lines) {
                out.add(parseLine(line))
            }
            return out
        }

        fun evaluateAllEx(lines: List<List<ListExpr>>) {
            var context = HashMap<Int, List<ListExpr>>()
            var galaxy: List<ListExpr>? = null
            for (line in lines) {
                if (line.size < 3 || !(line[1] is ListExpr.UnknownOp) || (line[1] as ListExpr.UnknownOp).opCode != -1) {
                    throw RuntimeException("Unexpected op in $line")
                }
                val opCode = (line[0] as ListExpr.UnknownOp).opCode
                if (opCode == -2) {  // it's a special galaxy op
                    galaxy = line.subList(2, line.size)
                } else {
                    context.put(opCode, line.subList(2, line.size))
                }
            }

            val galaxyTree = buildTree(galaxy!!, context)
            val res = galaxyTree.evalExpr()
            println("Galaxy = $res")
        }

    }

    abstract fun buildTreeEx(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeBuildResult


    ////////////////////////////////////////////////////////////////////////////////////

    abstract class SimlpeExpr : ListExpr() {
        override fun buildTreeEx(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeBuildResult {
            val node = buildTree(followingExpression, context)
            return TreeBuildResult(node, followingExpression)
        }

        abstract fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode
    }

    ////////////////////////////////////////////////////////////////////////////////////
    data class NumberElem(val value: Long) : SimlpeExpr() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.NumberElem(value))
        }

        override fun toString(): String {
            return value.toString()
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BooleanElem protected constructor(val value: Boolean) : BaseOp2() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            val treeExpr = if (value) TreeExpr.TrueElem else TreeExpr.FalseElem
            return TreeExprNode(treeExpr)
        }

        override fun toString(): String {
            return if (true) "t" else "f"
        }
    }

    object TrueElem : BooleanElem(true)

    object FalseElem : BooleanElem(false)


    ////////////////////////////////////////////////////////////////////////////////////
    abstract class BaseOp : SimlpeExpr() {
/*
        override fun evalExprImpl(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): EvalResult {
//            println("Evaluating ${this} rest = ${followingExpression}")
            val first = followingExpression[0]
            val rest = followingExpression.subList(1, followingExpression.size)
            // another ugly hack to handle Apply precedence
            val firstEv = if (first is Apply) first.evalExprImpl(rest, context) else EvalResult(first, rest)
            val evalRes = eval(firstEv.res, context)
//            return evalRes.evalExprImpl(firstEv.followingExpression)
            return EvalResult(evalRes, firstEv.followingExpression)
        }

*/
    }

    data class UnknownOp(val opCode: Int) : BaseOp() {
        override fun buildTreeEx(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeBuildResult {
            val mergedList: MutableList<ListExpr> = ArrayList()
            mergedList.addAll(context[opCode]!!)
            mergedList.addAll(followingExpression)

            val first = mergedList[0]
            val rest = mergedList.subList(1, mergedList.size)

            val innerRes = first.buildTreeEx(rest, context)
            return innerRes
//            return innerRes.res.evalExprImpl(innerRes.followingExpression, context)
        }

        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            TODO("Not implemented yet")
        }
        /*
        override fun eval(arg: ListExpr, context: Map<Int, List<ListExpr>>): ListExpr {
            //return ListExpr.evalExpr(context[opCode]!!, context)
            TODO("Not implemented yet")
        }

        override fun evalExprImpl(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): EvalResult {
            val mergedList: MutableList<ListExpr> = ArrayList()
            mergedList.addAll(context[opCode]!!)
            mergedList.addAll(followingExpression)

            val first = mergedList[0]
            val rest = mergedList.subList(1, mergedList.size)

            val innerRes = first.evalExprImpl(rest, context)
            return innerRes.res.evalExprImpl(innerRes.followingExpression, context)
            //return EvalResult(ListExpr.evalExpr(context[opCode]!!, context), followingExpression)
        }
*/

    }


    abstract class BaseOp1 : BaseOp() {
    }

    abstract class BaseOp2 : BaseOp() {
    }

    abstract class BaseOp3 : BaseOp() {
    }

    ////////////////////////////////////////////////////////////////////////////////////

    object EqOp : BaseOp2() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.EqOp)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // According to the specification in the message12 it is strictly less
    // 0 < 0 === False
    // 0 < 1 === True
    object Less : BaseOp2() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Less)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    /**
     * This ListExpr means that after that follows another [Apply] or some Op-glyph (a subclass of [BaseOp])
     * and then the same number of parameters as there were [Apply] ListExprs.
     * For example:
     * ap Dec 1 => 0
     * ap ap Plus 1 2 => 3
     */
    object Apply : ListExpr() {
        override fun buildTreeEx(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeBuildResult {
            val first = followingExpression[0]
            val rest = followingExpression.subList(1, followingExpression.size)
//            if (first is Apply) {
//
//            } else {
//                val firstRes = first.buildTreeEx(rest, context)
//                val firstNode = firstRes.res
//                val arg = rest[0]
//                val restRest = rest.subList(1, rest.size)
//                val argNode = arg.buildTreeEx(restRest, context)
//                firstNode.children.add(argNode.res)
//                return TreeBuildResult(firstNode, argNode.followingExpression)
//            }

            val firstRes = first.buildTreeEx(rest, context)
            val firstNode = firstRes.res
            val firstRest = firstRes.followingExpression
            val arg = firstRest[0]
            val argRest = firstRest.subList(1, firstRest.size)
            val argRes = arg.buildTreeEx(argRest, context)
            firstNode.children.add(argRes.res)
            return TreeBuildResult(firstNode, argRes.followingExpression)
        }
        /*
        override fun evalExprImpl(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): EvalResult {
//            println("Evaluating Apply ${this} rest = ${followingExpression}")
            val first = followingExpression[0]
            val rest = followingExpression.subList(1, followingExpression.size)
            val firstEv = if (first is Apply) first.evalExprImpl(rest, context) else EvalResult(first, rest)
            val evalResult = firstEv.res.evalExprImpl(firstEv.followingExpression, context)
            return evalResult
        }
*/
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Inc : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Inc)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Dec : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Dec)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Pwr2 : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Pwr2)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Nil : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Nil)
        }

        override fun toString(): String {
            return "Nil"
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object IsNil : BaseOp1() {

        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.IsNil)
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Id : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Id)
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////
    object Plus : BaseOp2() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Plus)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object Mul : BaseOp2() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Mul)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // 5 / -3 === -1
    // -5 / 3 === -1
    // -5 / -3 === 1
    // so the rounding seems always to be towards 0
    object Div : BaseOp2() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Div)
        }

    }

    ////////////////////////////////////////////////////////////////////////////////////
    object ConsOp : BaseOp2() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.ConsOp)
        }

        override fun toString(): String {
            return "cons"
        }
    }

    object CarOp : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.CarOp)
        }
    }

    object CdrOp : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.CdrOp)
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    // according to the specification in message 10
    // neg 0 === 0
    // neg 1 === -1
    // neg -1 === 1
    object Neg : BaseOp1() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.Neg)
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////

    object SCombinatorOp : BaseOp3() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.SCombinatorOp)
        }

        override fun toString(): String {
            return "s"
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////
    object BCombinatorOp : BaseOp3() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.BCombinatorOp)
        }

        override fun toString(): String {
            return "b"
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////

    object CCombinatorOp : BaseOp3() {
        override fun buildTree(followingExpression: List<ListExpr>, context: Map<Int, List<ListExpr>>): TreeExprNode {
            return TreeExprNode(TreeExpr.CCombinatorOp)
        }

        override fun toString(): String {
            return "c"
        }
    }
    ////////////////////////////////////////////////////////////////////////////////////

}


fun main(args: Array<String>) {
//    val p3 = ListExpr.parseLine("ap ap ap c add 1 2")
//    val tree = ListExpr.buildTree(p3)
//    println(tree)
//    val eval = tree.evalExpr()
//    println(eval)
    evaluateAllEx(
        parseTextEx(
            listOf(
                ":1029 = ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil",
                "galaxy = ap ap :1029 42 24"
//                "galaxy = ap ap :1029 42 ap ap cons 43 nil"
            )
        )
    )

//    evaluateAll(
//        parseText(
//            listOf(
//                ":2048 = ap f :2048",
//                "galaxy = ap :2048 42"
//            )
//        )
//    )


}
