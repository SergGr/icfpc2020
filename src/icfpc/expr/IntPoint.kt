package icfpc.expr

import java.util.*

class IntPoint(val x: Int, val y: Int, val layer: Int = 0) {

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o == null || javaClass != o.javaClass) return false
        val intPoint = o as IntPoint
        return x == intPoint.x &&
                y == intPoint.y
    }

    override fun hashCode(): Int {
        return Objects.hash(x, y)
    }

    override fun toString(): String {
        return "IntPoint{$x, $y}"
    }

    operator fun plus(o : IntPoint): IntPoint {
        return IntPoint(x + o.x, y + o.y)
    }
}