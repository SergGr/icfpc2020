package icfpc

import icfpc.expr.*
import icfpc.network.*
import org.apache.hc.client5.http.config.RequestConfig
import org.apache.hc.client5.http.fluent.Request
import org.apache.hc.client5.http.impl.classic.HttpClients
import org.apache.hc.core5.http.ClassicHttpResponse
import org.apache.hc.core5.http.ContentType
import org.apache.hc.core5.http.HttpStatus
import org.apache.hc.core5.http.HttpVersion
import java.io.ByteArrayOutputStream
import java.lang.StringBuilder
import java.math.BigInteger

// Entry point for the main submission
fun main(args: Array<String>) {
    val javaVersion = System.getProperty("java.version")
    println("Java version $javaVersion")

    val (serverUrl, playerKey) = args
    println("ServerUrl: $serverUrl; PlayerKey: $playerKey")
    val aliensApi = AliensApi(serverUrl + "/aliens/send")
    val player = ApiPlayer(playerKey.toBigInteger(), aliensApi)
    orbiter(player)
}