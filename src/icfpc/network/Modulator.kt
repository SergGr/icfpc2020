package icfpc.network

import icfpc.expr.ExprElem
import icfpc.expr.parseLine
import icfpc.expr.parseLineAndEval
import java.lang.StringBuilder
import java.math.BigInteger
import kotlin.math.abs

object Modulator {
    fun encode(listExpr: ExprElem): String {
        val sb = StringBuilder()
        append(sb, listExpr)
        return sb.toString()
    }

    private fun append(sb: StringBuilder, elem: ExprElem) {
        when (elem) {
            ExprElem.Nil -> appendNil(sb)
            is ExprElem.ConsElem -> appendCons(sb, elem)
            is ExprElem.NumberElem -> appendNumber(sb, elem.value)
            else -> throw RuntimeException("Unexpected element $elem")
        }
    }

    private fun appendNil(sb: StringBuilder) {
        sb.append("00")
    }

    private fun appendNumber(sb: StringBuilder, value: BigInteger) {
        if (value < BigInteger.ZERO) {
            sb.append("10")
        } else {
            sb.append("01")
        }
        if (value == BigInteger.ZERO) {
            sb.append("0")
            return
        }
        assert(value.abs() < BigInteger.valueOf(2).pow(63))
        val pow = powFromLong(value.toLong())
        val pow4 = pow / 4 + 1
        for (i in 0 until pow4)
            sb.append("1")
        sb.append("0")
        val abs = value.abs()
        val binStr = abs.toString(2)
        for (i in binStr.length until (pow4 * 4)) {
            sb.append(0)
        }
        sb.append(binStr)
    }

    private fun appendCons(sb: StringBuilder, cons: ExprElem.ConsElem) {
        sb.append("11")
        append(sb, cons.head)
        append(sb, cons.tail)
    }

    private fun powFromLong(value: Long): Int {
        if (value == 0L)
            return -1
        val abs = abs(value)
        var pow = 0
        var tmp = abs
        while (tmp > 1) {
            pow += 1
            tmp /= 2
        }
        return pow
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////

    fun decode(msg: String): ExprElem {
        return Demodulator(msg).expr
    }


    private class Demodulator(val msg: String) {
        private var pos: Int = 0
        val expr: ExprElem = decode()

        fun decode(): ExprElem {
            val ch0 = msg[pos]
            val ch1 = msg[pos + 1]
            val prefix = Pair(ch0, ch1)
            return when (prefix) {
                Pair('0', '0') -> {
                    pos += 2
                    ExprElem.Nil
                }
                Pair('1', '0') -> ExprElem.NumberElem(decodeNumber())
                Pair('0', '1') -> ExprElem.NumberElem(decodeNumber())
                Pair('1', '1') -> decodeCons()
                else -> throw RuntimeException("Unexpected message prefix $prefix")
            }
        }

        private fun decodeNumber(): BigInteger {
            val ch0 = msg[pos]
            val ch1 = msg[pos + 1]
            val isNegative = ch0 == '1'
            val lenStart = pos + 2
            var lenMul4 = 0
            while (msg[lenStart + lenMul4] == '1') {
                lenMul4 += 1
            }
            if (lenMul4 == 0) {
                pos += 2 + 1
                return BigInteger.ZERO
            }

            val valStart = lenStart + lenMul4 + 1
            val len = lenMul4 * 4
            var pow = BigInteger.ONE
            var value = BigInteger.ZERO
            for (x in (len - 1) downTo 0) {
                if (msg[valStart + x] == '1')
                    value += pow
                pow *= BigInteger.valueOf(2)
            }
            if (isNegative)
                value = -value

            pos += 2 + 1 + lenMul4 * 5

            return value
        }

        private fun decodeCons(): ExprElem.ConsElem {
            pos += 2
            val head = decode()
            val tail = decode()
            return ExprElem.ConsElem(head, tail)
        }
    }
}


fun main(args: Array<String>) {
//    println(Modulator.encode(ExprElem.Nil))
//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.Nil, ExprElem.Nil)))
//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.NumberElem(0), ExprElem.Nil)))
//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.NumberElem(0), ExprElem.ConsElem(ExprElem.NumberElem(38590), ExprElem.Nil))))


//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.NumberElem(0), ExprElem.Nil)))
//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.NumberElem(1), ExprElem.Nil)))
//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.NumberElem(15), ExprElem.Nil)))
//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.NumberElem(16), ExprElem.Nil)))
//    println(Modulator.encode(ExprElem.ConsElem(ExprElem.NumberElem(1), ExprElem.Nil)))

//    println(Modulator.encode(parseLineAndEval("0")))
//    println(parseLineAndEval("ap ap cons nil ap ap cons 1 0"))
//    println(Modulator.encode(parseLineAndEval("ap ap cons nil ap ap cons 0 ap ap cons 0 nil")))
    val expr = ExprElem.ConsElem(head=ExprElem.NumberElem(BigInteger.ZERO),
            tail=ExprElem.ConsElem(head=ExprElem.ConsElem(head=ExprElem.ConsElem(head=ExprElem.NumberElem(BigInteger.ZERO), tail=ExprElem.ConsElem(head=ExprElem.NumberElem(BigInteger.ZERO), tail=ExprElem.Nil)), tail=ExprElem.Nil), tail=ExprElem.ConsElem(head=ExprElem.ConsElem(head=ExprElem.ConsElem(head=ExprElem.ConsElem(head=ExprElem.NumberElem(BigInteger.ZERO), tail=ExprElem.ConsElem(head=ExprElem.NumberElem(BigInteger.ZERO), tail=ExprElem.Nil)), tail=ExprElem.Nil), tail=ExprElem.Nil), tail=ExprElem.Nil)))
    println(Modulator.encode(expr))
    println(Modulator.decode("1101100001110111110100000110011010100"))
//    println(Modulator.decode("110110000111011111100001001011010111110000"))
//    println(Modulator.decode("110110000111011111100001001001101000100000"))
//    println(Modulator.decode("110110000111011111100001001001100101111100"))
//    println(Modulator.decode("1101000"))
//    println(Modulator.decode("110110000100"))
//    println(Modulator.decode("110110111100"))

    // 11 00 11 01100001 010
//    println(Modulator.decode("11001101100001010"))

}