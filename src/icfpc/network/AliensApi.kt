package icfpc.network

import icfpc.expr.*
import icfpc.expr.IntPoint
import icfpc.player.visualizer.Visualizer
import org.apache.hc.client5.http.config.RequestConfig
import org.apache.hc.client5.http.fluent.Request
import org.apache.hc.client5.http.impl.classic.HttpClients
import org.apache.hc.core5.http.ClassicHttpResponse
import org.apache.hc.core5.http.ContentType
import org.apache.hc.core5.http.HttpStatus
import org.apache.hc.core5.http.HttpVersion
import java.io.ByteArrayOutputStream
import java.lang.Math.*
import java.math.BigInteger
import kotlin.concurrent.thread

data class GameResponse(var gameStage: BigInteger, var staticGameInfo: StaticGameInfo?, var gameState: GameState?) {}

data class StaticGameInfo(var x0: Expr, var role: BigInteger, var x2: Expr, var x3: Expr, var x4: Expr) {
    override fun toString(): String {
        val sb0 = StringBuilder()
        printCons(sb0, x0)
        val sb2 = StringBuilder()
        printCons(sb2, x2)
        val sb3 = StringBuilder()
        printCons(sb3, x3)
        val sb4 = StringBuilder()
        printCons(sb4, x4)
        return "StaticGameInfo(x0=$sb0, role=$role, x2=$sb2, x3=$sb3, x4=$sb4)"
    }
}


data class GameState(var gameTick: BigInteger, var x1: Expr,
                     var shipsAndCommands: List<Pair<ShipState, List<Expr>>>) {
    override fun toString(): String {
        val sb1 = StringBuilder()
        printCons(sb1, x1)
        val sb2 = StringBuilder()
        sb2.append('[')
        for (p in shipsAndCommands) {
            sb2.append('(')
            sb2.append(p.first)
            sb2.append(", [")
            for (expr in p.second) {
                printCons(sb2, expr)
                sb2.append(", ")
            }
            sb2.append("], ")
        }
        sb2.append("]")
        return "GameState(gameTick=$gameTick, x1=$sb1, shipsAndCommands=$sb2)"
    }
}

data class ShipState(var role: BigInteger,
                     var shipId: BigInteger,
                     var position: Expr,
                     var velocity: Expr,
                     var x4: Expr, var x5: Expr, var x6: Expr, var x7: Expr) {
    override fun toString(): String {
        val sb1 = StringBuilder()
        printCons(sb1, position)
        val sb2 = StringBuilder()
        printCons(sb2, velocity)
        val sb4 = StringBuilder()
        printCons(sb4, x4)
        val sb5 = StringBuilder()
        printCons(sb5, x5)
        val sb6 = StringBuilder()
        printCons(sb6, x6)
        val sb7 = StringBuilder()
        printCons(sb7, x7)
        return "ShipState(role=$role, shipId=$shipId, position=$sb1, velocity=$sb2, x4=$sb4, x5=$sb5, x6=$sb6, x7=$sb7)"
    }
}

data class CreateResponse(var attackPlayerKey: BigInteger, var defendPlayerKey: BigInteger) {}

fun parseGameResponse(inp: Expr): GameResponse? {
    val sb = StringBuilder()
    printCons(sb, inp)
    println("Trying to parse game response=$sb")
    var l = consToList(inp)
    if (l.size == 1) {
        return null
    } else {
        return GameResponse(asNum(l[1]), parseStaticGameInfo(l[2]), parseGameState(l[3]))
    }
}

fun parseStaticGameInfo(inp: Expr): StaticGameInfo? {
    if (inp is Atom && inp.name == "nil") {
        return null
    }
    var l = consToList(inp)
    return StaticGameInfo(l[0], asNum(l[1]), l[2], l[3], l[4])
}

fun parseGameState(inp: Expr): GameState? {
    if (inp is Atom && inp.name == "nil") {
        return null
    }
    var l = consToList(inp)
    return GameState(asNum(l[0]), l[1], parseShipsAndCmds(l[2]))
}

fun parseShipsAndCmds(inp: Expr): List<Pair<ShipState, List<Expr>>> {
    val l = consToList(inp)
    val out = ArrayList<Pair<ShipState, List<Expr>>>()
    for (expr in l) {
        val lpair = consToList(expr)
        out.add(Pair(parseShipState(lpair[0]), consToList(lpair[1])))
    }
    return out
}

fun parseShipState(inp: Expr): ShipState {
    val l = consToList(inp)
    return ShipState(asNum(l[0]), asNum(l[1]), l[2], l[3], l[4], l[5], l[6], l[7])
}

fun parseCreateResponse(ret: Ap): CreateResponse {
    assert(asNum(consHead(ret)) == BigInteger.ONE)
    val l = consToList(consHead(consTail(ret) as Ap))
    return CreateResponse(asNum(consHead(consTail(l[0] as Ap) as Ap)),
            asNum(consHead(consTail(l[1] as Ap) as Ap)))
}


fun parseCmdsImpl(inp: Expr): List<BaseCmd> {
    var out = ArrayList<BaseCmd>()
    val listOfCmds = consToList(inp)

    for (cmd in listOfCmds) {
        val lcmd = consToList(cmd)
        val type = asNum(lcmd[0])
        if (type == BigInteger.ZERO) {
            out.add(AccelerateCmd(asNum(lcmd[1]), lcmd[2]))
        } else if (type == BigInteger.ONE) {
            out.add(DetonateCmd(asNum(lcmd[1])))
        } else if (type == BigInteger.valueOf(2)) {
            out.add(ShootCmd(asNum(lcmd[1]), lcmd[2], lcmd[3]))
        }
    }
    return out
}

fun parseCmds(inp: Expr): List<BaseCmd> {
    val l = consToList(inp)
    assert(asNum(l[0]) == BigInteger.valueOf(4))
    return parseCmdsImpl(l[2])
}

open class BaseCmd {

}

data class AccelerateCmd(var shipId: BigInteger, var vec: Expr) : BaseCmd() {}
data class DetonateCmd(var shipId: BigInteger) : BaseCmd() {}
data class ShootCmd(var shipId: BigInteger, var vec: Expr, var x3: Expr) : BaseCmd() {}

data class AliensApi(var fullServerUrl: String = "https://icfpc2020-api.testkontur.ru/aliens/send?apiKey=75332bed49a84298a712d53297f41e64") {

    fun getResponseById(id: String): String? {// /aliens/{responseId}
        return null
    }

    // input: modulated vector, valid examples:
    // mod () => "1101000"
    // mod (1,0) =>
    fun sendToAliens(modVec: String): String? { // /aliens/send
        val request = Request.post(fullServerUrl)
                .useExpectContinue()
                .version(HttpVersion.HTTP_1_1)

                .bodyString(modVec, ContentType.TEXT_PLAIN)
        val requestConfig = RequestConfig.custom().setCircularRedirectsAllowed(true).build()
        val client = HttpClients.custom().setDefaultRequestConfig(requestConfig).build()

        val response = request.useExpectContinue()
                .execute(client)
                .returnResponse()

        val status = response.code

        if (status != HttpStatus.SC_OK) {
            throw RuntimeException("Failed to send request. Status code: $status")
        }
        val out = ByteArrayOutputStream()
        (response as ClassicHttpResponse).getEntity().writeTo(out)
        val result = out.toString()
        out.close()

        return result
    }

    fun sendExprToAliens(inp: Expr): Expr {
        val sb1 = StringBuilder()
        printCons(sb1, inp)
        println("send $sb1")

        val expr = TinyModulator.decode(sendToAliens(inp.modulate())!!)
        val sb2 = StringBuilder()
        printCons(sb2, expr)
        println("recv $sb2")
        return expr
    }

    fun makeCreateRequest(): Expr {
        return ConsParser.parse("(1,(0,nil))")
    }

    fun makeJoinRequest(playerKey: BigInteger): Expr {
        return ConsParser.parse("(2,($playerKey,(nil,nil)))")
    }

    fun makeStartRequest(playerKey: BigInteger, joinResponse: GameResponse?): Expr {
        if (joinResponse!!.staticGameInfo!!.role == BigInteger.ONE) {
            // for splits 152,0,8,100
            return ConsParser.parse("(3,($playerKey,((152,(0,(8,(100,nil)))),nil)))")
        } else {
            // for shoots 134,64,10,1
            return ConsParser.parse("(3,($playerKey,((134,(64,(10,(1,nil)))),nil)))")
        }
    }

    fun makeMoveRequest(playerKey: BigInteger, x: Int, y: Int, gameResponse: GameResponse?): Expr {
        val role = gameResponse!!.staticGameInfo!!.role
        val shipId = BigInteger.ONE - role
        return ConsParser.parse("(4,($playerKey,(((0,($shipId,(($x, $y),nil))),nil),nil)))")
    }

    fun makeNoopRequest(playerKey: BigInteger, gr: GameResponse?): Expr {
        return ConsParser.parse("(4,($playerKey,(nil,nil)))")
    }

    fun makeShootRequest(playerKey: BigInteger, x: Int, y: Int, power:Int, gr: GameResponse?): Expr {
        val role = gr!!.staticGameInfo!!.role
        val shipId = BigInteger.ONE - role
//        val power = 64
        return ConsParser.parse("(4,($playerKey,(((2,($shipId,(($x,$y),($power,nil)))),nil),nil)))")
//    (2,((-27,48),(5,(0,(4,nil)))))
    }

    fun makeSplitRequest(playerKey: BigInteger, gr: GameResponse?): Expr {
        val role = gr!!.staticGameInfo!!.role
        val shipId = BigInteger.ONE - role
        // with move (4,(2441348111319631358,(((3,(0,((0,(0,(0,(1,nil)))),nil))),((0,(0,((1,-1),nil))),nil)),nil)))
        return ConsParser.parse("(4,($playerKey,(((3,($shipId,((0,(0,(0,(1,nil)))),nil))),((0,($shipId,((1,-1),nil))),nil)),nil)))")
    }

    fun makeDetonateRequest(playerKey: BigInteger, gr: GameResponse?): Expr {
        val role = gr!!.staticGameInfo!!.role
        val shipId = BigInteger.ONE - role
        return ConsParser.parse("(4,($playerKey,(((1,($shipId,nil)),nil),nil)))")
    }
}

interface BasePlayer {
    var playerKey: BigInteger
    fun join()
    fun start()
    fun move(x: Int, y: Int)
    fun isGameFinished(): Boolean
    fun noop()
    fun shoot(x: Int, y: Int, power: Int)
    fun split()
    fun detonate()
}

data class ApiPlayer(override var playerKey: BigInteger, var aliensApi: AliensApi, var gr: GameResponse? = null) : BasePlayer {
    override fun join() {
        val resp = aliensApi.sendExprToAliens(aliensApi.makeJoinRequest(playerKey))
        gr = parseGameResponse(resp)
        println("join resp=$gr")
    }

    override fun start() {
        val resp = aliensApi.sendExprToAliens(aliensApi.makeStartRequest(playerKey, gr))
        gr = parseGameResponse(resp)
        println("start resp=$gr")
    }

    override fun move(x: Int, y: Int) {
        val resp = aliensApi.sendExprToAliens(aliensApi.makeMoveRequest(playerKey, x, y, gr))
        gr = parseGameResponse(resp)
        println("move resp=$gr")
    }

    override fun isGameFinished(): Boolean {
        return gr?.gameStage == BigInteger.valueOf(2)
    }

    override fun noop() {
        val resp = aliensApi.sendExprToAliens(aliensApi.makeNoopRequest(playerKey, gr))
        gr = parseGameResponse(resp)
        println("move resp=$gr")
    }

    override fun shoot(x: Int, y: Int, power: Int) {
        val resp = aliensApi.sendExprToAliens(aliensApi.makeShootRequest(playerKey, x, y, power, gr))
        gr = parseGameResponse(resp)
    }

    override fun split() {
        val resp = aliensApi.sendExprToAliens(aliensApi.makeSplitRequest(playerKey, gr))
        gr = parseGameResponse(resp)
    }

    override fun detonate() {
        val resp = aliensApi.sendExprToAliens(aliensApi.makeDetonateRequest(playerKey, gr))
        gr = parseGameResponse(resp)
    }
}

data class VisPlayer(override var playerKey: BigInteger, var vis: Visualizer) : BasePlayer {
    override fun join() {
        vis.gameEngine.state = ConsParser.parse("(5,((4,($playerKey,(nil,(nil,(nil,(nil,((36,0),(29902,nil)))))))),(9,(nil,nil))))")
        vis.gameEngine.step(Pair(22, -4))
        vis.updateUi()
    }

    override fun start() {
        vis.gameEngine.state = ConsParser.parse("(5,((8,($playerKey,((510,(0,(0,(1,nil)))),(nil,((1,(0,((256,(0,((512,(1,(64,nil))),((16,(128,nil)),((446,(0,(0,(1,nil)))),nil))))),(nil,nil)))),((1,(0,(0,(-1,(((1,(0,(2,nil))),((0,(0,(0,nil))),nil)),(nil,nil)))))),((36,0),(29902,nil)))))))),(9,(nil,nil))))")
        vis.gameEngine.step(Pair(76, 20))
        vis.updateUi()
    }

    override fun move(x: Int, y: Int) {
        vis.gameEngine.state = ConsParser.parse("(6,((0,(8,($playerKey,(1,(0,(2,(nil,(((0,(0,((0,-1),nil))),nil),(4,((0,((16,(128,nil)),((((1,(0,((-35,-48),((0,0),((446,(0,(0,(1,nil)))),(0,(64,(1,nil)))))))),(nil,nil)),(((0,(1,((35,48),((0,0),((510,(0,(0,(1,nil)))),(0,(64,(1,nil)))))))),(nil,nil)),nil)),nil))),((256,(1,((448,(1,(64,nil))),((16,(128,nil)),(nil,nil))))),(nil,((446,(0,(0,(1,nil)))),nil))))))))))))),(9,(nil,nil))))")
        vis.gameEngine.step(Pair(0, 0))
        vis.updateUi()
    }

    override fun isGameFinished(): Boolean {
        return false
//        TODO("Not yet implemented")
    }

    override fun noop() {
        vis.gameEngine.step(Pair(0, 0))
        vis.updateUi()
    }

    override fun shoot(x: Int, y: Int, power: Int) {
        TODO("Not yet implemented")
    }

    override fun split() {
        TODO("Not yet implemented")
    }

    override fun detonate() {
        TODO("Not yet implemented")
    }
}

fun simpleBot(player: BasePlayer) {
    player.join()
    player.start()
    while (true) {
        player.move(1, 1)
        if (player.isGameFinished()) {
            println("Game is finished for $player")
            return
        }
    }
}

fun noopBot(player: BasePlayer) {
    player.join()
    player.start()
    while (true) {
        player.noop()
        if (player.isGameFinished()) {
            println("Game is finished for $player")
            return
        }
    }
}

fun runAwayBot(player: ApiPlayer) {
    player.join()
    player.start()
    while (true) {
        val role = player.gr!!.staticGameInfo!!.role
        if (player.gr!!.gameState != null) {
            var myShipAndCmds: Pair<ShipState, List<Expr>> =
                    if (role == player.gr!!.gameState!!.shipsAndCommands[0].first.role)
                        player.gr!!.gameState!!.shipsAndCommands[0]
                    else
                        player.gr!!.gameState!!.shipsAndCommands[1]
            val pos = asNumPair(myShipAndCmds.first.position as Ap)

            var dx = if (pos.first.toInt() == 0) 1000 else ((1 + pos.second.toInt()) * pos.second.toInt()).div(pos.first.toInt()) + pos.first.toInt()
            var dy = if (pos.second.toInt() == 0) 1000 else ((1 + pos.first.toInt()) * pos.first.toInt()).div(pos.second.toInt()) + pos.second.toInt()
            dx = if (dx > 1) 1 else if (dx < -1) -1 else 0
            dy = if (dy > 1) 1 else if (dy < -1) -1 else 0
            println("from($pos)")
            println("move($dx, $dy)")
            player.move(-dy, dx)
        } else {
            player.noop()
        }
        if (player.isGameFinished()) {
            println("Game is finished for $player")
            return
        }
    }
}

fun shipStatesByRole(role: BigInteger, gameState: GameState): List<ShipState> {
    var out = ArrayList<ShipState>()
    for (pair in gameState.shipsAndCommands) {
        if (pair.first.role == role)
            out.add(pair.first)
    }
    return out
}

fun getFuel(shipState: ShipState): BigInteger {
    return asNum(consHead(shipState.x4 as Ap))
}

fun mainShip(shipStates: List<ShipState>): ShipState? {
    for (shipState in shipStates) {
        return shipState
//        if(getFuel(shipState) > BigInteger.ZERO) {
//            return shipState
//        }
    }
    return null
}

fun closestShip(ourPos: IntPoint, shipStates: List<ShipState>): ShipState? {
    var ret: ShipState? = null
    var minDist: Int = Int.MAX_VALUE
    for (otherShip in shipStates) {
        if (ret == null) {
            ret = otherShip
            continue
        }
        val otherPos = asNumIntPoint(otherShip.position as Ap) + asNumIntPoint(otherShip.velocity as Ap)
        val dist = abs(ourPos.x - otherPos.x) + abs(ourPos.y - otherPos.y)
        if (dist < minDist) {
            minDist = dist
            ret = otherShip
        }
    }
    return ret
}


fun asNumIntPoint(expr: Ap): IntPoint {
    val p = asNumPair(expr)
    return IntPoint(p.first.toInt(), p.second.toInt())
}

fun iCopySign(signSrc: Int, absSrc: Int): Int {
    return abs(absSrc) * iSign(signSrc)
}

fun iSign(value: Int): Int {
    return if (value >= 0) return 1
    else -1
}

fun getCmds(shipState: ShipState, gameState: GameState): List<Expr> {
    for (pair in gameState.shipsAndCommands) {
        if (pair.first.shipId == shipState.shipId)
            return pair.second
    }
    return ArrayList<Expr>()
}

fun gravityShift(pt : IntPoint): IntPoint {
    return if (abs(pt.x) > abs(pt.y)) IntPoint(-iSign(pt.x), 0) else IntPoint(0, -iSign(pt.y))
}

fun getNextPos(otherShip: ShipState, gameState: GameState): IntPoint {
    val otherPos = asNumIntPoint(otherShip.position as Ap)
    val otherVel = asNumIntPoint(otherShip.velocity as Ap)
    var addVel = IntPoint(0, 0)
    for (cmd in getCmds(otherShip, gameState)) {
        if (asNum(consHead(cmd as Ap)) == BigInteger.ZERO) {
            addVel = asNumIntPoint(consHead(consTail(cmd as Ap) as Ap) as Ap)
        }

    }
    return otherPos + otherVel + addVel + gravityShift(otherPos)
}

fun orbiter(player: ApiPlayer) {
    val planetSize = 16

    player.join()
    player.start()
    var step = 0
    var splitted = 0
    while (true) {
        step += 1
        if (player.gr!!.gameState == null) {
            player.noop()
            continue
        }
        val role = player.gr!!.staticGameInfo!!.role
        val myShipStates = shipStatesByRole(role, player.gr!!.gameState!!)
        val otherShipStates = shipStatesByRole(BigInteger.ONE - role, player.gr!!.gameState!!)
        val ourShip = mainShip(myShipStates)!!

        val ourPos = asNumIntPoint(ourShip.position as Ap)
        val ourVel = asNumIntPoint(ourShip.velocity as Ap)
        val otherShip = mainShip(otherShipStates)!!

        val coordSum = abs(ourPos.x) + abs(ourPos.y)
        val velSum = abs(ourVel.x) + abs(ourVel.y)
        fun orbitStep(): IntPoint? {
            val addV = if (coordSum > 5 * planetSize) -1 else
                (if (coordSum > 3 * planetSize) 0
                else 1)

            val avx = max(0, sqrt(abs(ourPos.y).toDouble()).toInt() + addV)
            val avy = max(0, sqrt(abs(ourPos.x).toDouble()).toInt() + addV)
            val v1: IntPoint = IntPoint(iCopySign(ourPos.x, avx), -iCopySign(ourPos.y, avy))
            val v2: IntPoint = IntPoint(-iCopySign(ourPos.x, avx), iCopySign(ourPos.y, avy))
            val dv1 = abs(v1.x - ourVel.x) + abs(v1.y - ourVel.y)
            val dv2 = abs(v2.x - ourVel.x) + abs(v2.y - ourVel.y)
            val tv = if (dv1 < dv2) v1 else v2
            println("Target orbit vel is $tv, cur is $ourVel")
            val dx = tv.x - ourVel.x
            val dy = tv.y - ourVel.y
            if (abs(dx) + abs(dy) > 2) {
                val pt = IntPoint(-iSign(dx), -iSign(dy))
                return pt
            } else {
                return null
            }
        }
        fun shouldBlowup(): Boolean {
            if (otherShipStates.size > 1) {
                return false
            }
            val attackerShipRadius = 5
            val pt = getNextPos(otherShip, player.gr!!.gameState!!)
            return (abs(pt.x - ourPos.x) + abs(pt.y - ourPos.y) < attackerShipRadius)
        }
        if (role == BigInteger.ONE) {
            // defend - orbit
            val pt = orbitStep()
            if (pt != null) {
                println("defend move($pt)")
                player.move(pt.x, pt.y)
            } else {
//                val ourHeat = getHeat(ourShip)
//                val acceptableHeat = BigInteger.valueOf(16)
                if (getFuel(ourShip) > BigInteger.valueOf(50) && splitted < 99) {
                    splitted += 1
                    player.split()
                } else {
                    player.noop()
                }
            }
        } else {
            // attack - orbit and blowup?
            val blowup = shouldBlowup()
            if (blowup) {
                player.detonate()
            } else {
                val pt = orbitStep()
                if (pt != null) {
                    println("attack move($pt)")
                    player.move(pt.x, pt.y)
                } else {
                    val maxHeat = BigInteger.valueOf(64)
                    val ourHeat = getHeat(ourShip)
                    if (ourHeat < maxHeat) {
                        val pt = getNextPos(otherShip, player.gr!!.gameState!!)
                        println("attack shoot($pt)")
                        player.shoot(pt.x, pt.y, (maxHeat-ourHeat).toInt())
                    } else {
                        player.noop()
                    }
                }
            }
        }
        if (player.isGameFinished()) {
            println("Game is finished for $player")
            return
        }
    }
}

private fun getHeat(ourShip: ShipState) = asNum(ourShip.x5)


fun main() {
//    val gr = parseGameResponse(ConsParser.parse("(1,(1,((256,(0,((512,(1,(64,nil))),((16,(128,nil)),((152,(0,(8,(100,nil)))),nil))))),((12,((16,(128,nil)),((((1,(0,((-25,-32),((-8,6),((140,(0,(8,(97,nil)))),(6,(64,(1,nil)))))))),(((3,((0,(0,(0,(1,nil)))),nil)),((0,((1,-1),nil)),nil)),nil)),(((0,(1,((21,38),((6,-4),((0,(38,(10,(1,nil)))),(64,(64,(1,nil)))))))),(((2,((-24,-34),(64,(0,(4,nil))))),nil),nil)),(((1,(2,((-23,-34),((-7,5),((0,(0,(0,(1,nil)))),(11,(64,(1,nil)))))))),(nil,nil)),(((1,(3,((-26,-31),((-8,6),((0,(0,(0,(1,nil)))),(1,(64,(1,nil)))))))),(nil,nil)),(((1,(4,((-25,-32),((-8,6),((0,(0,(0,(1,nil)))),(0,(64,(1,nil)))))))),(nil,nil)),nil))))),nil))),nil))))"))
//    println("gr=$gr")
    val aliensApi = AliensApi()
    val ret = aliensApi.sendExprToAliens(aliensApi.makeCreateRequest())
    val resp = parseCreateResponse(ret as Ap)
    println("gameResponse=$resp")
////
    val attacker = ApiPlayer(resp.attackPlayerKey, aliensApi)
    val defender = ApiPlayer(resp.defendPlayerKey, aliensApi)
    thread(start = true) { orbiter(attacker) }
    thread(start = true) { orbiter(defender) }

    // Order is important because of the click coords!
//    val attacker = VisPlayer(resp.attackPlayerKey, Visualizer())
//    val defender = ApiPlayer(resp.defendPlayerKey, aliensApi)
//
//    thread(start = true) { noopBot(attacker) }
//    thread(start = true) { orbiter(defender) }
}


