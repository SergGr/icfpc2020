package icfpc.player.visualizer

import icfpc.expr.AlienImage
import icfpc.expr.GameEngine
import icfpc.expr.HotPoints
import icfpc.expr.IntPoint
import java.awt.*
import java.awt.event.ComponentAdapter
import java.awt.event.ComponentEvent
import java.util.prefs.Preferences
import javax.swing.*


class Visualizer : JPanel() {
    val gameEngine: GameEngine = GameEngine()

    var alienImage: AlienImage = AlienImage.emptyImage
    var hotPoints: HotPoints? = null
    private val drawingArea: DrawingArea
    private var instance: Visualizer

    //        var cbBotConfigs: Array<JComboBox<*>> = arrayOfNulls(BOTS_COUNT)
    val restartButton: JButton
    val undoButton: JButton
    val pauseButton: JButton
    val nextStep: JButton
    val brutePairsButton: JButton
    val bruteTicTacButton: JButton
    val buttonPanel: JPanel
    private var showCoords: JCheckBox
    private var lastClickedPoint: JLabel
    private var currentMousePoint: JLabel
    private var showGrid: JCheckBox
    private var searchHots: JCheckBox
    private var annotateGlyphs: JCheckBox
    private var withCheats: JCheckBox
    private var searchHotsFlag = false
    private var annotateGlyphsFlag = false
    private var withCheatsFlag = false
    var progressBar: JProgressBar

    init {
        val dim = Toolkit.getDefaultToolkit().screenSize
        val prefs = Preferences.userRoot().node("visualizer")
        val width = prefs.getInt("screenW", dim.width - 100)
        val height = prefs.getInt("screenH", dim.height - 30)
        val wx = prefs.getInt("wx", 0)
        val wy = prefs.getInt("wy", 9)
        val myFrame = JFrame("Galaxy Interactor")
        myFrame.bounds = Rectangle(wx, wy, width, height)
        myFrame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
        myFrame.addComponentListener(object : ComponentAdapter() {
            override fun componentResized(e: ComponentEvent) {
                prefs.putInt("screenW", myFrame.width)
                prefs.putInt("screenH", myFrame.height)
                prefs.putInt("wx", myFrame.x)
                prefs.putInt("wy", myFrame.y)
            }

            override fun componentMoved(e: ComponentEvent) {
                prefs.putInt("screenW", myFrame.width)
                prefs.putInt("screenH", myFrame.height)
                prefs.putInt("wx", myFrame.x)
                prefs.putInt("wy", myFrame.y)
            }
        })
//        instance = Visualizer()
        instance = this
        myFrame.add(instance)
        val mainLayout = BorderLayout()
        instance.layout = mainLayout
        drawingArea = DrawingArea(instance)
        drawingArea.isOpaque = true
        instance.add(drawingArea, BorderLayout.CENTER)
        buttonPanel = JPanel()
        val buttonLayout = GridBagLayout()
        buttonPanel.layout = buttonLayout
        buttonPanel.minimumSize = Dimension(100, 100)
        instance.add(buttonPanel, BorderLayout.EAST)
        val defConstr = GridBagConstraints()
        defConstr.gridx = 0
        defConstr.gridy = GridBagConstraints.RELATIVE
        defConstr.fill = GridBagConstraints.HORIZONTAL
        defConstr.insets = Insets(0, 5, 0, 5)
        val glueConstr = defConstr.clone() as GridBagConstraints
        glueConstr.weightx = 10.0
        glueConstr.weighty = 10.0
        buttonPanel.add(Box.createVerticalStrut(10), defConstr)

        restartButton = JButton("Restart Game")
        restartButton.addActionListener({
            gameEngine.restart(withCheatsFlag)
            updateUi()
        })
        buttonPanel.add(Box.createVerticalStrut(10), defConstr)
        buttonPanel.add(restartButton, defConstr)

        withCheats = JCheckBox("... with Cheat Codes")
        withCheats.addActionListener({
            withCheatsFlag = withCheats.isSelected
            updateUi()
        })
        withCheats.setSelected(false)
        buttonPanel.add(withCheats, defConstr)

        undoButton = JButton("Undo")
        undoButton.addActionListener({
            gameEngine.undoStep()
            updateUi()
        })
        buttonPanel.add(Box.createVerticalStrut(10), defConstr)
        buttonPanel.add(undoButton, defConstr)
        undoButton.isEnabled = false

        showCoords = JCheckBox("Show Coords")
        showCoords.addActionListener({
            drawingArea.showCoords = showCoords.isSelected
            drawingArea.repaint()
        })
        showGrid = JCheckBox("Show Grid")
        showGrid.addActionListener({
            drawingArea.showGrid = showGrid.isSelected
            drawingArea.repaint()
        })
        lastClickedPoint = JLabel(" ")
        currentMousePoint = JLabel(" ")
        searchHots = JCheckBox("Search hot points")
        searchHots.addActionListener({
            searchHotsFlag = searchHots.isSelected
            updateUi()
        })
        searchHots.setSelected(false)

        annotateGlyphs = JCheckBox("Annotate glyphs")
        annotateGlyphs.addActionListener({
            annotateGlyphsFlag = annotateGlyphs.isSelected
            updateUi()
        })
        annotateGlyphs.setSelected(false)

        pauseButton = JButton("Pause Game")
        pauseButton.addActionListener({  })
        buttonPanel.add(pauseButton, defConstr)
        nextStep = JButton("Next step")
        nextStep.addActionListener({  })
        buttonPanel.add(nextStep, defConstr)
        brutePairsButton = JButton("BruteForce Pairs game")
        brutePairsButton.addActionListener({
            bruteForcePairsGame()
        })
        buttonPanel.add(brutePairsButton, defConstr)

        progressBar = JProgressBar(0, 100)
        progressBar.setValue(0)
        progressBar.setStringPainted(true)
        buttonPanel.add(progressBar, defConstr)

        bruteTicTacButton = JButton("BruteForce Tic-Tac-Toe")
        bruteTicTacButton.addActionListener({ bruteForceTicTacToe() })
        buttonPanel.add(bruteTicTacButton, defConstr)
        buttonPanel.add(Box.createVerticalStrut(10), defConstr)
/*
        for (i in 0 until numPlayers) {
            status[i] = JLabel(if (i == 0) YOUR_TURN else " ")
            status[i].foreground = DrawingArea.palette[i]
            status[i].horizontalAlignment = SwingConstants.LEFT
            score[i] = JLabel(if (numPlayers <= i) "" else "Score: 0")
            score[i].foreground = DrawingArea.palette[i]
//                cbBotConfigs[i] = JComboBox(choices)
//                cbBotConfigs[i].selectedIndex = 1
//                cbBotConfigs[i].addActionListener { e: ActionEvent -> config.get() = (e.source as JComboBox<*>).selectedItem as String }
//                cbBotConfigs[i].selectedIndex = 0
            buttonPanel.add(Box.createVerticalStrut(1), defConstr)
            buttonPanel.add(status[i], defConstr)
//                buttonPanel.add(cbBotConfigs[i], defConstr)
            buttonPanel.add(score[i], defConstr)
        }
*/

        buttonPanel.add(Box.createVerticalStrut(40), defConstr)
        buttonPanel.add(showCoords, defConstr)
        buttonPanel.add(lastClickedPoint, defConstr)
        buttonPanel.add(showGrid, defConstr)
        buttonPanel.add(currentMousePoint, defConstr)
        buttonPanel.add(searchHots, defConstr)
        buttonPanel.add(annotateGlyphs, defConstr)
        buttonPanel.add(Box.createVerticalGlue(), glueConstr)
        buttonPanel.add(Box.createVerticalStrut(10), defConstr)
        drawingArea.init(alienImage)
        myFrame.isVisible = true

        SwingUtilities.invokeLater { updateUi() }

    }

    fun bruteForcePairsGame() {
        val task = Runnable {
            val start = System.currentTimeMillis()
            progressBar.setString("0 pairs found")
            val oldState = gameEngine.state
            val found: MutableList<Pair<Pair<Int, Int>, Pair<Int, Int>>> = ArrayList()
            for (i in 0..62) {
                val firstCard = Pair((i and 7) * 6, (i shr 3) * 6)
                if (canSkip(found, firstCard)) {
                    continue
                }
                for (j in (i+1)..63) {
                    val secondCard = Pair((j and 7) * 6, (j shr 3) * 6)
                    if (canSkip(found, secondCard)) {
                        continue
                    }
                    gameEngine.step(firstCard)
                    gameEngine.step(secondCard) // click second card
                    val trySecondState = gameEngine.state
                    if (trySecondState != oldState) { // found pair
                        found.add(Pair(firstCard, secondCard))
                        progressBar.setString("${found.size} pair(s) found");
                        gameEngine.undoStep()
                        gameEngine.undoStep()
                        break
                    }
                }
                progressBar.value = i * 100 / 62
            }
            val end = System.currentTimeMillis()
            println("Found card pairs in ${end - start} ms:")
            for (cards in found) {
                println("${cards.first} and ${cards.second}")
            }
            // playing the cards
            for (cards in found) {
                gameEngine.step(cards.first)
                gameEngine.step(cards.second)
            }
            updateUi()
        }
        Thread(task).start()
    }

    private fun canSkip(found: MutableList<Pair<Pair<Int, Int>, Pair<Int, Int>>>, card: Pair<Int, Int>): Boolean {
        for (cardPair in found) {
            if (card == cardPair.first || card == cardPair.second) {
                return true
            }
        }
        return false
    }

    fun bruteForceTicTacToe() {
        val task = Runnable {
            val start = System.currentTimeMillis()
            TicTacToe.bruteForce(gameEngine, instance)
            val end = System.currentTimeMillis()
            println("Found 12 different terminal Tic-Tac-Toe positions in ${end - start} ms")
            updateUi()
        }
        Thread(task).start()
    }

    fun handleUserClick(p: IntPoint) {
        SwingUtilities.invokeLater { lastClickedPoint.text = "(${p.x}, ${p.y})" }

        val success = gameEngine.step(Pair(p.x, p.y))
        if (success) {
            updateUi()
        }
    }

    fun handleMouseMove(p: IntPoint) {
        SwingUtilities.invokeLater { currentMousePoint.text = "(${p.x}, ${p.y})" }
    }

    fun updateUi() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater { updateUi() }
            return
        }

//        println("Update UI for images ${gameEngine.lastImages.hashCode()}, state = ${gameEngine.hashCode()}")
        alienImage = gameEngine.lastAlienImage
        drawingArea.init(alienImage)
        drawingArea.repaint()

        if (searchHotsFlag && (hotPoints == null || hotPoints!!.state != gameEngine.state)) {
            val task = Runnable {
                val start = System.currentTimeMillis()
//                println("Searching for hot points for ${gameEngine.state.hashCode()}")
                val hotPoints = gameEngine.findHotPoints() { hotPoints -> applyHotPoints(hotPoints) }
                this.hotPoints = hotPoints
                val end = System.currentTimeMillis()
                println("Found ${hotPoints.hotPoints.size} ${hotPoints.inProgress} hotPoints for ${hotPoints.state.hashCode()}, state is ${gameEngine.state.hashCode()} in ${end - start}")
                applyHotPoints(hotPoints)
            }
            Thread(task).start()
        }

        updateButtons()
    }

    private fun applyHotPoints(hotPoints: HotPoints) {
        SwingUtilities.invokeLater {
            if (gameEngine.state == hotPoints.state) {
                if (hotPoints.hotPoints.isNotEmpty()) {
//                            println("Found ${hotPoints.hotPoints.take(5)} for ${hotPoints.state.hashCode()}")
                }
                drawingArea.hotPoints = hotPoints.hotPoints
                drawingArea.repaint()
            }
        }
    }

    fun updateButtons() {
        if (!SwingUtilities.isEventDispatchThread()) {
            SwingUtilities.invokeLater { updateButtons() }
            return
        }
        undoButton.isEnabled = gameEngine.canUndo
//        fastGameButton.isEnabled = !drawingArea.slowPlay && !drawingArea.fastPlay
//        slowGameButton.isEnabled = !drawingArea.slowPlay && !drawingArea.fastPlay
        pauseButton.isEnabled = drawingArea.autoPlay || drawingArea.slowPlay
    }


    companion object {

        private fun getScore(i: Int): Int {
            //        for (int mine : mapFromFile.getMines()) {
//            score += mapFromFile.calcScore(mine, i);
//        }
            return 0
        }
    }
}

fun main(args: Array<String>) {
    val vis = Visualizer()
}
