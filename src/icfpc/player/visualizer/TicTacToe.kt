package icfpc.player.visualizer

import icfpc.expr.*
import java.io.File
import kotlin.random.Random

object TicTacToe {

    private val db: MutableList<MutableList<Int>> = ArrayList()

    init {
        val lines: List<String> = File("./data/maps/tic-tac-toe.data").readLines()
        for (line in lines) {
            val game: MutableList<Int> = ArrayList()
            for (op in line.split(",")) {
                val elem: Int = when (op) {
                    "x" -> 1
                    "o" -> 2
                    "b" -> 0
                    else -> -1
                }
                if (elem != -1) {
                    game.add(elem)
                }
            }
            db.add(game)
        }
    }

    private var played: MutableList<Int> = ArrayList()
    private var finished = false
    private var listSize = 0
    private lateinit var instance: Visualizer

    // Take random position from DB, try to play it randomly,
    fun bruteForce(gameEngine: GameEngine, instance: Visualizer) {
        this.instance = instance
        var game: MutableList<Int>?
        var oldSize = 0
        instance.progressBar.string = "0 unique positions played"
        while (!finished) {
            played.clear()
            val idx = Random.nextInt(db.size)
            game = db[idx] // random game from DB
//            println("======= new game ==========")
            for (i in 0..4) { // play one game
                game = turn(game, gameEngine)
                if (game == null) {
                    break
                }
            }
            gameEngine.step(-1000 to -1000)
            if (listSize != oldSize) {
//                println("Advanced by $listSize")
                instance.progressBar.value = listSize * 100 / 12
                instance.progressBar.string = "$listSize unique positions played"
                oldSize = listSize
                instance.updateUi()
            }
        }
    }

    private fun turn(game: MutableList<Int>?, gameEngine: GameEngine): MutableList<Int>? {
        if (game == null) {
            return null
        }
        // play randomly in choosen game, skip already played pieces
        val r = Random.nextInt(getNumX(game) - played.size)
        // find the r-th "1" in this game
        var xCounter = 0
        var k = 0
        var board: MutableList<Int>? = ArrayList()
        for (piece in game) {
            if (!played.contains(k)) {
                if (piece == 1) { // human
                    if (xCounter == r) {
                        // game[k] contains n-th 1, play here
                        played.add(k)
                        val x = k % 3
                        val y = k / 3
                        gameEngine.step(x to y)
                        board = getBoard(gameEngine.state)
                        listSize = getListSize(gameEngine.state)
                        break
                    }
                    xCounter++
                }
            }
            k++
        }
        if (board == null) {
            finished = true
            return null
        }
        // check if terminal position
        if (db.contains(board)) {
            return null
        }
        // find first matching game in DB
        for (g in db) {
            if (matches(g, board)) {
                return g
            }
        }
        return null
    }

    private fun matches(game: MutableList<Int>, board: MutableList<Int>): Boolean {
        for (i in 0..8) {
            if (board[i] == 1 && game[i] == 1 || board[i] == 2 && game[i] == 2 || board[i] == 0) {
                continue
            }
            return false
        }
        return true
    }

    private fun getNumX(game: MutableList<Int>): Int {
        var i = 0
        for (x in game) {
            if (x == 1) {
                i++
            }
        }
        return i
    }

    private fun getBoard(state: Expr): MutableList<Int>? {
        if ((((state as Ap).func as Ap).arg as Atom).name.toInt() != 3) {
            return null
        }
        if ((((((state.arg as Ap).func as Ap).arg as Ap).func as Ap).arg as Atom).name.toInt() != 0) {
            return null // 2 == final win condition
        }
        val result: MutableList<Int> = ArrayList()
        var expr = ((((state.arg as Ap).func as Ap).arg as Ap).arg as Ap).func // board
        for (i in 0..8) {
            expr = (expr as Ap).arg
            val t = ((expr as Ap).func as Ap).arg as BaseAtom
            result.add(when(t) {
                is IntAtom -> t.value.toInt()
                else -> t.name.toInt()
            })
        }

        return result
    }

    private fun getListSize(state: Expr): Int {
        var size = 0
        var list = (((((((state as Ap).arg as Ap).func as Ap).arg as Ap).arg as Ap).arg as Ap).func as Ap).arg
        while (list is Ap) {
            size++
            list = list.arg
        }
        return size
    }
}