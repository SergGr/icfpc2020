package icfpc.player.visualizer

import icfpc.expr.AlienImage
import icfpc.expr.IntPoint
import java.awt.*
import java.awt.event.*
import java.awt.geom.*
import java.util.*
import javax.swing.JPanel

internal class DrawingArea(private val _visualizer: Visualizer) : JPanel(), MouseListener, MouseMotionListener, MouseWheelListener {
    var minX = 0
    var minY = 0
    var maxX = 0
    var maxY = 0
    var numLayers = 0
    var imagePoints: List<IntPoint> = ArrayList()
    var hotPoints: MutableList<IntPoint> = ArrayList()
    var hotPointsInProgress = false
    private val zoomAndPanTransform = AffineTransform()
    private var combinedTransform: AffineTransform? = null
    var autoPlay = false
    var fastPlay = false
    var slowPlay = false
    var showCoords = false
    var showGrid = false

    fun init(image: AlienImage) {
        imagePoints = image.points
        minX = image.minX
        minY = image.minY
        maxX = image.maxX
        maxY = image.maxY
        numLayers = image.numLayers
        hotPoints.clear()
    }

    public override fun paintComponent(g0: Graphics) {
        super.paintComponent(g0)
        val g = g0 as Graphics2D
        // add 10px padding and 1 cell wide border
        val cellSize = Math.min((width - 2*PADDING) / (maxX - minX + 3), (height - 2*PADDING) / (maxY - minY + 3))
        val originalTransform = g.transform
        val additionalTransform = AffineTransform()
        additionalTransform.translate(PADDING.toDouble(), PADDING.toDouble())
        additionalTransform.scale(cellSize.toDouble(), cellSize.toDouble())
        additionalTransform.translate(-minX + 1.0, -minY + 1.0) // double coords of origin
        //        additionalTransform.transform(zoomAndPanTransform);
        g.transform(additionalTransform)

        // now stroke widths are independent on zoom
        val scaleX = additionalTransform.scaleX.toFloat()
        val scaleY = additionalTransform.scaleY.toFloat()
        combinedTransform = additionalTransform
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_GASP)
        g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE)
        g.color = Color.black
        g.fill(Rectangle2D.Double((minX - 1).toDouble(), (minY - 1).toDouble(), (maxX-minX+3).toDouble(), (maxY-minY+3).toDouble()))
        for (p in imagePoints) {
            g.color = Color(255, 255, 255, 255 - 224 * p.layer / numLayers)
            val x = p.x.toDouble()
            val y = p.y.toDouble()
            g.fill(Rectangle2D.Double(x, y, (cellSize / scaleX).toDouble(), (cellSize / scaleY).toDouble()))
        }
        if (hotPointsInProgress) {
            g.color = Color(255, 0, 0, 200)
        } else {
            g.color = Color(125, 255, 0, 200)
        }
        for (p in hotPoints) {
            val x = p.x.toDouble()
            val y = p.y.toDouble()
            g.fill(Rectangle2D.Double(x, y, (cellSize / scaleX).toDouble(), (cellSize / scaleY).toDouble()))
        }
        if (showGrid) {
            val oldStroke = g.stroke
            g.stroke = BasicStroke(0.5f / scaleX)
            g.color = Color.gray
            for (x in minX - 1..maxX + 2) {
                g.draw(Line2D.Double(x.toDouble(), (minY - 1).toDouble(), x.toDouble(), (maxY + 2).toDouble()))
            }
            for (y in minY - 1..maxY + 2) {
                g.draw(Line2D.Double((minX - 1).toDouble(), y.toDouble(), (maxX + 2).toDouble(), y.toDouble()))
            }
            g.stroke = oldStroke
        }

        // Show coords on the cells
        g.transform = originalTransform
        if (showCoords) {
            val font = Font("Dialog", Font.PLAIN, 12)
            g.font = font
            val fm = g.fontMetrics
            for (m in imagePoints) {
                val x = m.x.toDouble()
                val y = m.y.toDouble()
                val pos = additionalTransform.transform(Point2D.Double(x, y), null)
                val str = "" + m.x + "," + m.y
                g.color = palette[13]
                val rect = fm.getStringBounds(str, g)
                rect.setRect(pos.x + rect.width * 0.2, pos.y + rect.height * 0.2, rect.width, rect.height)
                g.drawString(str, pos.x.toFloat(), (pos.y + rect.height).toFloat())
            }
        }
    }

    private fun screenCoordsToDouble(mousePoint: Point?): IntPoint? {
        if (combinedTransform == null) return null
        val inv: AffineTransform
        inv = try {
            combinedTransform!!.createInverse()
        } catch (e: NoninvertibleTransformException) {
            throw RuntimeException("AAAAA")
        }
        val result = inv.transform(mousePoint, Point2D.Double())
        return IntPoint(round(result.x), round(result.y))
    }

    override fun mouseClicked(e: MouseEvent) {
        val userPoint = screenCoordsToDouble(e.point)
        if (userPoint != null) {
            _visualizer.handleUserClick(userPoint)
        }
    }

    override fun mouseMoved(e: MouseEvent) {
        val userPoint = screenCoordsToDouble(e.point)
        if (userPoint != null) {
            _visualizer.handleMouseMove(userPoint)
        }
    }

    override fun mousePressed(e: MouseEvent) {}
    override fun mouseReleased(e: MouseEvent) {}
    override fun mouseEntered(e: MouseEvent) {}
    override fun mouseExited(e: MouseEvent) {}
    override fun mouseDragged(e: MouseEvent) {}
    override fun mouseWheelMoved(e: MouseWheelEvent) {}

    fun resetScaling() {
        zoomAndPanTransform.setToIdentity()
    }

    companion object {
        private val PADDING = 10
        var palette = arrayOf(
                Color.red,  // 0
                Color.blue,  // 1
                Color.magenta,  // 2
                Color.green,  // 3
                Color.cyan,  // 4
                Color.yellow,  // 5
                Color.DARK_GRAY,  // 6
                Color.ORANGE,  // 7
                Color.pink,  // 8
                Color(100, 14, 33),  // 9
                Color(150, 70, 33),  // 10
                Color(70, 150, 33),  // 11
                Color(70, 33, 150),  // 12
                Color(170, 33, 150))

        fun round(d: Double): Int {
            val r = d - 0.5 // shift by 0.5 is needed because coords are cell-centered
            return if (r > 0) {
                (r + 0.5).toInt()
            } else {
                (r - 0.5).toInt()
            }
        }
    }

    init {
        addMouseListener(this)
        addMouseMotionListener(this)
        //        addMouseWheelListener(this);
        background = Color.lightGray
    }
}