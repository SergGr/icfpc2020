#!/bin/bash

# build and run docker image locally for tests

#$(docker container ls -a -q -f "name=icfpc")
docker stop icfpc
docker rm --force icfpc
docker rmi --force "thirteen/icfpc2020:dev"
docker build --tag "thirteen/icfpc2020:dev" .
docker run --detach --name icfpc "thirteen/icfpc2020:dev"
