#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <algorithm>
#include <iterator>

bool is_number(const std::string& s)
{
    std::string::const_iterator it = s.begin();
    while (it != s.end() && (std::isdigit(*it) || *it == '-')) ++it;
    return !s.empty() && it == s.end();
}

class Stream {
  virtual bool HasNext() = 0;
  virtual std::string Next() = 0;
};

class SimpleStream: public Stream {
 public:
  SimpleStream(std::vector<std::string> s): s_(s), idx_(0) {}
  bool HasNext() {
    return idx_ < s_.size();
  }
  std::string Next() {
    return s_.at(idx_++);
  }
 private:
  std::vector<std::string> s_;
  long idx_;
};

class ChainStream: public Stream {
  ChainStream(Stream s1, Stream s2): s1_(s1), s2_(s2), switched_(false) {}
  bool HasNext() {
    if (!switched_) {
      return s1_.HasNext() || s2_.HasNext();
    } else {
      return s2_.HasNext()
    }
  }
  std::string Next() {
    if (switched_) {
      return s2_.Next();
    } else {
      if (s1_.HasNext()) {
        return s1_.Next();
      } else {
        switched_ = true;
        return s2.Next();
      }
    }
  }
 private:
  Stream s1_;
  Stream s2_;
  bool switched_;
}

struct ConsNode {
  ConsNode(std::unique_ptr<ConsNode> head, std::unique_ptr<ConsNode> tail): head_(head), tail_(tail) {}
  std::unique_ptr<ConsNode> head_;
  std::unique_ptr<ConsNode> tail_;
};

typedef std::variant<std::monostate, std::string, int64_t,
    ConsNode, std::function<TreeNode (TreeNode)>,
    std::function<TreeNode (TreeNode (TreeNode))>> TreeNode;
class Evaler {
 public:
  TreeNode consume_ap() {
    TreeNode input1 = consume1();
    try {
      std::string input1_str = std::get<std::string>(input1);
      if (input1_str[0] == ':') {
        ops_ = ChainStream(SimpleStream(context[input1]), ops_);
        return consume_ap();
      }
    }
    catch (const std::bad_variant_access&) {}
    return to_fn(input1)(consume1());
  }

  TreeNode consume1() {
    assert (ops_.HasNext());
    std::string head = ops_.Next();
    if (head == 'ap') {
      return consume_ap()
    } else {
      return head;
    }
  }

  TreeNode compute(std::vector<std::string> ops) {
    ops_ = SimpleStream(ops);
    return consume1();
  }

  int64_t to_int(TreeNode x) {
    try {
      int64_t x = std::get<int64_t>(x);
      return x;
    }
    catch (const std::bad_variant_access&) {}
    assert(false);
  }


  auto to_fn(TreeNode op) {
    try {
      std::string op_str = std::get<std::string>(op);
      if (op_str == "inc") {
        return TreeNode([&](TreeNode x) {
          return TreeNode(to_int(x) + 1);});
      } else if (op_str == "add") {
        return TreeNode([&](TreeNode x) {[&](TreeNode y) {
          return TreeNode(to_int(x) + to_int(y));}});
      }
    }
    catch (const std::bad_variant_access&) {}
    return std::monostate();
  }

 public:
  std::unordered_map<std::string, std::vector<std::string>> context;

 private:
  Stream ops_;
};

std::vector<std::string> split(std::string s) {
  std::istringstream iss(s);
  return std::vector<std::string>{
      std::istream_iterator<std::string>{iss},
      std::istream_iterator<std::string>{}};
}

int main() {
  Evaler evaler;
  evaler.context.
  evaler.context[":2048"] = split("ap f :2048")
  auto galaxy = split("ap :2048 42")
  std::cout << evaler.compute(galaxy);
}
// class Evaler(object):
//   def __init__(self):
//     self.context = {}
//     self.ops = None

//   def to_int(self, x):
//     if is_int(x):
//       return int(x)
//     if isinstance(x, list):
//       assert len(x) == 1, str(x)
//       return self.to_int(x[0])
//     if x in self.context:
//       return self.to_int(self.context[x])
//     if isinstance(x, Cons) and x.tail == 'nil':
//       return self.to_int(x.head)
//     print ("Something really-really bad happened")
//     import pdb; pdb.set_trace()
//     return 0

  // def to_fn(self, op):
  //   if op == 'add':
  //     return lambda x: lambda y: self.to_int(x) + self.to_int(y)
  //   elif op == 'mul':
  //     return lambda x: lambda y: self.to_int(x) * self.to_int(y)
  //   elif op == 'div':
  //     def div1(x):
  //       def div2(y):
  //         a = self.to_int(x)
  //         b = self.to_int(y)
  //         return (a+(-a%b))//b
  //       return div2
  //     return div1
  //   elif op == 'inc':
  //     return lambda x: self.to_int(x) + 1
  //   elif op == 'neg':
  //     return lambda x: -self.to_int(x)
  //   elif op == 'dec':
  //     return lambda x: self.to_int(x) - 1
  //   elif op == 't':
  //     def t(x):
  //       return lambda y: x
  //     return t
  //   elif op == 'f':
  //     def f(x):
  //       return lambda y: y
  //     return f
  //   elif op == 's':
  //     return lambda x0: lambda x1: lambda x2: self.to_fn(x0)(x2)(self.to_fn(x1)(x2))   #ap ap x0 x2 ap x1 x2
  //   elif op == 'c':
  //     return lambda x0: lambda x1: lambda x2: self.to_fn(self.to_fn(x0)(x2))(x1)  # ap ap x0 x2 x1
  //   elif op == 'b':
  //     return lambda x0: lambda x1: lambda x2: self.to_fn(x0)(self.to_fn(x1)(x2))
  //   elif op == 'cons':
  //     def cons1(x0):
  //       def cons2(x1):
  //         # xx0 = self.maybe_eval(x0)
  //         # xx1 = self.maybe_eval(x1)
  //         return Cons(x0, x1)
  //       return cons2
  //     return cons1 # self.to_fn(self.to_fn(x2)(x0))(x1)  #   #  ap ap x2 x0 x1
  //   elif op == 'car':
  //     def car(x2):
  //       # import pdb; pdb.set_trace()
  //       return x2.head
  //       # return self.to_fn(x2)(self.to_fn('t'))
  //     return car
  //   elif op == 'cdr':
  //     def cdr(x2):
  //       return x2.tail
  //     return cdr
  //       # return self.to_fn(x2)(self.to_fn('f'))
  //   elif op == 'nil':
  //     def nil(x0):
  //       return self.to_fn('t')
  //     return nil
  //   elif op == 'i':
  //     return lambda x0: x0
  //   elif op == 'eq':
  //     return lambda x0: lambda y0: self.to_fn('t') if self.to_int(x0) == self.to_int(y0) else self.to_fn('f')
  //   elif op == 'lt':
  //     return lambda x0: lambda y0: self.to_fn('t') if self.to_int(x0) < self.to_int(y0) else self.to_fn('f')
  //   elif op == 'isnil':
  //     def isnil(x0):
  //       return self.to_fn('t') if x0 == 'nil' else self.to_fn('f')
  //     return isnil
  //   elif isinstance(op, Cons):
  //     def cons(x):
  //       return self.to_fn(x)(op.head)(op.tail)
  //     return cons
  //   elif callable(op):
  //     return op
  //   elif isinstance(op, str) and ":" in op:
  //     # print("to_fn: Chaining %s" % op)
  //     self.ops = itertools.chain(simple_producer(self.context[op]), self.ops)
  //     inp = self.consume1()
  //     return self.to_fn(inp)
  //   else:
  //     import pdb; pdb.set_trace()
  //     assert False, "Shouldn't happen %s" % op
  //     return lambda x: x

// if __name__ == '__main__':
//   evaler = Evaler()
//   lines = [x.strip().split(' ') for x in open(sys.argv[1]).readlines()]
//   for line in lines:
//     if line[1] == '=':
//       evaler.context[line[0]] = line[2:]
//   # evaler.compute(simple_producer(evaler.context[":1104"]))
//   # evaler.context[":galaxy"] = evaler.context["galaxy"]
//   # galaxy = 'ap ap :1338 nil ap ap cons 0 ap ap cons 0 nil'.split(' ')
//   # print(evaler.compute(simple_producer(galaxy)))
//   # evaler.context = {":67108929": "ap ap b ap b ap ap s ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c cons nil ap c cons".split(' ')}
//   # galaxy = "ap ap :67108929 nil ap ap cons 43 ap ap cons 44 nil".split(' ')
//   # evaler.compute(simple_producer(galaxy))


//   # print (list(context.items())[:3])
//   # evaler.context = {":2048": "ap f :2048".split(' ')}
//   # galaxy = "ap :2048 42".split(' ')
//   # evaler.compute(simple_producer(galaxy))
//   # evaler.context = {":1029": "ap ap c ap ap b b ap ap b ap b ap cons 0 ap ap c ap ap b b cons ap ap c cons nil ap ap c ap ap b cons ap ap c cons nil nil".split(' ')}
//   # galaxy = "ap ap :1029 nil ap ap cons 43 nil".split(' ')
//   # print(evaler.compute(simple_producer(galaxy)))
//   # # compute(simple_producer(sys.argv[1:]))
//   # compute(simple_producer(galaxy), context)
