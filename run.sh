#!/bin/sh
# see startkit-MIT-LICENSE
#used by the Docker image

export RUN_CP=
for f in _submission_app/lib/*.jar;  do
  export RUN_CP=${f}:$RUN_CP
done;
echo "Run libs: $RUN_CP"
java -cp ./_submission_app/Main.jar:$RUN_CP icfpc.MainKt "$@"
