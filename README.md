THIRTEEN's take on ICFP Contest 2020 aka ICFPC2020 https://icfpcontest2020.github.io/

The code is in Kotlin and is expected to be run with JVM **11** from Adopt Open JDK (https://adoptopenjdk.net/).

This repository is at https://gitlab.com/SergGr/icfpc2020

Some other useful links:
- Team Log In https://icfpcontest2020.github.io/#/login Api key **75332bed49a84298a712d53297f41e64**
- https://github.com/icfpcontest2020/ Organizers GitHub root
- https://github.com/icfpcontest2020/dockerfiles/blob/master/README.md submission rules
- https://message-from-space.readthedocs.io/en/latest/condensed-version.html messages from space
- https://discord.com/invite/xvMJbas Discord chat
 
Git SSH key for organizers:
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCph7SeMYzXpY7mFXZBhQbzWWBS0wDqr+zs3YfAAdNLdn4WrGIyWSOK1QCIMnuG8rCfXnBZwwg+Yh6162speAPJhWIwZWd4kiJGH7rlGBXSkBbUdQayAkJ6GmOncHWjNaRlhRcW89/jLnhkbTgqNF2ht3W5iZrbPolu5z7DfhGzUuqMmXkGOAhDq1tU379wLTa/zAYRSulqAueyR7UjzPC7lZXsaW/Rx86Foc+sUv2nmwIK9aaYHPKgCGRP8EdGv5/jTdfju543ZFoIkE1IyGgO0k5HQPwSELIaxjjv4UXaDese9hNQ8MGnVVzXvku1oJXz/rmcb88i9vMENkUWANzT generated-key

TeamID(?) 59cd4e86-9727-4164-b2e5-f0ad0f9cae01
