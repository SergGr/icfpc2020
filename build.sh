#!/bin/sh
# see startkit-MIT-LICENSE
# used by the Docker image

mkdir -p "_submission_app"
mkdir -p "_submission_app/lib"

# copy and flatten lib .jar's
# [0-9] is a hack to not copy javadoc and sources .jar files
find ./lib -type f -iregex ".*[0-9].jar" -exec cp {} ./_submission_app/lib \;

export BUILD_CP=
for f in _submission_app/lib/*.jar;  do
  export BUILD_CP=${f}:$BUILD_CP
done;
echo "Build libs: $BUILD_CP"

# one more package deeper. Is there a fully recursive way?
kotlinc -classpath $BUILD_CP -include-runtime -d ./_submission_app/Main.jar src/*/*.kt src/*/*/*.kt src/*/*/*.java src/*/*/*/*.kt src/*/*/*/*.java
